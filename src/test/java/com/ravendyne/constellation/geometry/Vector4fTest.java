package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Vector4fTest {

    @DisplayName("constructors")
    @Test
    public void test() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
                7.0f,
                8.0f,
        };

        Vector4f vector;

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector4f(data);
        Assertions.assertEquals(data[0], vector.getX());
        Assertions.assertEquals(data[1], vector.getY());
        Assertions.assertEquals(data[2], vector.getZ());
        Assertions.assertEquals(data[3], vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector4f(data, Vector4f.SIZE);
        Assertions.assertEquals(data[4], vector.getX());
        Assertions.assertEquals(data[5], vector.getY());
        Assertions.assertEquals(data[6], vector.getZ());
        Assertions.assertEquals(data[7], vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector4f(data, 0, 2);
        Assertions.assertEquals(data[0], vector.getX());
        Assertions.assertEquals(data[2], vector.getY());
        Assertions.assertEquals(data[4], vector.getZ());
        Assertions.assertEquals(data[6], vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        BufferVector4f bufferVector = BufferVector4f.newInstance(FloatBuffer.wrap(data));

        vector = new Vector4f(bufferVector);
        Assertions.assertEquals(bufferVector.getX(), vector.getX());
        Assertions.assertEquals(bufferVector.getY(), vector.getY());
        Assertions.assertEquals(bufferVector.getZ(), vector.getZ());
        Assertions.assertEquals(bufferVector.getW(), vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector4f(3, 2, 1, 0);
        Assertions.assertEquals(3, vector.getX());
        Assertions.assertEquals(2, vector.getY());
        Assertions.assertEquals(1, vector.getZ());
        Assertions.assertEquals(0, vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector4f(6, 5, 4);
        Assertions.assertEquals(6, vector.getX());
        Assertions.assertEquals(5, vector.getY());
        Assertions.assertEquals(4, vector.getZ());
        Assertions.assertEquals(1, vector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vector = Vector4f.ZERO;
        Assertions.assertEquals(0, vector.getX());
        Assertions.assertEquals(0, vector.getY());
        Assertions.assertEquals(0, vector.getZ());
        Assertions.assertEquals(0, vector.getW());
    }

    @DisplayName("arithmetic")
    @Test
    public void test2() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
                7.0f,
                8.0f,
        };

        Vector4f vectorA;
        Vector4f vectorB;
        Vector4f vectorC;
        
        ///////////////////////////////////////////////////////////////////////////////
        vectorA = new Vector4f(data);
        vectorB = new Vector4f(data, Vector4f.SIZE);
        
        vectorC = vectorA.add(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() + vectorB.getX(), vectorC.getX());
        Assertions.assertEquals(vectorA.getY() + vectorB.getY(), vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() + vectorB.getZ(), vectorC.getZ());
        Assertions.assertEquals(vectorA.getW() + vectorB.getW(), vectorC.getW());
        
        ///////////////////////////////////////////////////////////////////////////////
        vectorC = vectorA.subtract(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() - vectorB.getX(), vectorC.getX());
        Assertions.assertEquals(vectorA.getY() - vectorB.getY(), vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() - vectorB.getZ(), vectorC.getZ());
        Assertions.assertEquals(vectorA.getW() - vectorB.getW(), vectorC.getW());
        
        ///////////////////////////////////////////////////////////////////////////////
        float factor = 10.0f;

        vectorC = vectorA.multiply(factor);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() * factor, vectorC.getX());
        Assertions.assertEquals(vectorA.getY() * factor, vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() * factor, vectorC.getZ());
        Assertions.assertEquals(vectorA.getW() * factor, vectorC.getW());

        ///////////////////////////////////////////////////////////////////////////////
        vectorC = vectorA.divide(factor);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() / factor, vectorC.getX());
        Assertions.assertEquals(vectorA.getY() / factor, vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() / factor, vectorC.getZ());
        Assertions.assertEquals(vectorA.getW() / factor, vectorC.getW());
        
        ///////////////////////////////////////////////////////////////////////////////
        float x = vectorA.getY() * vectorB.getZ() - vectorA.getZ() * vectorB.getY();
        float y = vectorA.getZ() * vectorB.getX() - vectorA.getX() * vectorB.getZ();
        float z = vectorA.getX() * vectorB.getY() - vectorA.getY() * vectorB.getX();

        vectorC = vectorA.cross(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(x, vectorC.getX());
        Assertions.assertEquals(y, vectorC.getY());
        Assertions.assertEquals(z, vectorC.getZ());
        Assertions.assertEquals(1.0f, vectorC.getW());
        
        ///////////////////////////////////////////////////////////////////////////////
        float expectedDotProduct = vectorA.getX() * vectorB.getX() + vectorA.getY() * vectorB.getY() + vectorA.getZ() * vectorB.getZ();

        float dotProduct = vectorA.dot(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(expectedDotProduct, dotProduct);
    }

    @DisplayName("utility")
    @Test
    public void test3() {
        float[] data = {
                1.0f,
                1.0f,
                1.0f,
                4.0f,
        };

        Vector4f vector;
        Vector4f vectorB;
        
        vector = new Vector4f(data);
        
        vectorB = vector.unit();
        Assertions.assertNotEquals(vectorB, vector);
        final float vectorLength = (float)Math.sqrt(3.0);
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getX());
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getY());
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getZ());
        Assertions.assertEquals(1.0f, vectorB.getW());
        
        vectorB = vector.perspectiveDivide();
        Assertions.assertNotEquals(vectorB, vector);
        Assertions.assertEquals(vector.getX() / vector.getW(), vectorB.getX());
        Assertions.assertEquals(vector.getY() / vector.getW(), vectorB.getY());
        Assertions.assertEquals(vector.getZ() / vector.getW(), vectorB.getZ());
        Assertions.assertEquals(1.0f, vectorB.getW());
    }

    @DisplayName("arrays and buffers")
    @Test
    public void test4() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
                7.0f,
                8.0f,
        };

        Vector4f vector;
        
        vector = new Vector4f(data);
        
        float[] array = vector.asFloatArray();
        Assertions.assertNotEquals(array, data);
        Assertions.assertEquals(Vector4f.SIZE, array.length);
        Assertions.assertEquals(data[0], array[0]);
        Assertions.assertEquals(data[1], array[1]);
        Assertions.assertEquals(data[2], array[2]);
        Assertions.assertEquals(data[3], array[3]);
        
        FloatBuffer floatBuffer = FloatBuffer.allocate(Vector4f.SIZE);
        vector.copyToBuffer(floatBuffer);
        Assertions.assertEquals(Vector4f.SIZE, floatBuffer.position());
        Assertions.assertEquals(data[0], floatBuffer.get(0));
        Assertions.assertEquals(data[1], floatBuffer.get(1));
        Assertions.assertEquals(data[2], floatBuffer.get(2));
        Assertions.assertEquals(data[3], floatBuffer.get(3));

        byte[] byteArray = vector.asByteArray();
        ByteBuffer byteBuffer = ByteBuffer.allocate(Vector4f.BYTES);
        vector.copyToBuffer(byteBuffer);
        Assertions.assertEquals(byteArray.length, byteBuffer.position());
        for(int idx = 0; idx < Vector4f.BYTES; idx++) {
            Assertions.assertEquals(byteArray[idx], byteBuffer.get(idx), "bytes at position " + idx + "don't match");
        }
    }
}
