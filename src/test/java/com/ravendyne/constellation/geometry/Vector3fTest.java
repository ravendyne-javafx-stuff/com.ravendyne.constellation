package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Vector3fTest {

    @DisplayName("constructors")
    @Test
    public void test() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
        };

        Vector3f vector;

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector3f(data);
        Assertions.assertEquals(data[0], vector.getX());
        Assertions.assertEquals(data[1], vector.getY());
        Assertions.assertEquals(data[2], vector.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        vector = new Vector3f(data, Vector3f.SIZE);
        Assertions.assertEquals(data[3], vector.getX());
        Assertions.assertEquals(data[4], vector.getY());
        Assertions.assertEquals(data[5], vector.getZ());
    }

    @DisplayName("arithmetic")
    @Test
    public void test2() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
                7.0f,
                8.0f,
        };

        Vector3f vectorA;
        Vector3f vectorB;
        Vector3f vectorC;
        
        ///////////////////////////////////////////////////////////////////////////////
        vectorA = new Vector3f(data);
        vectorB = new Vector3f(data, Vector3f.SIZE);
        
        vectorC = vectorA.add(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() + vectorB.getX(), vectorC.getX());
        Assertions.assertEquals(vectorA.getY() + vectorB.getY(), vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() + vectorB.getZ(), vectorC.getZ());
        
        ///////////////////////////////////////////////////////////////////////////////
        vectorC = vectorA.subtract(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() - vectorB.getX(), vectorC.getX());
        Assertions.assertEquals(vectorA.getY() - vectorB.getY(), vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() - vectorB.getZ(), vectorC.getZ());
        
        ///////////////////////////////////////////////////////////////////////////////
        float factor = 10.0f;

        vectorC = vectorA.multiply(factor);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() * factor, vectorC.getX());
        Assertions.assertEquals(vectorA.getY() * factor, vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() * factor, vectorC.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        vectorC = vectorA.divide(factor);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(vectorA.getX() / factor, vectorC.getX());
        Assertions.assertEquals(vectorA.getY() / factor, vectorC.getY());
        Assertions.assertEquals(vectorA.getZ() / factor, vectorC.getZ());
        
        ///////////////////////////////////////////////////////////////////////////////
        float x = vectorA.getY() * vectorB.getZ() - vectorA.getZ() * vectorB.getY();
        float y = vectorA.getZ() * vectorB.getX() - vectorA.getX() * vectorB.getZ();
        float z = vectorA.getX() * vectorB.getY() - vectorA.getY() * vectorB.getX();

        vectorC = vectorA.cross(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(x, vectorC.getX());
        Assertions.assertEquals(y, vectorC.getY());
        Assertions.assertEquals(z, vectorC.getZ());
        
        ///////////////////////////////////////////////////////////////////////////////
        float expectedDotProduct = vectorA.getX() * vectorB.getX() + vectorA.getY() * vectorB.getY() + vectorA.getZ() * vectorB.getZ();

        float dotProduct = vectorA.dot(vectorB);
        Assertions.assertNotEquals(vectorC, vectorA);
        Assertions.assertEquals(expectedDotProduct, dotProduct);
    }

    @DisplayName("utility")
    @Test
    public void test3() {
        float[] data = {
                1.0f,
                1.0f,
                1.0f,
                4.0f,
        };

        Vector3f vector;
        Vector3f vectorB;
        
        vector = new Vector3f(data);
        
        vectorB = vector.normalize();
        Assertions.assertNotEquals(vectorB, vector);
        final float vectorLength = (float)Math.sqrt(3.0);
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getX());
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getY());
        Assertions.assertEquals(1.0f / vectorLength, vectorB.getZ());
    }

    @DisplayName("edge cases")
    @Test
    public void edge() {

        Vector3f vectorA = new Vector3f( 0, -1, 0 );
        Vector3f vectorB = new Vector3f( 2, 1, 0 );

        Vector3f vectorN = vectorA.cross(vectorB).normalize();
        // after cross product and normalization, X becomes -0.0
        // and it should be 0.0
        // fixed by adding 0.0f in Vector3f::divide()
        Assertions.assertEquals( 0.0f, vectorN.getX() );
        Assertions.assertEquals(Vector3f.NORMAL_Z, vectorN);
    }

    @DisplayName("arrays and buffers")
    @Test
    public void test4() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
        };

        Vector3f vector;
        
        vector = new Vector3f(data);
        
        float[] array = vector.asFloatArray();
        Assertions.assertNotEquals(array, data);
        Assertions.assertEquals(Vector3f.SIZE, array.length);
        Assertions.assertEquals(data[0], array[0]);
        Assertions.assertEquals(data[1], array[1]);
        Assertions.assertEquals(data[2], array[2]);
        
        FloatBuffer floatBuffer = FloatBuffer.allocate(Vector3f.SIZE);
        vector.copyToBuffer(floatBuffer);
        Assertions.assertEquals(Vector3f.SIZE, floatBuffer.position());
        Assertions.assertEquals(data[0], floatBuffer.get(0));
        Assertions.assertEquals(data[1], floatBuffer.get(1));
        Assertions.assertEquals(data[2], floatBuffer.get(2));

        byte[] byteArray = vector.asByteArray();
        ByteBuffer byteBuffer = ByteBuffer.allocate(Vector3f.BYTES);
        vector.copyToBuffer(byteBuffer);
        Assertions.assertEquals(byteArray.length, byteBuffer.position());
        for(int idx = 0; idx < Vector3f.BYTES; idx++) {
            Assertions.assertEquals(byteArray[idx], byteBuffer.get(idx), "bytes at position " + idx + "don't match");
        }
    }
}
