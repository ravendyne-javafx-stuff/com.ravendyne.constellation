package com.ravendyne.constellation.geometry;

import java.nio.DoubleBuffer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.ravendyne.constellation.geometry.BufferVector2d;

public class BufferVector2dTest {

    @Test
    public void test() {
        double[] data = {
                1.0,
                2.0,
                3.0,
                4.0
        };
        DoubleBuffer bufferVectors = DoubleBuffer.wrap(data);
        // so we don't mess with current r/w position of the original buffer
        DoubleBuffer vectors = bufferVectors.duplicate();

        BufferVector2d v1 = BufferVector2d.newInstance(vectors);
        // skip number of elements (doubles) equal to the size of BufferVector2d 
        vectors.position(vectors.position() + BufferVector2d.SIZE);
        BufferVector2d v2 = BufferVector2d.newInstance(vectors);
        
        Assertions.assertEquals(data[0], v1.getX());
        Assertions.assertEquals(data[1], v1.getY());

        Assertions.assertEquals(data[2], v2.getX());
        Assertions.assertEquals(data[3], v2.getY());

        Assertions.assertEquals(0, v1.position);
        Assertions.assertEquals(2, v2.position);
    }
}
