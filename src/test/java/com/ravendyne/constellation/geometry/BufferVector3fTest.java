package com.ravendyne.constellation.geometry;

import java.nio.FloatBuffer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BufferVector3fTest {

    @Test
    public void test() {
        float[] data = {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f,
                6.0f,
        };
        FloatBuffer bufferVectors = FloatBuffer.wrap(data);
        // so we don't mess with current r/w position of the original buffer
        FloatBuffer vectors = bufferVectors.duplicate();

        BufferVector3f v1 = BufferVector3f.newInstance(vectors);
        // skip number of elements (floats) equal to the size of BufferVector3f
        vectors.position(vectors.position() + BufferVector3f.SIZE);
        BufferVector3f v2 = BufferVector3f.newInstance(vectors);
        
        Assertions.assertEquals(data[0], v1.getX());
        Assertions.assertEquals(data[1], v1.getY());
        Assertions.assertEquals(data[2], v1.getZ());

        Assertions.assertEquals(data[3], v2.getX());
        Assertions.assertEquals(data[4], v2.getY());
        Assertions.assertEquals(data[5], v2.getZ());

        Assertions.assertEquals(0, v1.position);
        Assertions.assertEquals(3, v2.position);
        
        v1.copyTo(v2);
        Assertions.assertEquals(v1.getX(), v2.getX());
        Assertions.assertEquals(v1.getY(), v2.getY());
        Assertions.assertEquals(v1.getZ(), v2.getZ());
    }
}
