package com.ravendyne.constellation.geometry;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Matrix4fTest {
    private static float[] IDENTITY = new float[] {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
       };

    @DisplayName("constructors")
    @Test
    public void constructors() {
        float[] matrixData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] otherData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] result = {
                90.0f, 100.0f, 110.0f, 120.0f,
                202.0f, 228.0f, 254.0f, 280.0f,
                314.0f, 356.0f, 398.0f, 440.0f,
                426.0f, 484.0f, 542.0f, 600.0f,
        };
        
        Matrix4f matrix;
        
        ///////////////////////////////////////////////////////////////////////////////
        matrix = new Matrix4f();
        Assertions.assertArrayEquals(IDENTITY, matrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        matrix = new Matrix4f().fromArray(matrixData);
        Assertions.assertArrayEquals(matrixData, matrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        matrix = new Matrix4f(
                90.0f, 100.0f, 110.0f, 120.0f,
                202.0f, 228.0f, 254.0f, 280.0f,
                314.0f, 356.0f, 398.0f, 440.0f,
                426.0f, 484.0f, 542.0f, 600.0f
            );
        Assertions.assertArrayEquals(result, matrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f otherMatrix = new Matrix4f().fromArray(otherData);

        matrix = new Matrix4f(otherMatrix);
        Assertions.assertArrayEquals(otherData, matrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        final int arrayOffset = 7;
        float[] offsetArray = new float[arrayOffset + matrixData.length];
        System.arraycopy(matrixData, 0, offsetArray, arrayOffset, matrixData.length);

        matrix = new Matrix4f().fromArray(offsetArray, arrayOffset);
        Assertions.assertArrayEquals(matrixData, matrix.toArray());
    }

    @DisplayName("matrix")
    @Test
    public void matrix() {
        float[] matrixData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] transposedMatrix = {
                1.0f, 5.0f, 9.0f,  13.0f,
                2.0f, 6.0f, 10.0f, 14.0f,
                3.0f, 7.0f, 11.0f, 15.0f,
                4.0f, 8.0f, 12.0f, 16.0f,
        };

        Matrix4f matrix = new Matrix4f().fromArray(matrixData);
        Matrix4f transposed = matrix.transpose();
        Assertions.assertNotEquals(matrix, transposed);
        Assertions.assertArrayEquals(transposedMatrix, transposed.toArray());
    }

    @DisplayName("math")
    @Test
    public void math() {
        float[] matrixData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] vectorData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] result = {
                90.0f, 100.0f, 110.0f, 120.0f,
                202.0f, 228.0f, 254.0f, 280.0f,
                314.0f, 356.0f, 398.0f, 440.0f,
                426.0f, 484.0f, 542.0f, 600.0f,
        };
        
        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f matrix = new Matrix4f().fromArray(matrixData);
        Vector4f vector = new Vector4f(vectorData, 0, 4);

        Vector4f resultVector = matrix.multiply(vector);
        Assertions.assertEquals(result[0], resultVector.getX());
        Assertions.assertEquals(result[4], resultVector.getY());
        Assertions.assertEquals(result[8], resultVector.getZ());
        Assertions.assertEquals(result[12], resultVector.getW());

        ///////////////////////////////////////////////////////////////////////////////
        Assertions.assertEquals(matrix.multiply(new Vector3f(5, 5, 5)), matrix.multiply(new Vector4f(5, 5, 5, 1)));

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f otherMatrix = new Matrix4f().fromArray(vectorData);

        Matrix4f multipliedMatrix = matrix.multiply(otherMatrix);
        Assertions.assertNotEquals(matrix, multipliedMatrix);
        Assertions.assertNotEquals(otherMatrix, multipliedMatrix);
        Assertions.assertArrayEquals(result, multipliedMatrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        final float scalar = 2.0f;
        float[] originalMatrixArray = matrix.toArray();

        multipliedMatrix = matrix.multiply(scalar);
        Assertions.assertNotEquals(matrix, multipliedMatrix);

        float[] scalarMultiplyResult = multipliedMatrix.toArray();
        for(int idx = 0; idx < scalarMultiplyResult.length; idx++) {
            Assertions.assertEquals(scalarMultiplyResult[ idx ], originalMatrixArray[ idx ] * scalar);
        }
    }

    @DisplayName("other")
    @Test
    public void other() {
        float[] matrixData = {
                1.0f,  2.0f,  3.0f,  4.0f,
                5.0f,  6.0f,  7.0f,  8.0f,
                9.0f,  10.0f, 11.0f, 12.0f,
                13.0f, 14.0f, 15.0f, 16.0f,
        };

        float[] array;

        Matrix4f matrix = new Matrix4f().fromArray(matrixData);

        ///////////////////////////////////////////////////////////////////////////////
        Assertions.assertArrayEquals(matrixData, matrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        array = new float[Matrix4f.SIZE];
        matrix.toArray(array);
        Assertions.assertArrayEquals(matrixData, array);

        ///////////////////////////////////////////////////////////////////////////////
        float[] matrixArray = matrix.toArray();
        final int offset = 9;
        array = new float[offset + Matrix4f.SIZE];

        matrix.toArray(array, offset);
        for(int idx = 0; idx < matrixArray.length; idx ++) {
            Assertions.assertEquals(matrixArray[ idx ], array[ offset + idx ], "elements at " + idx + "are different: expected<" + matrixArray[ idx ] + ">, got <" + array[ offset + idx ] + ">");
        }
    }

    @DisplayName("utilities")
    @Test
    public void utilities() {
        Vector4f originalVector = new Vector4f(1, 1, 1);
        Vector4f translationVector = new Vector4f(1, 2, 3);
        Vector3f scalingVector = new Vector3f(1, 2, 3);
        Vector3f shearVectorX = new Vector3f(2, 0, 0);
        Vector3f shearVectorY = new Vector3f(0, 2, 0);
        Vector3f shearVectorZ = new Vector3f(0, 0, 2);

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f identityMatrix = Matrix4f.identity();
        Assertions.assertArrayEquals(IDENTITY, identityMatrix.toArray());

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f translationMatrix = Matrix4f.translation(
                translationVector.getX(),
                translationVector.getY(),
                translationVector.getZ()
                );

        Vector4f result = translationMatrix.multiply( originalVector );
        Assertions.assertTrue( result.asVector3f().equals( originalVector.add(translationVector).asVector3f() ) );

        ///////////////////////////////////////////////////////////////////////////////
        Assertions.assertTrue( Matrix4f.translation( new Vector3f( 1, 2, 3 ) ).equals( Matrix4f.translation( 1, 2, 3 ) )  );

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f scalingMatrix = Matrix4f.scale(
                scalingVector.getX(),
                scalingVector.getY(),
                scalingVector.getZ()
                );

        result = scalingMatrix.multiply( originalVector );
        Assertions.assertEquals(originalVector.getX() * scalingVector.getX(), result.getX());
        Assertions.assertEquals(originalVector.getY() * scalingVector.getY(), result.getY());
        Assertions.assertEquals(originalVector.getZ() * scalingVector.getZ(), result.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        Assertions.assertTrue( Matrix4f.scale( new Vector3f( 1, 2, 3 ) ).equals( Matrix4f.scale( 1, 2, 3 ) )  );

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f shearMatrixX = Matrix4f.shear(
                shearVectorX.getX(),
                shearVectorX.getY(),
                shearVectorX.getZ()
                );

        result = shearMatrixX.multiply( originalVector );
        Assertions.assertEquals(originalVector.getX(), result.getX());
        Assertions.assertEquals(originalVector.getY() + shearVectorX.getX() * originalVector.getX(), result.getY());
        Assertions.assertEquals(originalVector.getZ() + shearVectorX.getX() * originalVector.getX(), result.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f shearMatrixY = Matrix4f.shear(
                shearVectorY.getX(),
                shearVectorY.getY(),
                shearVectorY.getZ()
                );

        result = shearMatrixY.multiply( originalVector );
        Assertions.assertEquals(originalVector.getX() + shearVectorY.getY() * originalVector.getY(), result.getX());
        Assertions.assertEquals(originalVector.getY(), result.getY());
        Assertions.assertEquals(originalVector.getZ() + shearVectorY.getY() * originalVector.getY(), result.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        Matrix4f shearMatrixZ = Matrix4f.shear(
                shearVectorZ.getX(),
                shearVectorZ.getY(),
                shearVectorZ.getZ()
                );

        result = shearMatrixZ.multiply( originalVector );
        Assertions.assertEquals(originalVector.getX() + shearVectorZ.getZ() * originalVector.getZ(), result.getX());
        Assertions.assertEquals(originalVector.getY() + shearVectorZ.getZ() * originalVector.getZ(), result.getY());
        Assertions.assertEquals(originalVector.getZ(), result.getZ());

        ///////////////////////////////////////////////////////////////////////////////
        Assertions.assertTrue( Matrix4f.shear( new Vector3f( 1, 2, 3 ) ).equals( Matrix4f.shear( 1, 2, 3 ) )  );
    }
}
