package com.ravendyne.constellation.ode;


public class ReferenceGravity {

    public ReferenceGravity() {
    }
    
    public double value(double time) {
        // d = g x t^2 / 2
        return 9.81 * time * time / 2;
    }

    static double speed( double time ) {
        // v = g x t
        return 9.81 * time;
    }
}
