package com.ravendyne.constellation.ode;


/**
 * http://user.engineering.uiowa.edu/~sxiao/class/058-153/lecture-2.pdf
 */
public class ReferenceSpring {
    double Ks;
    double Kd;
    double m;
    double length;
    double springRootPosition;

    double initialDisplacementU0;
    double initialSpeedV0;
    double omega0;
    double initialSpeedDisplacement;

    double omegaD;
    double Y;
    double dampingRatio;
    double theta;

    /**
     * Spring fixed end position is X = 0, Y = {@code springRootPosition},
     * and it's rest length is {@code length}.
     * 
     * @param springRootPosition Y coordinate of the fixed spring end
     * @param K stiffness
     * @param length spring rest length
     * @param m mass on the other end
     */
    public ReferenceSpring(double springRootPosition, double length, double Ks, double Kd, double initialPosition, double m) {
        this.Ks = Ks;
        this.Kd = Kd;
        this.m = m;
        this.length = length;
        this.springRootPosition = springRootPosition;

        
        // we first translate initial positin relative to spring root
        //  * initialPosition - springRootPosition
        // we then multiply by -1 since the system is upside-down relative to Y-axis direction
        // i.e. the mass is below spring root when Y-axis is oriented vertically up
        // we then calculate displacement
        //  * (initialPosition - springRootPosition) - length
        initialDisplacementU0 = (-1 * (initialPosition - springRootPosition)) - length;
        initialSpeedV0 = 0.0;

        omega0 = Math.sqrt( Ks / m );
        initialSpeedDisplacement = initialSpeedV0 / omega0;



        // critical damping: eta == 1
        // over damped: eta > 1
        // under or lightly damped system: 0 < eta < 1
        dampingRatio = Kd / ( 2.0 * m * omega0 );

        if(dampingRatio >= 1) throw new IllegalArgumentException("spring system has to be uder damped (eta < 1)");
        //
        // under/lightly damped system
        //
        if(dampingRatio > 0.0) {
            omegaD = omega0 * Math.sqrt( 1 - dampingRatio * dampingRatio);
            final double term = ( dampingRatio * omega0 * initialDisplacementU0 + initialSpeedV0 ) / omegaD;
            theta = Math.atan( initialDisplacementU0 / term );
            Y = Math.sqrt( initialDisplacementU0 * initialDisplacementU0 + term * term );
        }
        
    }

    public double value(double time) {
        double value = 0.0;
        if(dampingRatio == 0.0) {
            // undamped
            value = initialDisplacementU0 * Math.cos( omega0 * time ) - initialSpeedDisplacement * Math.sin( omega0 * time );
        } else {
            // under damped
            double exp = Math.exp( - dampingRatio * omega0 * time );
            value = exp * Y * Math.sin( omegaD * time + theta);
        }

        // reverse the coordinate direction and translate relative to spring root position
        return ((-1 * value) + springRootPosition) * (-1);
    }

}
