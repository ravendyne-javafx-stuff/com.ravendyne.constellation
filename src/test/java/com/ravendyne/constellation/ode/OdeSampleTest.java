package com.ravendyne.constellation.ode;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.ode.solver.api.ISolver;
import com.ravendyne.constellation.ode.solver.impl.MidpointSolver;
import com.ravendyne.constellation.simulation.force.kind.SpringForceField;
import com.ravendyne.constellation.simulation.force.kind.UniformGravityForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;
import com.ravendyne.constellation.simulation.particles.system.ArrayListVertexSystemImpl;

public class OdeSampleTest {

    final static int    ITERATIONS    = 500;
    final static double DELTA         = 0.05;
    final static int    NUM_PARTICLES = 1;

    final static double Ks = 1.0;
    final static double Kd = 0.2;
//    final static double Kd = 0.0;
    final static double mass = 1.0;
    final static double length = 2.0;
    final static double initialPosition = 0.0;
    final static double springRootPosition = 1.0;

    final static boolean printCSV = true;
    
    public static void main( String[] args ) {

        runSolver();
    }

    private static void runSolver() {

        ArrayListVertexSystemImpl system = new ArrayListVertexSystemImpl();
        populate( system );

        ISolver solver = new MidpointSolver();
//        ISolver solver = new EulerSolver();
        ReferenceSpring spring = new ReferenceSpring( springRootPosition, length, Ks, Kd, initialPosition, mass );

        System.out.println( "Start..." );
        long startTime = System.currentTimeMillis();
        if(printCSV) printCSVHeader();
        for ( int i = 0; i < ITERATIONS; i++ ) {
            solver.applyStep( system, DELTA );

            if(printCSV) {
                double time = system.getTime();
                double Yt = spring.value( time );// A * Math.cos( omega * time + phi) - springRootPosition;
                int timestamp = ( int ) (time * 1000);
                system.printPositionSpeedCSV( Yt, timestamp );
            }
        }
        long deltaTime = System.currentTimeMillis() - startTime;

        System.out.println( "iterations: " + ITERATIONS );
        System.out.println( "particles: " + NUM_PARTICLES );

        System.out.println( "Total: " + deltaTime + "ms" );
        System.out.println( "Per iteration: " + deltaTime / ITERATIONS + "ms" );

        System.out.println( "Per second @ 30fps: " + deltaTime * 30.0 / ITERATIONS + "ms" );
        System.out.println( "# of seconds @ 30fps: " + ITERATIONS / 30.0 + "s" );
        System.out.println( "FPS from delta: " + 1.0 / DELTA + " FPS" );

        System.out.println( "Done." );
    }

    private static void printCSVHeader() {
        System.out.println( "timestamp,posx,posy,vx,vy,Yt,error" );
    }

    private static void populate( ArrayListVertexSystemImpl system ) {

        for ( int i = 0; i < NUM_PARTICLES; i++ ) {
            system.addVertex( system.newVertex( new Vector2d( 0, initialPosition ), Vector2d.ZERO, mass ) );
        }

        system.addForceField(new UniformGravityForceField());
        // this particle is there just to anchor the spring
        // it should not be affected by calculations so we don't add it to particles set.
        IVertex springRootParticle = system.newVertex( new Vector2d( 0, springRootPosition ), Vector2d.ZERO, 0.0 );
        system.addForceField( new SpringForceField( springRootParticle, length, Ks, Kd ) );

        // this updates initial forces on all particles
        system.advanceTime( 0.0 );
    }
}
