package com.ravendyne.constellation.graph.generic.gml;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GmlBuilderTest {

    @DisplayName("number")
    @Test
    public void number() {

        GmlValueBuilder number = GmlBuilder.newNumberValue();

        ///////////////////////////////////////////////////////////////////
        // IDENTITY
        Assertions.assertTrue(number.isNumber());
        Assertions.assertFalse(number.isString());
        Assertions.assertFalse(number.isMap());
        Assertions.assertFalse(number.isList());

        ///////////////////////////////////////////////////////////////////
        // SETTER
        number.setNumber(10);
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            number.setString(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            number.setMapItem(null, null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            number.addListItem(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            number.addAllListItems(null);
        });

        ///////////////////////////////////////////////////////////////////
        // GETTER
        Assertions.assertNotNull(number.getNumber());
        Assertions.assertNull(number.getString());
        Assertions.assertNull(number.getMap());
        Assertions.assertNull(number.getList());
        
        ///////////////////////////////////////////////////////////////////
        // VALUE
        Assertions.assertEquals(10, number.getNumber());
    }

    @DisplayName("string")
    @Test
    public void string() {

        GmlValueBuilder string = GmlBuilder.newStringValue();

        ///////////////////////////////////////////////////////////////////
        // IDENTITY
        Assertions.assertFalse(string.isNumber());
        Assertions.assertTrue(string.isString());
        Assertions.assertFalse(string.isMap());
        Assertions.assertFalse(string.isList());

        ///////////////////////////////////////////////////////////////////
        // SETTER
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            string.setNumber(null);
        });
        string.setString("test");
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            string.setMapItem(null, null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            string.addListItem(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            string.addAllListItems(null);
        });

        ///////////////////////////////////////////////////////////////////
        // GETTER
        Assertions.assertNull(string.getNumber());
        Assertions.assertNotNull(string.getString());
        Assertions.assertNull(string.getMap());
        Assertions.assertNull(string.getList());
        
        ///////////////////////////////////////////////////////////////////
        // VALUE
        Assertions.assertEquals("test", string.getString());
    }

    @DisplayName("map")
    @Test
    public void map() {

        GmlValueBuilder map = GmlBuilder.newMapValue();

        ///////////////////////////////////////////////////////////////////
        // IDENTITY
        Assertions.assertFalse(map.isNumber());
        Assertions.assertFalse(map.isString());
        Assertions.assertTrue(map.isMap());
        Assertions.assertFalse(map.isList());

        ///////////////////////////////////////////////////////////////////
        // SETTER
        GmlValueBuilder aNumberValue = GmlBuilder.newNumberValue();
        aNumberValue.setNumber(43);

        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            map.setNumber(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            map.setString(null);
        });
        map.setMapItem("test", aNumberValue);
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            map.addListItem(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            map.addAllListItems(null);
        });

        ///////////////////////////////////////////////////////////////////
        // GETTER
        Assertions.assertNull(map.getNumber());
        Assertions.assertNull(map.getString());
        Assertions.assertNotNull(map.getMap());
        Assertions.assertNull(map.getList());

        ///////////////////////////////////////////////////////////////////
        // VALUE
        Assertions.assertEquals(aNumberValue, map.getMap().get("test"));
        Assertions.assertEquals(43, map.getMap().get("test").getNumber());
    }

    @DisplayName("list")
    @Test
    public void list() {

        GmlValueBuilder list = GmlBuilder.newListValue();

        ///////////////////////////////////////////////////////////////////
        // IDENTITY
        Assertions.assertFalse(list.isNumber());
        Assertions.assertFalse(list.isString());
        Assertions.assertFalse(list.isMap());
        Assertions.assertTrue(list.isList());

        ///////////////////////////////////////////////////////////////////
        // SETTER
        GmlValueBuilder aNumberValue = GmlBuilder.newNumberValue();
        aNumberValue.setNumber(43);

        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            list.setNumber(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            list.setString(null);
        });
        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            list.setMapItem(null, null);
        });
        list.addListItem(aNumberValue);

        GmlValueBuilder aList = GmlBuilder.newListValue();
        aList.addListItem(GmlBuilder.newNumberValue(2));
        aList.addListItem(GmlBuilder.newNumberValue(3));
        list.addAllListItems(aList);

        ///////////////////////////////////////////////////////////////////
        // GETTER
        Assertions.assertNull(list.getNumber());
        Assertions.assertNull(list.getString());
        Assertions.assertNull(list.getMap());
        Assertions.assertNotNull(list.getList());

        ///////////////////////////////////////////////////////////////////
        // VALUE
        Assertions.assertEquals(3, list.getList().size());
        Assertions.assertEquals(aNumberValue, list.getList().get(0));
        Assertions.assertEquals(43, list.getList().get(0).getNumber());
        Assertions.assertEquals(2, list.getList().get(1).getNumber());
        Assertions.assertEquals(3, list.getList().get(2).getNumber());
    }
    
    @DisplayName("graph")
    @Test
    public void test() {
        ///////////////////////////////////////////////////////////////////
        // GENERATE GML output into a PrintStream

        // GRAPH
        GmlValueBuilder graph = GmlBuilder.newMapValue();
        graph.setMapItem("width", GmlBuilder.newNumberValue(1024));
        graph.setMapItem("height", GmlBuilder.newNumberValue(768));

        // NODE items
        GmlValueBuilder nodesList = GmlBuilder.newListValue();
        GmlValueBuilder node1 = GmlBuilder.newMapValue();
        node1.setMapItem("id", GmlBuilder.newNumberValue(1));
        GmlValueBuilder node2 = GmlBuilder.newMapValue();
        node2.setMapItem("id", GmlBuilder.newNumberValue(2));

        nodesList.addListItem(node1);
        nodesList.addListItem(node2);
        graph.setMapItem("node", nodesList);

        // EDGE items
        GmlValueBuilder edgesList = GmlBuilder.newListValue();
        GmlValueBuilder edge12 = GmlBuilder.newMapValue();
        edge12.setMapItem("source", GmlBuilder.newNumberValue(1));
        edge12.setMapItem("target", GmlBuilder.newNumberValue(2));

        edgesList.addListItem(edge12);
        graph.setMapItem("edge", edgesList);
        
        // a way to save output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos, true);
        GmlBuilder.save(graph, out);


        ///////////////////////////////////////////////////////////////////
        // PARSE to check generated output

        // parse from generated string
        Gml gml = new Gml(baos.toString());
        // should parse without errors
        Assertions.assertTrue(gml.getErrorMessage().isEmpty());
        
        // GRAPH
        Map<String, GmlValue> map = gml.get().get("graph").getMap();
        // graph attrs
        Assertions.assertNotNull(map.get("width"));
        Assertions.assertNotNull(map.get("height"));
        Assertions.assertEquals(1024, map.get("width").getNumber());
        Assertions.assertEquals(768, map.get("height").getNumber());
        
        // NODES
        Assertions.assertNotNull(map.get(Gml.NODE_LIST_TAG));
        Assertions.assertTrue(map.get(Gml.NODE_LIST_TAG).isList());

        List<GmlValue> nodes = map.get(Gml.NODE_LIST_TAG).getList();
        Assertions.assertEquals(2, nodes.size());

        // first node
        Assertions.assertTrue(nodes.get(0).isMap());
        // get attr 'id' value
        int node1Id = nodes.get(0).getMap().get("id").getNumber().intValue();
        // 'id' should be either 1 or 2
        Assertions.assertTrue(node1Id == 1 || node1Id == 2);

        // second node
        Assertions.assertTrue(nodes.get(1).isMap());
        // get attr 'id' value
        int node2Id = nodes.get(1).getMap().get("id").getNumber().intValue();
        // the other node's 'id' should be 2 if the first is 1 and vice versa
        if(node1Id == 1) {
            Assertions.assertTrue(node2Id == 2);
        }
        if(node1Id == 2) {
            Assertions.assertTrue(node2Id == 1);
        }

        // EDGES
        Assertions.assertNotNull(map.get(Gml.EDGE_LIST_TAG));
        Assertions.assertTrue(map.get(Gml.EDGE_LIST_TAG).isList());

        List<GmlValue> edges = map.get(Gml.EDGE_LIST_TAG).getList();
        Assertions.assertEquals(1, edges.size());

        // first (and the only) edge
        Assertions.assertTrue(edges.get(0).isMap());
        // get attr 'source' value
        int sourceId = edges.get(0).getMap().get("source").getNumber().intValue();
        Assertions.assertTrue(sourceId == 1);
        // get attr 'target' value
        int targetId = edges.get(0).getMap().get("target").getNumber().intValue();
        Assertions.assertTrue(targetId == 2);
    }
}
