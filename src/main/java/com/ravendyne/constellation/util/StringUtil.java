package com.ravendyne.constellation.util;

import java.util.Arrays;

public final class StringUtil {
    private StringUtil() {}
    
    public static void splitString( String value, int column ) {
        // The regex matches an empty string that has the last match (\G) followed by N characters (.{N}) before it ((?<= ))
        Arrays.spliterator( value.split(String.format("(?<=\\G.{%1$d})", column ) ) ).forEachRemaining(System.out::println);
    }
}
