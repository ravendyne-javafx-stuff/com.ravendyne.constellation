package com.ravendyne.constellation.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public class ClassUtil {
    
    public static Set< String > findClassFiles(Path path) {
        Set<String> classNames = new HashSet<>();

        try {
            Files.walk(path)
            .filter(Files::isRegularFile)
            .filter((p) -> p.toString().endsWith( ".class" ))
            .forEach((p) -> {
                classNames.add(path.relativize(p).toString());
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classNames;
    }
}
