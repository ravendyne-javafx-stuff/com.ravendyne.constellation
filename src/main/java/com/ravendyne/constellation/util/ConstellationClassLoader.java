package com.ravendyne.constellation.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class ConstellationClassLoader extends ClassLoader {

    Path path;
    Map<String, Path> classLocations;

    public ConstellationClassLoader( String folderName ) {
        path = Paths.get( folderName );
        path = path.toAbsolutePath().normalize();
    }

    @Override
    protected Class<?> findClass(String name)
            throws ClassNotFoundException {
        Class<?> clazz = null;
        try {
            byte[] b = loadClassData(name);
            clazz = defineClass(name, b, 0, b.length);
        } catch (IOException e) {
            throw new ClassNotFoundException(name);
        }
        
        return clazz;
        
    }

    private byte[] loadClassData(String name) throws ClassNotFoundException, IOException {
        // load the class data from the connection
        Path classFilePath = path.resolve(name.replace(".", "/"));
        if(!classFilePath.toFile().exists()) {
            throw new ClassNotFoundException(name);
        }
        byte[] data = Files.readAllBytes(classFilePath);
        return data;
    }

}
