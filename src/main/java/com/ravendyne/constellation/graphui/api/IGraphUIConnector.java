package com.ravendyne.constellation.graphui.api;

import com.ravendyne.constellation.geometry.Vector2d;

public interface IGraphUIConnector {
    /** Updates connector position based on position of its start and end nodes. */
    void updatePosition();
    /** Explicitly set positions of connector's start and end points. */
    void setPosition( Vector2d start, Vector2d end );
    IGraphUINode getStartNode();
    IGraphUINode getEndNode();
    /** Returns and object representing this graph connector in an implementation specific way */
    Object getNode();
}
