package com.ravendyne.constellation.graphui.api;

import com.ravendyne.constellation.geometry.Vector2d;

public interface IGraphUI {
    enum ConnectorType {
        Line,
        Cubic,
        Quadratic
    }
    
    enum NodeType {
        Origin,
        Anchor,
        Box
    }

    /**
     * Creates a new node instance that can be rendered by this implementation.
     * A node created by this method will only be rendered in UI if it is added via {@link #addNode(IGraphUINode)}.
     * @param type node type to be created
     * @param label label for the node
     * @return node instance that can be rendered by this implementation.
     */
    IGraphUINode newNode(NodeType type, String label);
    /**
     * Creates a new connector instance that can be rendered by this implementation.
     * A connector created by this method will only be rendered in UI if it is added via {@link #addConnector(IGraphUIConnector)}.
     * @param type connector type
     * @param label label for the connector
     * @param sourceNode source node
     * @param targetNode target node
     * @return connector instance that can be rendered by this implementation.
     */
    IGraphUIConnector newConnector(ConnectorType type, String label, IGraphUINode sourceNode, IGraphUINode targetNode);

    /** Adds a node to be represented in UI */
    IGraphUINode addNode(IGraphUINode node);
    /** Adds a connection to be represented in UI */
    IGraphUIConnector addConnector(IGraphUIConnector connector);

    /** Returns nodes that have UI representation in this graph.
     * Some nodes (i.e. anchors) may not be represented in UI and those should
     * not be accessible through {@link Iterable} returned by this method. */
    Iterable< IGraphUINode > getNodes();
    /** Returns connectors that have UI representation in this graph.
     * Some nodes (i.e. connectors to/from anchor nodes) may not be represented in
     * UI and those should not be accessible through {@link Iterable} returned 
     * by this method. */
    Iterable< IGraphUIConnector > getConnectors();
    
    Vector2d getCanvasSize();

    /**
     * Provides access to <b>all</b> nodes and connectors in the graph.
     * Some of the nodes and/or connectors may not be displayed in UI
     * (i.e. anchor points, connectors to anchor points etc.) and those
     * are not accessible via {@link #getNodes()} and {@link #getConnectors()}
     * methods. This method returns <b>all</b> the nodes/connectors.
     * 
     * @return interface to access all the nodes and connectors that comprise a graph
     * being rendered by this implementation
     */
    INodesConnectorsProvider getAllNodesConnectors();
}
