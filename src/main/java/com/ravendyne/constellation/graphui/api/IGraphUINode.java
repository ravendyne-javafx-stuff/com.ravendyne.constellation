package com.ravendyne.constellation.graphui.api;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;

public interface IGraphUINode {
    /** Sets the node position in graph representation space. */
    void setPosition(Vector2d position);
    /** Returns node position in graph representation space. */
    Vector2d getPosition();
    /** Node type. */
    NodeType getType();
    /** Returns and object representing this graph node in an implementation specific way */
    Object getNode();
}
