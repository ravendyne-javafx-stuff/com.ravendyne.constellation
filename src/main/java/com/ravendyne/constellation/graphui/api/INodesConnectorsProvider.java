package com.ravendyne.constellation.graphui.api;

public interface INodesConnectorsProvider {

    Iterable< IGraphUINode > getNodes();

    Iterable< IGraphUIConnector > getConnectors();

}
