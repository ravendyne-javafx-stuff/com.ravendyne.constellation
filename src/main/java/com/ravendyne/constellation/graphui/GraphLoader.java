package com.ravendyne.constellation.graphui;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graph.generic.gml.GmlUtil;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.IGraphEdgeConsumer;
import com.ravendyne.constellation.graph.generic.gml.IGraphNodeConsumer;
import com.ravendyne.constellation.graphui.api.IGraphUI;
import com.ravendyne.constellation.graphui.api.IGraphUIConnector;
import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.graphui.api.INodesConnectorsProvider;
import com.ravendyne.constellation.graphui.api.IGraphUI.ConnectorType;
import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;

/**
 * Loads a graph definition from GML file into an {@link IGraphUI} implementation instance.
 * <p>
 * Maintains lists of all nodes and connectors.
 * </p>
 * <p>
 * GML nodes that have attribute {@code type} set to {@code "origin"} will be created as
 * {@link IGraphUI.NodeType.Origin NodeType.Origin} and the ones with {@code type} set 
 * to {@code "anchor"} will be created as {@link IGraphUI.NodeType.Anchor NodeType.Anchor}
 * nodes. Default node type is {@link IGraphUI.NodeType.Box NodeType.Box}.
 * </p>
 * <p>
 * Loader also picks up {@code label} attributes for nodes and edges and {@code x} and {@code y}
 * attributes for nodes.
 * </p>
 *
 */
public class GraphLoader implements INodesConnectorsProvider {
    public static final String GML_ATTR_LABEL = "label";
    public static final String GML_NODE_ATTR_TYPE = "type";
    public static final String GML_NODE_ATTR_X = "x";
    public static final String GML_NODE_ATTR_Y = "y";

    public static final String GML_NODE_TYPE_ORIGIN = "origin";
    public static final String GML_NODE_TYPE_ANCHOR = "anchor";

    private IGraphUI graphUI;
    private Set<IGraphUINode> allNodes;
    private Set<IGraphUIConnector> allConnectors;
    private boolean ignoreAnchorConnectors;
    private ConnectorType connectorType;

    public GraphLoader(IGraphUI graphUI) {
        this.graphUI = graphUI;
        allNodes = new HashSet<>();
        allConnectors = new HashSet<>();

        ignoreAnchorConnectors = true;
        connectorType = ConnectorType.Line;
    }

    public void setConnectorType(ConnectorType connectorType) {
        this.connectorType = connectorType;
    }

    public void setIgnoreAnchorConnectors(boolean ignoreAnchorConnectors) {
        this.ignoreAnchorConnectors = ignoreAnchorConnectors;
    }

    @Override
    public Iterable< IGraphUINode > getNodes() {
        return allNodes;
    }

    @Override
    public Iterable< IGraphUIConnector > getConnectors() {
        return allConnectors;
    }

    public void load(List<String> gmlSource) {
        Map<String, GmlValue> graphAsGmlValues = GmlUtil.loadGml(gmlSource);
        load(graphAsGmlValues);
    }

    public void load(Map<String, GmlValue> graph) {
        loadGraph(graph);
    }
    
    private void loadGraph(Map<String, GmlValue> graph) {
        Map<String, IGraphUINode> nodesMap = new HashMap<>();
        Set<IGraphUIConnector> connectorSet = new HashSet<>();
        
        Consumer<Map<String, GmlValue>> graphAttrsConsumer = (attrs) -> {
            System.out.println("graph attrs: " + attrs);
        };

        // create boxes
        IGraphNodeConsumer nodeConsumer = (id, attrs) -> {
            String label = attrs.get(GML_ATTR_LABEL).getString();

            String type = "";
            GmlValue typeValue = attrs.get(GML_NODE_ATTR_TYPE);
            if(typeValue != null) {
                type = typeValue.getString();
            }

            IGraphUINode node;

            if(type.equals(GML_NODE_TYPE_ORIGIN)) {
                node = graphUI.newNode( NodeType.Origin, label );
            } else if(type.equals(GML_NODE_TYPE_ANCHOR)) {
                node = graphUI.newNode( NodeType.Anchor, label );
            } else {
                node = graphUI.newNode( NodeType.Box, label );
            }


            Vector2d position = Vector2d.random(500.0);
            double x = position.x;
            double y = position.y;

            GmlValue xVal = attrs.get(GML_NODE_ATTR_X);
            if(xVal != null) {
                x = xVal.getNumber().doubleValue();
            }
            GmlValue yVal = attrs.get(GML_NODE_ATTR_Y);
            if(yVal != null) {
                y = yVal.getNumber().doubleValue();
            }

            position = new Vector2d(x, y);


            node.setPosition( position );
            nodesMap.put(id, node);
            allNodes.add(node);
//            System.out.println(id + " -> " + node);
        };
        
        IGraphNodeConsumer isolatedNodeConsumer = (x, attrs) -> {
            // ignore isolated nodes
            System.out.println("isolated node ignored: " + attrs);
        };

        // create lines
        IGraphEdgeConsumer edgeConsumer = (source, target, attrs) -> {
            IGraphUINode sourceNode = nodesMap.get(source);
            IGraphUINode targetNode = nodesMap.get(target);

            String label = null;
            GmlValue labelValue = attrs.get( GML_ATTR_LABEL );
            if(labelValue != null) {
                label = labelValue.getString();
            }
            IGraphUIConnector connector = graphUI.newConnector(connectorType, label, sourceNode, targetNode);
            connector.updatePosition();
            allConnectors.add(connector);
//            System.out.println(connector);

            if( ignoreAnchorConnectors ){
                if( sourceNode.getType() == NodeType.Anchor || targetNode.getType() == NodeType.Anchor ) {
                    // skip adding connecting lines to UI for anchors
                    return;
                }
            }

            connectorSet.add(connector);
        };

        GmlUtil.parseGraph(graph, graphAttrsConsumer, nodeConsumer, edgeConsumer, isolatedNodeConsumer);
        
        // first lines and then boxes so boxes render on top of lines
        for(IGraphUIConnector connector : connectorSet) {
            graphUI.addConnector(connector);
        }
        for(IGraphUINode node : nodesMap.values()) {
            graphUI.addNode(node);
        }
    }
}
