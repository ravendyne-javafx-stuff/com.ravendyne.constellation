package com.ravendyne.constellation.graphui.layout.forcebased.nodes;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class LayoutGraphNodeOrigin extends LayoutGraphNode {
    IGraphUINode node;

    public LayoutGraphNodeOrigin(IGraphUINode node) {
        super(node);
        particle = new OriginParticle();
    }

    @Override
    public void setParticle(IVertex particle) {
    }

    private static class OriginParticle implements IVertex {
        Vector2d p;
        public OriginParticle(){
            this.p = Vector2d.ZERO;
        }

        @Override
        public Vector2d getPosition() {
            return p;
        }

        @Override
        public Vector2d getVelocity() {
            return null;
        }

        @Override
        public double getMass() {
            return 0;
        }

        @Override
        public Vector2d getForce() {
            return null;
        }

        @Override
        public void setForce( Vector2d add ) {
        }
        
    }

}
