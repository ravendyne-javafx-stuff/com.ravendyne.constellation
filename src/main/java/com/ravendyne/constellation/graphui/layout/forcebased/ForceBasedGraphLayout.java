package com.ravendyne.constellation.graphui.layout.forcebased;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ravendyne.constellation.geometry.BoundingBox;
import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUI;
import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.graphui.api.INodesConnectorsProvider;
import com.ravendyne.constellation.graphui.api.IGraphUI.ConnectorType;
import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;
import com.ravendyne.constellation.graphui.layout.UIUtil;
import com.ravendyne.constellation.graphui.layout.UIUtil.MutableBoundingBox;
import com.ravendyne.constellation.graphui.layout.forcebased.nodes.LayoutGraphNode;
import com.ravendyne.constellation.graphui.layout.forcebased.nodes.LayoutGraphNodeOrigin;
import com.ravendyne.constellation.simulation.force.kind.CoulombForce;
import com.ravendyne.constellation.simulation.force.kind.EnvironmentDragForce;
import com.ravendyne.constellation.simulation.force.kind.SpringForce;
import com.ravendyne.constellation.simulation.force.kind.UniformForceField;
import com.ravendyne.constellation.simulation.force.kind.UniformGravityForceField;
import com.ravendyne.constellation.simulation.particles.system.EnergyThresholdVertexSystem;

public class ForceBasedGraphLayout {

    final static double Ke = 32.0;
    final static double clamp = 0.1;

    // eta = Kd / ( 2 * sqrt( Ks ) )
    // no damping:      Kd = 0
    // under-damped:    Kd < 2 * sqrt( Ks )
    // critical:        Kd == 2 * sqrt( Ks )
    // over-damped:     Kd > 2 * sqrt( Ks )
    final static double Ks = 16.0;
    final static double Kd = 8.0; // 2.0 -> critical damping for Ks = 1.0
    final static double length = 1.0;
    final static double mass = 1.0;
    final static double initialPosition = 0.0;
    final static double springRootPosition = 1.0;

    double dragCoefficient = 0.8;

    double gravityFieldMagnitude = 0.1;

    SpringForce springForce;
    CoulombForce coulombForce;
    UniformGravityForceField gravity;
    UniformForceField likeGravity;
    EnvironmentDragForce dragForce;

    EnergyThresholdVertexSystem simulation;

    IGraphUI graphPane;
    INodesConnectorsProvider allNodesConnectors;

    Set<IGraphLayoutNode> layoutNodes;

    public ForceBasedGraphLayout(IGraphUI graphPane) {
        this.graphPane = graphPane;
        this.allNodesConnectors = graphPane.getAllNodesConnectors();
        createSimulation();
        createLayoutNodes();
    }
    
    private void createLayoutNodes() {
        layoutNodes = new HashSet<>();
        Map<IGraphUINode, IGraphLayoutNode> nodesMap = new HashMap<>();

        // create layout vertices
        allNodesConnectors.getNodes().forEach((node) -> {
            node.getPosition();

            IGraphLayoutNode layoutNode;

            Vector2d position = Vector2d.random(2.0);
            layoutNode = newNode(node);
            
            switch(node.getType()) {
            case Origin:
                position = Vector2d.ZERO;
                break;
            case Anchor:
                position = new Vector2d(node.getPosition().x, node.getPosition().y);
//                position = new Vector(0.0, 2.0);
                break;
            case Box:
                position = Vector2d.random(2.0);
                break;
            }
            layoutNode.setParticle(simulation.newParticle(position.x, position.y, 1.0));

            // nodes labeled as "anchor" will not be added to the simulation. because of that,
            // no simulation forces will affect them so their position will remain fixed.
            // they will however influence any node connected to them via a binary force, i.e. spring.
            if(node.getType() == NodeType.Box) {
                simulation.addVertex(layoutNode.getParticle());
            }
            
            nodesMap.put(node, layoutNode);
        });

        // create layout connections
        allNodesConnectors.getConnectors().forEach((connector) -> {
            IGraphUINode startNode = connector.getStartNode();
            IGraphUINode endNode = connector.getEndNode();
            IGraphLayoutNode sourceNode = nodesMap.get(startNode);
            IGraphLayoutNode targetNode = nodesMap.get(endNode);
            
            simulation.addConnection(sourceNode.getParticle(), targetNode.getParticle(), springForce);
        });

        for(IGraphLayoutNode node : nodesMap.values()) {
            layoutNodes.add(node);
        }
    }
    
    IGraphLayoutNode newNode(IGraphUINode node) {
        IGraphLayoutNode layoutNode = null;
        
        switch(node.getType()) {
        case Origin:
            layoutNode = new LayoutGraphNodeOrigin(node);
            break;
        case Anchor:
            layoutNode = new LayoutGraphNode(node);
            break;
        case Box:
            layoutNode = new LayoutGraphNode(node);
            break;
        }

        return layoutNode;
    }

    final ConnectorType connectorType = ConnectorType.Line;

    private void createSimulation() {
        simulation = new EnergyThresholdVertexSystem();

        springForce = new SpringForce(length, Ks, Kd);

        coulombForce = new CoulombForce(Ke, clamp);
        simulation.addUnboundedBinaryForce( coulombForce );

//        gravity = new UniformGravityForceField();
//        simulation.addForceField(gravity);

//        likeGravity = new UniformForceField(gravityFieldMagnitude, new Vector(0, -1));
//        simulation.addForceField(likeGravity);
        
        dragForce = new EnvironmentDragForce(dragCoefficient);
        simulation.addForceField(dragForce);
    }

    public void layoutStep(double delta) {
        simulation.update(delta);
        updateUI();
    }
    
    public boolean isLayoutDone() {
        return simulation.isEnergyBelowThreshold();
    }

    BoundingBox previousBB;
    private void updateUI() {
        MutableBoundingBox bb = new MutableBoundingBox();
        layoutNodes.forEach((node) -> {
            UIUtil.updateBoundingBox(bb, node.getParticle().getPosition());
        });
        BoundingBox boundingBox = bb.get();

        // Uncomment to keep biggest interim boundaries
//        if(previousBB == null) previousBB = boundingBox;
//        boundingBox = UIUtil.addBoundingBox( previousBB, boundingBox );
        previousBB = boundingBox;

        layoutNodes.forEach((node) -> {
            Vector2d vertexPosition = node.getParticle().getPosition();

            Vector2d newLayoutPosition = UIUtil.toScreen(vertexPosition, graphPane.getCanvasSize(), previousBB);
            
            node.getNode().setPosition(newLayoutPosition);

        });
        graphPane.getConnectors().forEach((line) -> line.updatePosition());
    }
}
