package com.ravendyne.constellation.graphui.layout.forcebased.nodes;

import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.graphui.layout.forcebased.IGraphLayoutNode;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class LayoutGraphNode implements IGraphLayoutNode {
    IGraphUINode node;
    IVertex particle;

    public LayoutGraphNode(IGraphUINode node) {
        this.node = node;
    }

    @Override
    public IGraphUINode getNode() {
        return node;
    }

    @Override
    public void setParticle(IVertex particle) {
        this.particle = particle;
    }

    @Override
    public IVertex getParticle() {
        return particle;
    }

}
