package com.ravendyne.constellation.graphui.layout.forcebased;

import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.simulation.particles.IVertex;

public interface IGraphLayoutNode {
    void setParticle(IVertex particle);
    IVertex getParticle();
    
    IGraphUINode getNode();
}
