package com.ravendyne.constellation.graphui.layout;

import com.ravendyne.constellation.geometry.BoundingBox;
import com.ravendyne.constellation.geometry.Vector2d;

public final class UIUtil {

    private static Vector2d bottomleftInitial = new Vector2d(-1,-1);
    private static Vector2d toprightInitial = new Vector2d(1,1);

    public static BoundingBox addBoundingBox(BoundingBox left, BoundingBox right) {
        double bottomleftX = left.bottomleft.x;
        double bottomleftY = left.bottomleft.y;
        double toprightX = right.topright.x;
        double toprightY = right.topright.y;

        if (right.bottomleft.x < bottomleftX) {
            bottomleftX = right.bottomleft.x;
        }
        if (right.bottomleft.y < bottomleftY) {
            bottomleftY = right.bottomleft.y;
        }
        if (left.topright.x > toprightX) {
            toprightX = left.topright.x;
        }
        if (left.topright.y > toprightY) {
            toprightY = left.topright.y;
        }

        return new BoundingBox( new Vector2d( bottomleftX, bottomleftY), new Vector2d( toprightX, toprightY) );
    }
    
    public static class MutableBoundingBox {
        public double bottomleftX;
        public double bottomleftY;
        public double toprightX;
        public double toprightY;
        
        public MutableBoundingBox() {
            bottomleftX = bottomleftInitial.x;
            bottomleftY = bottomleftInitial.y;
            toprightX = toprightInitial.x;
            toprightY = toprightInitial.y;
        }
        
        public BoundingBox get() {
            return new BoundingBox( new Vector2d( bottomleftX, bottomleftY), new Vector2d( toprightX, toprightY) );
        }
    }

    public static void updateBoundingBox(MutableBoundingBox bb, Vector2d point) {
        if (point.x < bb.bottomleftX) {
            bb.bottomleftX = point.x;
        }
        if (point.y < bb.bottomleftY) {
            bb.bottomleftY = point.y;
        }
        if (point.x > bb.toprightX) {
            bb.toprightX = point.x;
        }
        if (point.y > bb.toprightY) {
            bb.toprightY = point.y;
        }
    }

    public static Vector2d toScreen(Vector2d p, Vector2d canvasSize, BoundingBox currentBB) {
        Vector2d size = currentBB.topright.subtract(currentBB.bottomleft);

        p = p.subtract(currentBB.bottomleft);

        // scale the original position
        double xScale = canvasSize.x / size.x;
        double yScale = canvasSize.y / size.y;

        // uniform scale by both axes in a way that always keeps the whole graph in canvas bounds
        double scale = xScale < yScale ? xScale : yScale;
        xScale = yScale = scale;

        double scaledX = p.x * xScale;
        double scaledY = p.y * yScale;

        // center horizontally
        scaledX += (canvasSize.x - size.x * scale) / 2.0;

        // flip Y-axis since on-screen Y-axis is in top-down direction
        scaledY = canvasSize.y - scaledY;

        // final result
        Vector2d scaled = new Vector2d( scaledX, scaledY );

        // these four lines add a bit of padding on all sides
        double padding = 0.05; // 5%
        Vector2d offset = canvasSize.multiply( padding );
        scaled = scaled.multiply( 1.0 - padding*2.0 ); // shrinks the whole thing from top-right
        scaled = scaled.add( offset ); // translates the whole thing from bottom-left

        return scaled;
    };

    protected Vector2d fromScreen(Vector2d s, Vector2d canvasSize, BoundingBox currentBB) {
        Vector2d size = currentBB.topright.subtract(currentBB.bottomleft);
        double px = (s.x / canvasSize.x / 2.0) * size.x + currentBB.bottomleft.x;
        double py = (s.y / canvasSize.y / 2.0) * size.y + currentBB.bottomleft.y;
        return new Vector2d(px, py);
    };

}
