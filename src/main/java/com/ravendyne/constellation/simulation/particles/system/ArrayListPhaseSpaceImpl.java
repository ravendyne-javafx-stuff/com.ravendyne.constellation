package com.ravendyne.constellation.simulation.particles.system;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ravendyne.constellation.ode.solver.api.IPhaseSpace;
import com.ravendyne.constellation.ode.solver.api.IPhaseVector;
import com.ravendyne.constellation.ode.solver.api.IPhaseVectorHandle;

class ArrayListPhaseSpaceImpl implements IPhaseSpace {
    List<IPhaseVector> vectors;

    ArrayListPhaseSpaceImpl() {
        vectors = new ArrayList<>();
    }

    @Override
    public void sum( IPhaseSpace other ) {
        ArrayListPhaseSpaceImpl otherImpl = (ArrayListPhaseSpaceImpl)other;

        if(otherImpl.vectors.size() != vectors.size()) {
            throw new IllegalArgumentException( "sum() operation can be performed only on phase spaces of the same size." );
        }

        Iterator< IPhaseVector > thisIterator = vectors.iterator();
        Iterator< IPhaseVector > otherIterator = otherImpl.vectors.iterator();

        while(thisIterator.hasNext()) {
            thisIterator.next().add( otherIterator.next() );
        }
    }

    @Override
    public void scale( double delta ) {
        for(IPhaseVector element : vectors) {
            element.scale( delta );
        }
    }

    @Override
    public IPhaseVector get( IPhaseVectorHandle handle ) {
        for(IPhaseVector element: vectors) {
            if(element.getHandle() == handle) {
                return element;
            }
        }
        return null;
    }

    @Override
    public int size() {
        return vectors.size();
    }

    void add( IPhaseVector vector ) {
        vectors.add( vector );
    }

}
