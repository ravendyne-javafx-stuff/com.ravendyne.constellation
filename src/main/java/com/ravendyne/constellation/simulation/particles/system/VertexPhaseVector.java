package com.ravendyne.constellation.simulation.particles.system;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.ode.solver.api.IPhaseVector;
import com.ravendyne.constellation.ode.solver.api.IPhaseVectorHandle;

/**
 * VertexPhaseVector plays a role of {@link IPhaseVector phase vector} bounded to a specific vertex.
 * <br><br>
 * Besides phase vector data (position/velocity or velocity/acceleration), it also keeps reference to the vertex that it is bound to.
 * <br>
 */
class VertexPhaseVector implements IPhaseVector {
    // each VertexPhaseVector keeps reference to the vertex it represents
    final VectorVertex vertex;

    double x;
    double y;
    double xdot;
    double ydot;
    
    VertexPhaseVector(VectorVertex vertex) {
        this.vertex = vertex;
        this.x = vertex.getPosition().x;
        this.y = vertex.getPosition().y;
        this.xdot = vertex.getVelocity().x;
        this.ydot = vertex.getVelocity().y;
    }

    @Override
    public IPhaseVector copy() {
        // we keep vertex reference, only phase vector (x,y,xdot,ydot) is changing in VertexPhaseVector
        VertexPhaseVector copy = new VertexPhaseVector( vertex );
        copy.x = x;
        copy.y = y;
        copy.xdot = xdot;
        copy.ydot = ydot;

        return copy;
    }

    /**
     *  updates phase vector and then vertex from the vector
     * @param other
     * @return
     */
    VertexPhaseVector setFrom(VertexPhaseVector other) {
        x = other.x;
        y = other.y;
        xdot = other.xdot;
        ydot = other.ydot;

        updateVertexFromVector();

        return this;
    }

    VertexPhaseVector setVectorFrom(Vector2d u, Vector2d udot) {
        x = u.x;
        y = u.y;
        xdot = udot.x;
        ydot = udot.y;

        return this;
    }

    VertexPhaseVector updateVertexFromVector() {
        vertex.position = new Vector2d( x, y );
        vertex.velocity = new Vector2d( xdot, ydot );

        return this;
    }

    @Override
    public void add( IPhaseVector other ) {
        VertexPhaseVector otherVPV = (VertexPhaseVector)other;
        x += otherVPV.x;
        y += otherVPV.y;
        xdot += otherVPV.xdot;
        ydot += otherVPV.ydot;
    }

    @Override
    public void scale( double delta ) {
        x *= delta;
        y *= delta;
        xdot *= delta;
        ydot *= delta;
    }

    @Override
    public IPhaseVectorHandle getHandle() {
        return vertex;
    }
    
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + xdot + ", " + ydot + ")";
    }
}
