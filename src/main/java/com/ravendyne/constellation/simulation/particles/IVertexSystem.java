package com.ravendyne.constellation.simulation.particles;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IBinaryForce;
import com.ravendyne.constellation.simulation.force.IForceField;

public interface IVertexSystem {
    /**
     * Creates instance of {@link IVertex vertex} specific to this vertex system implementation.
     * <br><br>
     * Parameters are initial values and may change over time. The only identifying value for specific vertex
     * created by this method is the object reference of the instance.
     * <br>
     * Created vertex will not be automatically added to the system itself, that has to be explicitly done
     * by calling {@link #addVertex(IVertex)} method. This enables creation of vertices that affect geometry
     * and behavior of the system but are not affected by the simulation itself. Examples are environment anchor points,
     * wall points, sources of force fields etc.
     * 
     * @param position
     * @param velocity
     * @param mass
     * @return
     */
    IVertex newVertex(Vector2d position, Vector2d velocity, double mass);

    /**
     * Adds a vertex to this vertex system.
     * <br><br>
     * Adding a vertex that is not created by this system results in undefined behavior.
     * 
     * @param vertex
     */
    void addVertex(IVertex vertex);
    
    /**
     * Connects two vertices by a {@link IBinaryForce binary force}. The force is called "bounded"
     * since it only affects these two vertices. Cost of calculating these forces is proportional
     * to O(Nedge), where {@code Nedge} is number of connections between vertices made by calls to
     * {@link #addBoundedBinaryForce(IVertex, IVertex, IBinaryForce)}.
     * <br><br>
     * Most common example of using bounded binary force is connecting vertices with a spring,
     * where spring is simulated by the {@code force} parameter.
     * <br>
     * This method may not add the vertices to the system, depending on implementation.
     * <br>
     * The force will be added to the system as an instance that connects only these two vertices.
     * <br>
     * Connecting vertices not created by this system results in undefined behavior.
     * 
     * @param vertexA
     * @param vertexB
     * @param force
     */
    void addBoundedBinaryForce(IVertex vertexA, IVertex vertexB, IBinaryForce force);

    /**
     * Adds a {@link IBinaryForce binary force} to the system. The force is called "unbounded" because
     * it may affect any pair of vertices in the system. Cost of calculating these forces is proportional
     * to O(Np), where Np is number of vertices in the system that are affected by this force.
     * <br><br>
     * Most common example of using unbounded binary force is gravity in celestial mechanic simulations or
     * Coulomb force in electro-dynamic simulations.
     * 
     * @param force
     */
    void addUnboundedBinaryForce(IBinaryForce force);

    /**
     * Adds a {@link IForceField force field} type of force to the system.
     * <br><br>
     * Added this way, the force will affect all the vertices in the system.
     * @param force
     */
    void addForceField(IForceField force);

    /**
     * Gets the time elapsed since the system started running simulation.
     * 
     * @return
     */
    double getTime();
}
