package com.ravendyne.constellation.simulation.particles.system;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.DoubleAdder;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.ode.solver.api.ISolver;
import com.ravendyne.constellation.ode.solver.impl.MidpointSolver;
import com.ravendyne.constellation.simulation.force.IBinaryForce;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;
import com.ravendyne.constellation.simulation.particles.IVertexSystemPhaseSpaceProvider;

public class EnergyThresholdVertexSystem {
    final static int MAXITERATIONS = 500;

    private double systemKineticEnergy;
    private double systemEnergyTreshold;

    IVertexSystemPhaseSpaceProvider system;
    ISolver solver;

    // keeping these so we can calculate total kinetic energy
    Set<IVertex> vertices;

    private double energyThresholdScale;

    public EnergyThresholdVertexSystem() {
        
        systemKineticEnergy = 0.0;
        systemEnergyTreshold = 0.0;
        energyThresholdScale = 200.0;

        system = new ArrayListVertexSystemImpl();

        solver = new MidpointSolver();
        
        vertices = new HashSet<>();
    }
    
    public IVertex newParticle( double x, double y, double vx, double vy, double mass) {
        return system.newVertex( new Vector2d( x, y ), new Vector2d(vx, vy), mass );
    }

    public IVertex newParticle( double x, double y, double mass) {
        return system.newVertex( new Vector2d( x, y ), Vector2d.ZERO, mass );
    }
    
    public void addForceField(IForceField forceField) {
        system.addForceField( forceField );
    }
    public void addUnboundedBinaryForce( IBinaryForce force ) {
        system.addUnboundedBinaryForce( force );
    }

    public void addVertex( IVertex boxParticle ) {
        Objects.requireNonNull(boxParticle);

        system.addVertex( boxParticle );
        vertices.add( boxParticle );
    }

    public void addConnection( IVertex startParticle, IVertex endParticle, IBinaryForce force ) {
        Objects.requireNonNull(startParticle);
        Objects.requireNonNull(endParticle);

        system.addBoundedBinaryForce( startParticle, endParticle, force );
    }

    public boolean isEnergyBelowThreshold() {
        return systemKineticEnergy < systemEnergyTreshold;
    }
    
    public void update(double delta) {
        solver.applyStep( system, delta );
        calculateTotalEnergy();
    }

    public void setEnergyThresholdScale(double value) {
        energyThresholdScale = value;
    }

    private void calculateTotalEnergy() {
        DoubleAdder totalKineticEnergy = new DoubleAdder();

        vertices.forEach( (vertex) -> {
            double speed = vertex.getVelocity().magnitude();

            // we don't strictly need kinetic energy, we just need some sort of a measure of
            // overall rate of movement in the system, so we use E = v^2 instead of E = m * v^2
            totalKineticEnergy.add( vertex.getMass() * speed * speed / 2.0 );
        });
        
        systemKineticEnergy = totalKineticEnergy.doubleValue();
        
        // initially, systemEnergyTreshold == 0.0 and we want to set it to
        // some sensible value that will enable us to detect that movement of
        // vertices is winding down.
        if( systemEnergyTreshold == 0.0 ) {
            // what we assign here depends very much on what kind of
            // system has been set up: what forces are added to the system
            // and how those forces are configured.
            systemEnergyTreshold = systemKineticEnergy / energyThresholdScale;
        }
    }

}
