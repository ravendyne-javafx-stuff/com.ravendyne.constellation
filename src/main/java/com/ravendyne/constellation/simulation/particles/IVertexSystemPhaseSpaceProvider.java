package com.ravendyne.constellation.simulation.particles;

import com.ravendyne.constellation.ode.solver.api.IPhaseSpaceProvider;
import com.ravendyne.constellation.ode.solver.api.ISolver;

/**
 * {@link IVertexSystem Vertex system} that is also a {@link IPhaseSpaceProvider phase space provider}.
 * Useful when {@link ISolver ODE solver} is used for calculations.
 *
 */
public interface IVertexSystemPhaseSpaceProvider extends IVertexSystem, IPhaseSpaceProvider{

}
