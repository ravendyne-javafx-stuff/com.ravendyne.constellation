package com.ravendyne.constellation.simulation.particles.system;

import com.ravendyne.constellation.simulation.force.IBinaryForce;

/**
 * VertexEdge represents connection between two vertices made via connecting {@link IBinaryForce force}.
 * <br><br>
 * Example of binding force may be spring force.
 *
 */
public class VertexEdge {
    VertexPhaseVector origin;
    VertexPhaseVector target;
    IBinaryForce force;
    
    VertexEdge(VectorVertex origin, VectorVertex target, IBinaryForce force) {
        this.origin = new VertexPhaseVector(origin);
        this.target = new VertexPhaseVector(target);
        this.force = force;
    }
    
    void applyForce( double time ) {
        force.apply( origin.vertex, target.vertex, time);
    }
    
    public VertexPhaseVector getOrigin() {
        return origin;
    }

    public VertexPhaseVector getDestination() {
        return target;
    }

}
