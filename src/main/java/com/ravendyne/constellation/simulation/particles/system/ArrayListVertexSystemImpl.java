package com.ravendyne.constellation.simulation.particles.system;

import java.util.ArrayList;
import java.util.List;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.ode.solver.api.IPhaseSpace;
import com.ravendyne.constellation.simulation.force.IBinaryForce;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;
import com.ravendyne.constellation.simulation.particles.IVertexSystemPhaseSpaceProvider;

public class ArrayListVertexSystemImpl implements IVertexSystemPhaseSpaceProvider {

    // element -> [ x, xdot ]
    List<VertexPhaseVector> vertices;
    List<VertexEdge> edges;
    
    List<IForceField> forceFields;
    List<IBinaryForce> unboundedBinaryForces;

    double time;
    
    public ArrayListVertexSystemImpl() {
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
        time = 0;

        forceFields = new ArrayList<>();
        unboundedBinaryForces = new ArrayList<>();
    }

    @Override
    public void addForceField(IForceField force) {
        forceFields.add(force);
    }
    
    @Override
    public void addVertex(IVertex vertex) {
        vertices.add( new VertexPhaseVector( (VectorVertex)vertex ) );
    }

    /**
     * {@inheritDoc}
     * <br><br>
     * This implementation adds the parameter vertices to the system.
     */
    @Override
    public void addBoundedBinaryForce(IVertex vertexA, IVertex vertexB, IBinaryForce force) {
        VertexEdge edge = new VertexEdge( (VectorVertex)vertexA, (VectorVertex)vertexB, force );
        edges.add(edge);
    }

    @Override
    public void advanceTime( double delta ) {
        time += delta;
    }

    @Override
    public double getTime() {
        return time;
    }

    @Override
    public IPhaseSpace getPhaseSpace() {
        // FIXME possible to avoid copying??
        // or make it less expensive??
        // we don't want outsiders to change the phase space
        // by applying sum(), scale() or any other
        ArrayListPhaseSpaceImpl space = new ArrayListPhaseSpaceImpl();

        vertices.forEach( (se) -> {
            space.add( se.copy() );
        });

        return space;
    }

    @Override
    public IPhaseSpace getDerivative() {

        ArrayListPhaseSpaceImpl space = new ArrayListPhaseSpaceImpl();
        double time = getTime();

        // clear force accumulators
        vertices.forEach( (vpv) -> {
            vpv.vertex.force = Vector2d.ZERO;
        });

        // apply force fields
        for(IForceField force: forceFields) {
            vertices.forEach( (vpv) -> {
                    force.apply(vpv.vertex, time);
            });
        }

        // apply bounded forces
        edges.forEach( (ve) -> {
            ve.applyForce(time);
        });

        // apply un-bounded binary forces
        for(IBinaryForce force: unboundedBinaryForces) {
            vertices.forEach( (vpv1) -> {
                vertices.forEach( (vpv2) -> {
                        force.apply(vpv1.vertex, vpv2.vertex, time);
                });
            });
        }

        // calculate & return derivatives as phase vectors (implemented by ParticeVertex class)
        vertices.forEach( (vpv) -> {
            VertexPhaseVector me = ( VertexPhaseVector ) vpv.copy();
            // ParticleVertex.copy() makes sure that me.particle and se.particle are one and the same
            VectorVertex vertex = me.vertex;

            // return phase vector [ v, f/m ]
            me.setVectorFrom( vertex.velocity, vertex.force.divide( vertex.mass ) );

            space.add( me );
        });

        return space;
    }

    @Override
    public void setPhaseSpace( IPhaseSpace space ) {

        vertices.forEach( (vpv) -> {
            VertexPhaseVector otherVpv = (VertexPhaseVector)space.get( vpv.vertex );
            vpv.setFrom( otherVpv );
        });

    }

    @Override
    public IVertex newVertex( Vector2d position, Vector2d velocity, double mass ) {
        return new VectorVertex(position, velocity, mass);
    }

    @Override
    public void addUnboundedBinaryForce( IBinaryForce force ) {
        unboundedBinaryForces.add( force );
    }

    public void printPositionSpeedCSV( double Yt, int timestamp ) {

        System.out.print( timestamp + "," );
        vertices.forEach( (pv) -> {
            IVertex vertex = (IVertex)pv.getHandle();
            System.out.print( vertex.getPosition().x + "," + vertex.getPosition().y + "," + vertex.getVelocity().x + "," + vertex.getVelocity().y + "," + Yt + "," + ( Yt - vertex.getPosition().y ) );
            System.out.println();
        });

    }

}
