package com.ravendyne.constellation.simulation.particles;

import com.ravendyne.constellation.geometry.Vector2d;

public interface IVertex {

    Vector2d getPosition();
    Vector2d getVelocity();

    double getMass();

    Vector2d getForce();
    void setForce( Vector2d add );

}
