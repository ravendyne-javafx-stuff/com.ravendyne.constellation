package com.ravendyne.constellation.simulation.particles.system;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.ode.solver.api.IPhaseVectorHandle;
import com.ravendyne.constellation.simulation.particles.IVertex;

/**
 * {@link IVertex} implementation based on {@link Vector2d}.
 *
 */
class VectorVertex implements IVertex, IPhaseVectorHandle {
    
    // Position in phase space
    Vector2d position;
    Vector2d velocity;
    
    // force accumulator
    Vector2d force;
    
    // mass, eh?
    double mass;

    public VectorVertex(Vector2d position, Vector2d velocity, double mass) {
        force = Vector2d.ZERO;
        
        this.position = position;
        this.velocity = velocity;
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "p: " + position + ", v: " + velocity;
    }
    
    public VectorVertex copy() {
        VectorVertex copy = new VectorVertex( position, velocity, mass );
        copy.force = force;
        return copy;
    }

    @Override
    public Vector2d getPosition() {
        return position;
    }

    @Override
    public Vector2d getVelocity() {
        return velocity;
    }

    @Override
    public double getMass() {
        return mass;
    }

    @Override
    public Vector2d getForce() {
        return force;
    }

    @Override
    public void setForce(Vector2d force) {
        this.force = force;
    }
}
