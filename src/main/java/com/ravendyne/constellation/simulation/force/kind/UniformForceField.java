package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class UniformForceField implements IForceField {

    private final double magnitude;
    private final Vector2d direction;

    public UniformForceField(double magnitude, Vector2d direction) {
        this.magnitude = magnitude;
        this.direction = direction.normalise();
    }

    @Override
    public void apply(IVertex particle, double time) {
        particle.setForce( particle.getForce().add(influenceOn(particle, time)) );
    }

    @Override
    public Vector2d influenceOn(IVertex particle, double time) {
        return direction.multiply( particle.getMass() * magnitude );
    }

}
