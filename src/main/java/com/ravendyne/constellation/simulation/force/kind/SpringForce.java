package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IBinaryForce;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class SpringForce implements IBinaryForce {
    
    private double length;
    private double ks;
    private double kd;

    public SpringForce(double length, double ks, double kd) {
        this.length = length;
        this.ks = ks;
        this.kd = kd;
    }

    @Override
    public void apply( IVertex particleA, IVertex particleB, double time ) {
        Vector2d distanceVector = particleB.getPosition().subtract(particleA.getPosition());

        final double distance = distanceVector.magnitude();
        final Vector2d distanceUnitVector = distanceVector.normalise();

        // dumping influence
        double dumpingMagnitude = 0;
        if(kd != 0) {
            Vector2d deltaPosition = particleB.getVelocity().subtract(particleA.getVelocity());
            double deltaVelocity = deltaPosition.dot(distanceUnitVector);
            dumpingMagnitude = - deltaVelocity * kd;
        }

        // elastic influence
        double magnitude = - ks * (distance - length);

        Vector2d springForce = distanceUnitVector.multiply(magnitude + dumpingMagnitude);
        // The force is calculated as a force on particleB...
        particleB.setForce( particleB.getForce().add(springForce) );
        // ...so we set opposite force on particleA.
        particleA.setForce( particleA.getForce().add(springForce.reverse()) );
    }

    @Override
    public String toString() {
        return "L = " + length + ", Ks = " + ks + ", Kd = " + kd;
    }
}
