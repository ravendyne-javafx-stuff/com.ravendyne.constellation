package com.ravendyne.constellation.simulation.force;

import com.ravendyne.constellation.simulation.particles.IVertex;

/**
 * Forces that exist between two vertices.
 * <br><br>
 * Depending on implementation, forces on both vertices may or may not obey Newton's Second Law
 * (i.e. the force on {@code vertexA} can, but doesn't have to, be equal but opposite to the force on {@code vertexB}).
 * Also, it may or may not be significant which of the two vertices is passed in
 * as first and which as the second parameter.
 * <br>
 * {@code time} parameter may or may not be significant, depending on the implementation of a concrete binary
 * force.
 * <br>
 * Most common example of binary force is a spring force between two vertices. In that case, in general, {@code time}
 * parameter is not significant and can be ignored (i.e. passed 0.0 value). Another example of binary force would
 * be gravity in celestial mechanics simulation.
 *
 */
public interface IBinaryForce {
    void apply(IVertex vertexA, IVertex vertexB, double time);
}
