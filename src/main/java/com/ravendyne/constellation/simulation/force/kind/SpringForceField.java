package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class SpringForceField implements IForceField {
    
    private IVertex root;
    private double length;
    private double ks;
    private double kd;

    public SpringForceField(IVertex root, double length, double ks, double kd) {
        this.root = root;
        this.length = length;
        this.ks = ks;
        this.kd = kd;
    }

    @Override
    public void apply(IVertex particle, double time) {
        Vector2d springForce = influenceOn(particle, time);
        particle.setForce( particle.getForce().add(springForce) );
        root.getForce().add(springForce.reverse());
    }

    @Override
    public Vector2d influenceOn(IVertex particle, double time) {
        Vector2d distanceVector = particle.getPosition().subtract(root.getPosition());
        final double distance = distanceVector.magnitude();
        final Vector2d distanceUnitVector = distanceVector.normalise();

        // dumping influence
        double dumpingMagnitude = 0;
        if(kd != 0) {
            Vector2d deltaPosition = particle.getVelocity().subtract(root.getVelocity());
            double deltaVelocity = deltaPosition.dot(distanceUnitVector);
            dumpingMagnitude = - deltaVelocity * kd;
        }

        // elastic influence
        double magnitude = - ks * (distance - length);

        return distanceUnitVector.multiply(magnitude + dumpingMagnitude);
    }

}
