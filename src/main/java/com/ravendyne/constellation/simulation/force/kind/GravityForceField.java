package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class GravityForceField implements IForceField {
    
    private IVertex source;
    private double G;

    public GravityForceField(double gravityConstant, IVertex gravitySource) {
        this.source = gravitySource;
        this.G = - Math.abs(gravityConstant);
    }

    @Override
    public void apply(IVertex particle, double time) {
        particle.setForce( particle.getForce().add(influenceOn(particle, time)) );
    }

    @Override
    public Vector2d influenceOn(IVertex particle, double time) {
        Vector2d distanceVector = particle.getPosition().subtract(source.getPosition());
        double distance = distanceVector.magnitude();

        Vector2d gForce = distanceVector.normalise().multiply( source.getMass() * particle.getMass() * G / distance / distance );

        return gForce;
    }

}
