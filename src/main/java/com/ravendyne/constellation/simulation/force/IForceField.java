package com.ravendyne.constellation.simulation.force;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.particles.IVertex;

/**
 * Describes a force field. Force in a field depends, in general, on the position where it is observed,
 * and is therefore applied on one vertex at a time.
 * <br>
 * Depending on implementation, {@code time} parameter may or may not be significant. For example, gravitational
 * force field is, in general, independent of time and that parameter may be ignored by implementation.
 * Other example may be a wind force where implementation will take into account some form of dependency
 * on current time.
 *
 */
public interface IForceField {
    void apply(IVertex vertex, double time);
    Vector2d influenceOn(IVertex vertex, double time);
}
