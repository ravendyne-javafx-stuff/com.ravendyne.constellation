package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class UniformGravityForceField implements IForceField {

    UniformForceField uniformGravity;

    public UniformGravityForceField() {
        uniformGravity = new UniformForceField(9.81, new Vector2d(0, -1));
    }

    @Override
    public void apply(IVertex vertex, double time) {
        uniformGravity.apply(vertex, time);
    }

    @Override
    public Vector2d influenceOn(IVertex vertex, double time) {
        return uniformGravity.influenceOn(vertex, time);
    }

}
