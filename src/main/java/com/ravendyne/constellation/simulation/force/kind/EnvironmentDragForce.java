package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IForceField;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class EnvironmentDragForce implements IForceField {
    
    private double Cd;
    
    public EnvironmentDragForce(double dragCoefficient) {
        this.Cd = Math.abs(dragCoefficient);
    }

    @Override
    public void apply(IVertex vertex, double time) {
        vertex.setForce(vertex.getForce().add(influenceOn(vertex, time)));
    }

    @Override
    public Vector2d influenceOn(IVertex vertex, double time) {
        Vector2d velocityVector = vertex.getVelocity();
        Vector2d velocityUnitVector = vertex.getVelocity().normalise();
        double velocity = velocityVector.magnitude();
        
        Vector2d dragForce = velocityUnitVector.multiply( - Cd * velocity * velocity);
        
        return dragForce;
    }

}
