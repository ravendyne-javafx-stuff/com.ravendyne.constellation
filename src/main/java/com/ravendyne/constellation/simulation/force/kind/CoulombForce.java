package com.ravendyne.constellation.simulation.force.kind;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.simulation.force.IBinaryForce;
import com.ravendyne.constellation.simulation.particles.IVertex;

public class CoulombForce implements IBinaryForce {
    
    private double ke;
    private double clamp;

    public CoulombForce(double ke, double clamp) {
        this.ke = ke;
        this.clamp = clamp;
    }

    @Override
    public void apply( IVertex particleA, IVertex particleB, double time ) {
        Vector2d distanceVector = particleB.getPosition().subtract(particleA.getPosition());

        double distance = distanceVector.magnitude();
        Vector2d distanceUnitVector = distanceVector.normalise();

        // avoid extreme forces on super-small distances
        distance += clamp;
        // coulomb influence
        double magnitude = ke / (distance * distance);

        // force is in the direction of distance unit vector, from A to B, ergo repulsing force
        Vector2d repulsingForce = distanceUnitVector.multiply(magnitude);
        // The force is calculated as a force on particleB...
        particleB.setForce( particleB.getForce().add(repulsingForce) );
        // ...so we set opposite force on particleA.
        particleA.setForce( particleA.getForce().add(repulsingForce.reverse()) );
    }

}
