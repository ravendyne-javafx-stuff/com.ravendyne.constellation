package com.ravendyne.constellation.events.simple;

import java.util.ArrayList;
import java.util.List;

public class SimpleEventType<T extends SimpleEvent> {
    private List<IEventHandler<T>> registrar;

    public void addListener( IEventHandler<T> listener ) {
        if(registrar == null) {
            registrar = new ArrayList<>();
        }
        
        registrar.add( listener );
    }

    public void removeListener( IEventHandler<T> listener ) {
        if( registrar == null ) {
            return;
        }
        
        registrar.remove( listener );
    }

    public void dispatch( T event ) {
        if( registrar == null ) {
            return;
        }
        
        if( registrar.isEmpty() ) {
            return;
        }
        
        for(IEventHandler< T > listener : registrar) {
            listener.handle( event );
        }
    }
}
