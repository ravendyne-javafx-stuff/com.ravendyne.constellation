/**
 * Implementation of a simple event handler system. This is meant to be used on JavaFX application thread so the implementation
 * is <b>not</b> thread safe.
 * <p>
 * The intent behind {@link com.ravendyne.constellation.events.simple.SimpleEvent SimpleEvent} and {@link com.ravendyne.constellation.events.simple.SimpleEventType SimpleEventType} is
 * to avoid long chains of method calls in order to pass a value or to trigger processing on another object. Examples are (highly application specific):
 * <ul>
 * <li>setting currently active drawing (many places may need to know when active drawing changes)</li>
 * <li>switching visibility of a toolbox by clicking on a button somewhere in the UI</li>
 * <li>transferring information to the correct Pane about currently selected component type to be created</li>
 * <li>etc.</li>
 * </ul>
 * </p>
 * <p>
 * Long chains of method calls have <b>very</b> high affinity to bug generation when adding/removing classes
 * or shuffling (a.k.a. refactoring) things around etc. To avoid this, we create {@link com.ravendyne.constellation.events.simple.SimpleEventType SimpleEventType} for
 * the event we need and then register listener on it at any place that will consume the event. Event is triggered by calling {@code dipatch()} on the specific type
 * instance, which simply immediately calls all registered listeners.
 * </p>
 * <p>
 * Events are classes that extend {@link com.ravendyne.constellation.events.simple.SimpleEvent SimpleEvent}. If you need to pass some data from event
 * dispatcher to event consumers, simply add whatever you need to the event class. Each event class contains public static instances of {@link com.ravendyne.constellation.events.simple.SimpleEventType SimpleEventType}s
 * that may be listened for and dispatched.
 * </p>
 * <p>
 * One side-effect of the way this simple event system is implemented is that you can easily find all places in the code that produce or consume a specific event. Since an event is represented by a
 * static final instance of {@link com.ravendyne.constellation.events.simple.SimpleEventType SimpleEventType} class contained in specific subclass of {@link com.ravendyne.constellation.events.simple.SimpleEvent SimpleEvent} for that event,
 * you can simply use your IDE to select that instance and then call whatever function the IDE has to show all the places in the code where that instance has been used.
 * <br>
 * For example, in Eclipse IDE, you would right-click on the event type instance and choose "Open Call Hierarchy" from the popup menu. If you are using IntelliJ IDEA, you would select "Find Usages" from the popup menu.
 * Since, in order to register listener or dispatch an event, you'd have to call {@code addListener()} or {@code dispatch()} method on the instance of that event, the call hierarchy (usage list) will give you all the places
 * in your code where you registered for/dispatched the event.
 * </p>
 * <p>
 * For example see FXBlueprints {@link com.ravendyne.fxblueprints.events.ComponentSelectionEvent ComponentSelectionEvent} class.
 * </p>
 * 
 */
package com.ravendyne.constellation.events.simple;
