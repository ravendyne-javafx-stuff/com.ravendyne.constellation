package com.ravendyne.constellation.events.simple;

public interface IEventHandler<T extends SimpleEvent> {
    void handle(T e);
}
