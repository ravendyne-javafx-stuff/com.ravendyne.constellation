package com.ravendyne.constellation.graph.generic.gml;

public interface GmlValueBuilder extends GmlValue {
    
    void setNumber(Number value);

    void setString(String value);

    void setMapItem(String key, GmlValue value);

    void addListItem(GmlValue value);

    void addAllListItems(GmlValue value);

}
