package com.ravendyne.constellation.graph.generic;

public interface IGraphEdge<N extends IGraphNode> {
    N getOrigin();
    N getDestination();
    boolean isDirected();
}
