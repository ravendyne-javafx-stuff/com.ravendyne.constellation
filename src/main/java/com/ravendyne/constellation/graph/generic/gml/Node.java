package com.ravendyne.constellation.graph.generic.gml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class Node implements GmlValue {

    @Override
    public boolean isNumber() {
        return false;
    }

    @Override
    public boolean isString() {
        return false;
    }

    @Override
    public boolean isMap() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public Number getNumber() {
        return null;
    }

    @Override
    public String getString() {
        return null;
    }

    @Override
    public Map< String, GmlValue > getMap() {
        return null;
    }

    @Override
    public List<GmlValue> getList() {
        return null;
    }

    static final class Key {
        public String name;
        public Key(String name) {
            this.name = name;
        }
        @Override
        public String toString() {
            return name;
        }
    }
    
    static final class KeyValue {
        public Key key;
        public GmlValue value;
        public KeyValue(Key key, GmlValue value) {
            this.key = key;
            this.value = value;
        }
        @Override
        public String toString() {
            return key + "->" + value;
        }
    }

    static final class Numeric extends Node {
        Number value;
        
        public Numeric(Object value) {
            if(value instanceof Integer || value instanceof Double) {
                this.value = (Number)value;
            } else if(value instanceof String) {
                this.value = Double.valueOf((String)value);
            }
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public boolean isNumber() {
            return true;
        }

        @Override
        public Number getNumber() {
            return value;
        }
        
    }
    
    static final class Text extends Node {
        String value;
        
        public Text(Object value) {
            this.value = String.valueOf(value);
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public boolean isString() {
            return true;
        }

        @Override
        public String getString() {
            return value;
        }
 
    }

    static final class ValueList extends Node {
        List<GmlValue> list;
        
        public ValueList() {
            list = new ArrayList<>();
        }
        
        public void add(GmlValue value) {
            list.add(value);
        }

        @Override
        public String toString() {
            return list.toString();
        }

        @Override
        public boolean isList() {
            return true;
        }

        @Override
        public List<GmlValue> getList() {
            return list;
        }
 
    }
    
    static final class KeyValueList extends Node {
        Map<String, GmlValue> map;

        public KeyValueList() {
            map = new HashMap<>();
        }
        
        public void add(KeyValue pair) {
            map.put( pair.key.name, pair.value );
        }

        @Override
        public String toString() {
            return map.toString();
        }

        @Override
        public boolean isMap() {
            return true;
        }

        @Override
        public Map< String, GmlValue > getMap() {
            return map;
        }
        
    }
}
