package com.ravendyne.constellation.graph.generic;

import java.util.ArrayList;
import java.util.List;

final class Incidence<N extends IGraphNode, E extends IGraphEdge<N>> {
    private final List<E> in;
    private final List<E> out;
    private final List<E> undirected;
    private final List<E> all;
    
    Incidence() {
        in = new ArrayList<>();
        out = new ArrayList<>();
        undirected = new ArrayList<>();
        all = new ArrayList<>();
    }

    void in(E edge) {
        in.add( edge );
        all.add( edge );
    }

    List<E> in() {
        return in;
    }

    void out(E edge) {
        out.add( edge );
        all.add( edge );
    }

    List<E> out() {
        return out;
    }

    List<E> undirected() {
        return undirected;
    }

    void undirected(E edge) {
        undirected.add( edge );
        all.add( edge );
    }
    
    List<E> all() {
        return all;
    }

    void remove(E edge) {
        in.remove( edge );
        out.remove( edge );
        undirected.remove( edge );
        all.remove( edge );
    }
}
