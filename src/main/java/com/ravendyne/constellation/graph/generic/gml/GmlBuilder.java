package com.ravendyne.constellation.graph.generic.gml;

import java.io.PrintStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GmlBuilder {
    public static final String GRAPH_KEY = "graph";

    public static String indent = "  ";
    
    static class ValueBuilderBase implements GmlValueBuilder {

        @Override
        public boolean isNumber() {
            return false;
        }

        @Override
        public boolean isString() {
            return false;
        }

        @Override
        public boolean isMap() {
            return false;
        }

        @Override
        public boolean isList() {
            return false;
        }

        @Override
        public Number getNumber() {
            return null;
        }

        @Override
        public String getString() {
            return null;
        }

        @Override
        public Map<String, GmlValue> getMap() {
            return null;
        }

        @Override
        public List<GmlValue> getList() {
            return null;
        }

        @Override
        public void setNumber(Number value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setString(String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setMapItem(String key, GmlValue value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addListItem(GmlValue value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addAllListItems(GmlValue value) {
            throw new UnsupportedOperationException();
        }
        
    }

    static class NumberValue extends ValueBuilderBase {
        private Number value;
        
        public NumberValue() {
            value = null;
        }

        @Override
        public boolean isNumber() {
            return true;
        }

        @Override
        public Number getNumber() {
            return value;
        }

        @Override
        public void setNumber(Number value) {
            this.value = value;
        }
    }

    static class StringValue extends ValueBuilderBase {
        private String value;
        
        public StringValue() {
            value = null;
        }

        @Override
        public boolean isString() {
            return true;
        }

        @Override
        public String getString() {
            return value;
        }

        @Override
        public void setString(String value) {
            this.value = value;
        }
    }

    static class MapValue extends ValueBuilderBase {
        private Map<String, GmlValue> value;
        
        public MapValue() {
            value = new HashMap<>();
        }

        @Override
        public boolean isMap() {
            return true;
        }

        @Override
        public Map<String, GmlValue> getMap() {
            return value;
        }

        @Override
        public void setMapItem(String key, GmlValue value) {
            this.value.put(key, value);
        }
    }

    static class ListValue extends ValueBuilderBase {
        private List<GmlValue> value;
        
        public ListValue() {
            value = new ArrayList<>();
        }

        @Override
        public boolean isList() {
            return true;
        }

        @Override
        public List<GmlValue> getList() {
            return value;
        }

        @Override
        public void addListItem(GmlValue value) {
            this.value.add(value);
        }

        @Override
        public void addAllListItems(GmlValue value) {
            if(! value.isList()) {
                throw new IllegalArgumentException("Can only add GmlValue that is a list");
            }
            
            this.value.addAll(value.getList());
        }
    }

    public static GmlValueBuilder newNumberValue() {
        return new NumberValue();
    }

    public static GmlValueBuilder newNumberValue(Number value) {
        final NumberValue numberValue = new NumberValue();
        numberValue.setNumber(value);
        return numberValue;
    }

    public static GmlValueBuilder newStringValue() {
        return new StringValue();
    }

    public static GmlValueBuilder newStringValue(String value) {
        final StringValue stringValue = new StringValue();
        stringValue.setString(value);
        return stringValue;
    }

    public static GmlValueBuilder newMapValue() {
        return new MapValue();
    }

    public static GmlValueBuilder newListValue() {
        return new ListValue();
    }

    public static void save(GmlValue graphMap, PrintStream out) {
        saveMap(GRAPH_KEY, graphMap.getMap(), out, 0);
    }

    public static void save(String rootKey, GmlValue graphMap, PrintStream out) {
        if(! graphMap.isMap()) {
            throw new InvalidParameterException("Can only save as a root GmlValue which is a map.");
        }

        saveMap(rootKey, graphMap.getMap(), out, 0);
    }

    public static String indent(int level) {
        return String.join("", Collections.nCopies(level, indent));
    }
    
    private static void saveNumber(String key, Number value, PrintStream out, int indentLevel) {
        out.print(indent(indentLevel));
        out.println(key + " " + value);
    }
    
    private static void saveString(String key, String value, PrintStream out, int indentLevel) {
        out.print(indent(indentLevel));
        out.print(key + " ");
        out.println("\"" + value.replace('"', '\'') + "\"");
    }

    private static void saveMap(String mapKey, Map<String, GmlValue> map, PrintStream out, int indentLevel) {
        String indent = indent(indentLevel);

        out.print(indent);
        out.println(mapKey + " [");

        for(String key : map.keySet()) {
            GmlValue item = map.get(key);

            if(item.isNumber()) {
                saveNumber(key, item.getNumber(), out, indentLevel + 1);
            } else if(item.isString()) {
                saveString(key, item.getString(), out, indentLevel + 1);
            } else if(item.isMap()) {
                saveMap(key, item.getMap(), out, indentLevel + 1);
            } else if(item.isList()) {
                saveList(key, item.getList(), out, indentLevel + 1);
            }
        }
        
        out.print(indent);
        out.println("]");
        
    }
    
    private static void saveList(String key, List<GmlValue> list, PrintStream out, int indentLevel) {
        for(GmlValue item : list) {
            if(item.isMap()) {
                Map<String, GmlValue> map = item.getMap();
                saveMap(key, map, out, indentLevel);
            }
        }
    }

}
