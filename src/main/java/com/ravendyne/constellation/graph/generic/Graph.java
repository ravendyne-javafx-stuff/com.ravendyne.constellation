package com.ravendyne.constellation.graph.generic;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class Graph<N extends IGraphNode, E extends IGraphEdge<N>> {
    private Set<N> nodes;
    private Set<E> edges;
    private Map<N, Incidence<N, E>> adjacency;

    private List<IGraphListener> onGraphChangedListeners;

    public Graph() {
        nodes = new HashSet<>();
        edges = new HashSet<>();
        adjacency = new HashMap<>();

        onGraphChangedListeners = new ArrayList<>();
    }
    
    /**
     * Adds the {@code node} instance to this graph.
     * 
     * @param node to add to the graph.
     * @return the added node.
     */
    public N addNode(N node) {
        if(nodes.contains( node )) {
            return node;
        }

        insertNode( node );

        onGraphChanged();
        return node;
    };

    /**
     * Adds the {@code edge} instance to this graph's edge collection.
     * <br><br>
     * If nodes that the edge connects aren't already in this graph, they will be added to it.
     * 
     * @param edge to add to the graph.
     * @return the added edge.
     */
    public E addEdge(E edge) {
        if(edges.contains( edge )) {
            return edge;
        }

        insertNode( edge.getOrigin() );
        insertNode( edge.getDestination() );
        insertEdge( edge );

        onGraphChanged();
        return edge;
    };

    /**
     * Removes the node and its incident edges from the graph.
     * 
     * @param node
     */
    public void removeNode(N node) {
        if(!nodes.contains( node )) {
            return;
        }

        deleteNode( node );

        onGraphChanged();
    };

    /**
     * Removes all incident edges of this node but leaves node in the graph.
     * <br>
     * This operation will make the node isolated node.
     * 
     * @param node
     */
    public void detachNode(N node) {
        if(!nodes.contains( node )) {
            return;
        }
        
        deleteEdgesForNode( node );

        onGraphChanged();
    };

    /**
     * Removes the edge from the graph.
     * 
     * @param edge
     */
    public void removeEdge(E edge) {
        if(!edges.contains( edge )) {
            return;
        }
        
        deleteEdge( edge );

        onGraphChanged();
    };

    /**
     * Iterates over graph edges and calls {@code callback} passing it current edge as parameter.
     * 
     * @param callback
     */
    public void forEachEdge(Consumer<E> callback) {
        edges.forEach( callback );
    }

    /**
     * Iterates over graph nodes and calls {@code callback} passing it current node as parameter.
     * 
     * @param callback
     */
    public void forEachNode(Consumer<N> callback) {
        nodes.forEach( callback );
    }

    /**
     * Adds a listener for graph change events: node insertion/removal, edge insertion/removal.
     * 
     * @param listener
     */
    public void addGraphListener(IGraphListener listener) {
        onGraphChangedListeners.add(listener);
    };

    /**
     * 
     * @param node
     * @return true if the node is in this graph
     */
    public boolean containsNode(N node) {
        return nodes.contains(node);
    }
    
    //------------------------------------------------------------------------------
    // INTERNAL STUFF
    //------------------------------------------------------------------------------
    
    private void onGraphChanged() {
        onGraphChangedListeners.forEach((listener) -> {
            listener.graphChanged();
        });
    };

    /**
     * Inserts the node into appropriate structures.
     * Assumes the node is a new node, not referenced anywhere in the graph.
     * 
     * @param node
     */
    private void insertNode(N node) {
        nodes.add( node );
        Incidence<N, E> incidenceContainer = new Incidence<>();
        adjacency.put( node, incidenceContainer );
    }
    
    /**
     * Inserts the edge into appropriate structures.
     * Assumes the edge is a new edge, not referenced anywhere in the graph.
     * Assumes both edge nodes (origin, destination) are already correctly present in the graph.
     * 
     * @param edge
     */
    private void insertEdge(E edge) {
        edges.add( edge );

        Incidence< N, E > originNodeInicidence = adjacency.get( edge.getOrigin() );
        Incidence< N, E > destinationNodeInicidence = adjacency.get( edge.getDestination() );

        if(edge.isDirected()) {
            originNodeInicidence.out( edge );
            destinationNodeInicidence.in( edge );
        } else {
            originNodeInicidence.undirected( edge );
            destinationNodeInicidence.undirected( edge );
        }
    }

    /**
     * Removes the node from this graph.
     * Assumes the node is already correctly present in the graph.
     * 
     * @param node
     */
    private void deleteNode(N node) {
        deleteEdgesForNode(node);

        adjacency.remove( node );
        nodes.remove(node);
    }

    /**
     * Removes the edge from this graph.
     * Assumes the edge is already correctly present in the graph.
     * 
     * @param edge
     */
    private void deleteEdge(E edge) {
        adjacency.get( edge.getOrigin() ).remove( edge );
        adjacency.get( edge.getDestination() ).remove( edge );
        edges.remove(edge);
    }

    /**
     * Removes all the edges incident on the node from this graph.
     * Assumes the node is already correctly present in the graph.
     * 
     * @param node
     */
    private void deleteEdgesForNode(N node) {
        ArrayList<E> nodeEdges = new ArrayList<>(adjacency.get( node ).all());

        for(E edge : nodeEdges) {
            deleteEdge(edge);
        }
    };

    //------------------------------------------------------------------------------
    // HANDY UTILITY METHODS
    //------------------------------------------------------------------------------
    
    public static enum TraversalType {
        ALL,
        DOWNSTREAM,
        UPSTREAM
    }

    public void traverseGraph(N root, TraversalType type, Consumer<N> consumer) {
        if(!nodes.contains( root )) {
            return;
        }

        // 'visited' Set helps us avoid visiting nodes that we already processed.
        // otherwise we might do the same work multiple times, and in the worst case scenario,
        // end up running in circles for ever and ever (actually, until we run out of memory :).
        // in general, nodes that get visited more than once are those nodes that have more than one parent.
        Set<N> visited = new HashSet<>();
        
        ArrayDeque<N> stack = new ArrayDeque<>();
        
        stack.push(root);
        
        while(!stack.isEmpty()) {
            N node = stack.pop();

            if (visited.contains(node)) {
                continue;
            }

            visited.add(node);

            consumer.accept(node);

            List< E > edgeList = new ArrayList<>();
            switch(type) {
                case ALL:
                    edgeList = adjacency.get( node ).all();
                    break;
                case DOWNSTREAM:
                    edgeList = adjacency.get( node ).out();
                    break;
                case UPSTREAM:
                    edgeList = adjacency.get( node ).in();
                    break;
            }

            for ( E edge : edgeList ) {
                stack.push(edge.getDestination());
            }
        }
    }

}
