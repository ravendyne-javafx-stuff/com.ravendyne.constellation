package com.ravendyne.constellation.graph.generic.gml;

import java.util.Map;

public interface IGraphEdgeFactory<N, E> {
    E create(N sourceNode, N targetNode, Map<String, GmlValue> edgeAttrs);
}
