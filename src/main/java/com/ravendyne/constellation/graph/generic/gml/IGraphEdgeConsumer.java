package com.ravendyne.constellation.graph.generic.gml;

import java.util.Map;

public interface IGraphEdgeConsumer {
    void accept(String sourceId, String targetId, Map<String, GmlValue> attrs);
}
