package com.ravendyne.constellation.graph.generic.gml;

import java.util.HashMap;
import java.util.Map;

import com.ravendyne.constellation.graph.generic.gml.mouseruntime.SourceString;

public class Gml {
    public static final String NODE_TAG = "node";
    public static final String EDGE_TAG = "edge";

    public static final String NODE_LIST_TAG = "nodes";
    public static final String NODE_ID_TAG = "id";
    public static final String NODE_LABEL_TAG = "label";

    public static final String EDGE_LIST_TAG = "edges";
    public static final String EDGE_SOURCE_TAG = "source";
    public static final String EDGE_TARGET_TAG = "target";
    
    public static final String ISOLATED_NODE_ID = String.valueOf((String)null);

    private Map<String, GmlValue> result;
    String errorMessage;

    public Gml(String source) {
        result = new HashMap<>();
        GmlParser parser = new GmlParser();
        SourceString src = new SourceString(source);
        boolean ok = parser.parse(src);

        GmlSemantics sem = parser.semantics();
        if (ok)
        {
            errorMessage = "";
            result = sem.getGml();
        } else {
            errorMessage = sem.getErrorMessage();
        }
    }
    
    public Map<String, GmlValue> get() {
        return result;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
}
