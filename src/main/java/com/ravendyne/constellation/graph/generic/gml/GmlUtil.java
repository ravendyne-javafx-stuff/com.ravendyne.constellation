package com.ravendyne.constellation.graph.generic.gml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import com.ravendyne.constellation.graph.generic.Graph;
import com.ravendyne.constellation.graph.generic.IGraphEdge;
import com.ravendyne.constellation.graph.generic.IGraphNode;

public class GmlUtil {
    private GmlUtil() {}

    public static Map<String, GmlValue> loadGml(String source) {
        Gml gmlFile = new Gml(source);
        
        if(!gmlFile.getErrorMessage().isEmpty()) {
            throw new IllegalArgumentException("GML file format error: " + gmlFile.getErrorMessage());
        }
        
        return gmlFile.get();
    }

    public static Map<String, GmlValue> loadGml(List<String> textSource) {
        String source = String.join("\n", textSource);
        return loadGml(source);
    }

    /**
     * Parses given graph key-value tree while calling callback parameters in the process.
     * <br><br>
     * <i>Isolated node</i> (consumed by <code>isolatedNodeConsumer</code>) is a node which has no ID.
     * <br><br>
     * Technically, these are strongly isolated nodes, since they can't be connected to any other node at all 
     * because they don't have an ID. A node with an ID can be isolated if it is not connected to any other node, but in the
     * context of this utility method, those (weakly) isolated nodes are not considered as isolated. Reason for this is 
     * because edge processing in this method happens after all nodes are processed, and in theory, any node which has ID 
     * could turn out to be connected with and edge to some other node, so they are cosumed by <code>nodeConsumer</code>
     * without further discrimination.
     * <br><br>
     * Callback parameters are called in following order:
     * <ol>
     *  <li><b>graphAttrsConsumer</b>, once for the graph</li>
     *  <li><b>nodeConsumer</b> or <b>isolatedNodeConsumer</b>, once for each node in the graph, depending on node type</li>
     *  <li><b>edgeConsumer</b>, once for each edge</li>
     * </ol>
     * 
     * @param gml
     * @param graphAttrsConsumer
     * @param nodeConsumer
     * @param edgeConsumer
     * @param isolatedNodeConsumer
     */
    public static void parseGraph(
            Map<String, GmlValue> gml, 
            Consumer<Map<String, GmlValue>> graphAttrsConsumer, 
            IGraphNodeConsumer nodeConsumer, 
            IGraphEdgeConsumer edgeConsumer,
            IGraphNodeConsumer isolatedNodeConsumer) {
        Objects.requireNonNull(gml);
        Objects.requireNonNull(graphAttrsConsumer);
        Objects.requireNonNull(nodeConsumer);
        Objects.requireNonNull(edgeConsumer);

        GmlValue graphValue = gml.get("graph");
        Objects.requireNonNull(graphValue, "GML has to have a root node named 'graph'");
        if(!graphValue.isMap()) {
            throw new IllegalArgumentException("GML 'graph' node has to be a map of values");
        }

        Map<String, GmlValue> graphMap = graphValue.getMap();

        //-----------------------------------------------------------
        // Graph attributes
        //  callbacks:
        //      - graphAttrs
        //-----------------------------------------------------------
        Map<String, GmlValue> graphAttributes = new HashMap<>();
        for(String key : graphMap.keySet()) {
            if(key.equals(Gml.NODE_LIST_TAG)) {
                continue;
            }
            if(key.equals(Gml.EDGE_LIST_TAG)) {
                continue;
            }
            graphAttributes.put(key, graphMap.get(key));
        }
        graphAttrsConsumer.accept(graphAttributes);

        //-----------------------------------------------------------
        // Collect nodes and edges
        //-----------------------------------------------------------
        GmlValue nodeValue = graphMap.get(Gml.NODE_LIST_TAG);
        GmlValue edgeValue = graphMap.get(Gml.EDGE_LIST_TAG);
        
        if(nodeValue == null) {
            // nothing else to do when there are no nodes
            return;
        }

        List<GmlValue> nodes = new ArrayList<>();
        if(nodeValue.isList()) {
            nodes.addAll(nodeValue.getList());
        }
        
        List<GmlValue> edges = new ArrayList<>();
        if (edgeValue != null && edgeValue.isList()) {
            edges.addAll(edgeValue.getList());
        }

        
        
        //-----------------------------------------------------------
        // Process nodes
        //  callbacks:
        //      - nodeConsumer
        //      - isolatedNodeConsumer
        //-----------------------------------------------------------
        Map<String, GmlValue> nodesMap = new HashMap<>();

        for(GmlValue nodeElement : nodes) {
            if(!nodeElement.isMap()) {
                continue;
            }
            Map<String, GmlValue> nodeAttrs = new HashMap<>( nodeElement.getMap() );
            
            GmlValue nodeIdValue = nodeAttrs.get(Gml.NODE_ID_TAG);
            String nodeId = String.valueOf(nodeIdValue);
            if(nodeIdValue != null) {
                if(nodesMap.containsKey(nodeIdValue.toString())) {
                    throw new IllegalArgumentException("GML graph has more than one node with attribute '" + Gml.NODE_ID_TAG + "' equal to '" + nodeIdValue.toString() + "'");
                }
    
                nodeConsumer.accept(nodeId, nodeAttrs);
                
                nodesMap.put(nodeId, nodeElement);
            } else {
                // node with no ID -> isolated node
                isolatedNodeConsumer.accept(Gml.ISOLATED_NODE_ID, nodeAttrs);
            }
        }

        //-----------------------------------------------------------
        // Process edges
        //  callbacks:
        //      - edgeConsumer
        //-----------------------------------------------------------
        for(GmlValue edgeElement : edges) {
            if(!edgeElement.isMap()) {
                continue;
            }
            Map<String, GmlValue> edgeAttrs = new HashMap<>( edgeElement.getMap() );
            
            GmlValue sourceId = edgeAttrs.get(Gml.EDGE_SOURCE_TAG);
            Objects.requireNonNull(sourceId, "a GML edge has to have an attribute named '" + Gml.EDGE_SOURCE_TAG + "'");
            GmlValue targetId = edgeAttrs.get(Gml.EDGE_TARGET_TAG);
            Objects.requireNonNull(targetId, "a GML edge has to have an attribute named '" + Gml.EDGE_TARGET_TAG + "'");

            GmlValue sourceNode = nodesMap.get(sourceId.toString());
            Objects.requireNonNull(sourceNode, "a GML edge references '" + Gml.EDGE_SOURCE_TAG + "' node with id '" + sourceId.toString() + "' which doesn't exist in the list of nodes");
            GmlValue targetNode = nodesMap.get(targetId.toString());
            Objects.requireNonNull(targetNode, "a GML edge references '" + Gml.EDGE_TARGET_TAG + "' node with id '" + targetId.toString() + "' which doesn't exist in the list of nodes");

            edgeConsumer.accept(sourceId.toString(), targetId.toString(), edgeAttrs);
        }
    }

    public static <N extends IGraphNode, E extends IGraphEdge<N>> Graph<N, E> loadGraph(
            Map<String, GmlValue> gml, 
            Consumer<Map<String, GmlValue>> graphAttrs, 
            IGraphNodeFactory<N> newNode, 
            IGraphEdgeFactory<N, E> newEdge) {
        Graph<N, E> graph = new Graph<>();
        Map<String, N> nodesMap = new HashMap<>();

        IGraphNodeConsumer nodeConsumer = (id, attrs) -> {
            N node = newNode.create(attrs);
            Objects.requireNonNull(node);
            graph.addNode(node);
            // parseGml() assures that ID is unique and non-null (via HashMap.containsKey())
            nodesMap.put(id, node);
        };
        IGraphNodeConsumer isolatedNodeConsumer = (x, attrs) -> {
            N node = newNode.create(attrs);
            Objects.requireNonNull(node);
            graph.addNode(node);
        };

        IGraphEdgeConsumer edgeConsumer = (source, target, attrs) -> {
            // parseGml() assures that source and target are IDs of existing nodes
            N sourceNode = nodesMap.get(source);
            N targetNode = nodesMap.get(target);
            E edge = newEdge.create(sourceNode, targetNode, attrs);
            Objects.requireNonNull(edge);
            graph.addEdge(edge);
        };

        parseGraph(gml, graphAttrs, nodeConsumer, edgeConsumer, isolatedNodeConsumer);
        
        return graph;
    }

}
