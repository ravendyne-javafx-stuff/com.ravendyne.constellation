package com.ravendyne.constellation.graph.generic.gml;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ravendyne.constellation.graph.generic.gml.mouseruntime.SourceString;

class Try {
    public static void main(String argv[]) throws Exception {
//        interactive();
        test();
    }

    static void test() {
//        String line = "graph [node [id 1]]";
//        String line = "graph [node [id 1 width 32.05 label \"another day in paradise\"]]";
        String line = "graph [name \"just the way it is\" node [id 1] node [id 2] node [id 3] edge [source 1 target 2] edge [source 2 target 3]]";

        GmlParser parser = new GmlParser(); // Instantiate Parser+Semantics
        SourceString src = new SourceString(line); // Wrap up the line
        boolean ok = parser.parse(src); // Apply Parser to it
        GmlSemantics sem = parser.semantics(); // Access Semantics
        if (ok) // If succeeded:
        {
            System.out.println(sem.getGml().toString()); // Get the tree and show it
        } else {
            System.out.println(sem.getErrorMessage());
        }
    }

    static void interactive() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        GmlParser parser = new GmlParser(); // Instantiate Parser+Semantics
        while (true) {
            System.out.print("> "); // Print prompt
            String line = in.readLine(); // Read line
            if (line.length() == 0)
                return; // Quit on empty line
            SourceString src = new SourceString(line); // Wrap up the line
            boolean ok = parser.parse(src); // Apply Parser to it
            if (ok) // If succeeded:
            {
                GmlSemantics sem = parser.semantics(); // Access Semantics
                System.out.println(sem.result.toString()); // Get the tree and show it
            }
        }
    }
}