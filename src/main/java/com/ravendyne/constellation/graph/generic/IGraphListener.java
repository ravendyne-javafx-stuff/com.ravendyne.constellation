package com.ravendyne.constellation.graph.generic;

public interface IGraphListener {
    void graphChanged();
}
