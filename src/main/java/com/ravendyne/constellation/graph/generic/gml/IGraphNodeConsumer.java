package com.ravendyne.constellation.graph.generic.gml;

import java.util.Map;

public interface IGraphNodeConsumer {
    void accept(String nodeId, Map<String, GmlValue> attrs);
}
