/**
 * Parser for Graph Modeling Language (GML) files.
 * <br><br>
 * The parser is created using <a href="http://mousepeg.sourceforge.net/">Mouse parser generator</a>.
 * <br><br>
 * GML grammar, as defined in the <a href="http://www.fim.uni-passau.de/fileadmin/files/lehrstuhl/brandenburg/projekte/gml/gml-technical-report.pdf">GML Technical Report</a>
 * has been cleaned up a bit and is included in <code>gml.txt</code> file.
 * <br><br>
 * Some parts of the specification are <i>not implemented</i>:
 * <ul>
 *      <li>Maximum line length and key size are <b>not</b> limited to 254 characters</li>
 *      <br>
 *      <li>{@link com.ravendyne.constellation.graph.generic.gml.Gml Gml} parser does <b>not</b> enforce uniqueness of node {@code ID} attributes 
 *          nor does it check if {@code source} or {@code target} attributes of edge exist or if they refer to existing nodes.<br>
 *          However, {@link com.ravendyne.constellation.graph.generic.gml.GmlUtil#parseGraph GmlUtil.parseGraph()} does check for all of the above.</li>
 *      <br>
 *      <li>Duplicate keys will result in <b>one entry</b> for the <b>current</b> map. Which entry will end up being used is unspecified.<br>
 *          The only time when this rule does not apply is for <code><b>graph</b></code> map and key names <code><b>node</b></code> and <code><b>edge</b></code>.
 *          These will be collected into two lists, one for nodes and the other for edges.</li>
 *      <br>
 *      <li>Only predefined keys for graphs are reserved: <code>graph, node, edge, node.id, edge.source, edge.target</code>, 
 *          all other key names, including global ones (id, label, comment, graphics etc.), are treated the same.</li>
 *      <br>
 * </ul>
 * 
 * @see <a href="http://www.fim.uni-passau.de/fileadmin/files/lehrstuhl/brandenburg/projekte/gml/gml-technical-report.pdf">GML Technical Report</a>
 */
package com.ravendyne.constellation.graph.generic.gml;
