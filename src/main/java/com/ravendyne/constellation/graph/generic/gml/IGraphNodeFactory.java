package com.ravendyne.constellation.graph.generic.gml;

import java.util.Map;

public interface IGraphNodeFactory<T> {
    T create(Map<String, GmlValue> nodeAttrs);
}
