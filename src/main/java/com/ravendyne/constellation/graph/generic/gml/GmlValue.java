package com.ravendyne.constellation.graph.generic.gml;

import java.util.List;
import java.util.Map;

public interface GmlValue {
    boolean isNumber();
    boolean isString();
    boolean isMap();
    boolean isList();
    Number getNumber();
    String getString();
    Map<String, GmlValue> getMap();
    List<GmlValue> getList();
}
