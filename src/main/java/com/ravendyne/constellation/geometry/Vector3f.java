package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;

/**
 * 3-component float vector.
 * 
 * This is immutable vector.
 *
 */
public class Vector3f {
    /** Number of elements in this vector. */
    public static final int SIZE = 3;
    /** Number of bytes this vector needs for all its elements. */
    public static final int BYTES = Float.BYTES * SIZE;
    
    public static final Vector3f ZERO           = new Vector3f( 0, 0, 0 );
    public static final Vector3f ONES           = new Vector3f( 1, 1, 1 );
    public static final Vector3f NORMAL_X       = new Vector3f( 1, 0, 0 );
    public static final Vector3f NORMAL_NEG_X   = new Vector3f( -1, 0, 0 );
    public static final Vector3f NORMAL_Y       = new Vector3f( 0, 1, 0 );
    public static final Vector3f NORMAL_NEG_Y   = new Vector3f( 0, -1, 0 );
    public static final Vector3f NORMAL_Z       = new Vector3f( 0, 0, 1 );
    public static final Vector3f NORMAL_NEG_Z   = new Vector3f( 0, 0, -1 );
    
    private final float x;
    private final float y;
    private final float z;

    public Vector3f(BufferVector3f bufferVector) {
        assert bufferVector != null;

        x = bufferVector.getX();
        y = bufferVector.getY();
        z = bufferVector.getZ();
    }

    public Vector3f(Vector4f otherVector) {
        assert otherVector != null;

        x = otherVector.getX();
        y = otherVector.getY();
        z = otherVector.getZ();
    }

    public Vector3f(float[] data, int offset, int stride) {
        assert data != null;
        assert data.length >= offset + 3 * stride;

        x = data[ offset + 0 * stride ];
        y = data[ offset + 1 * stride ];
        z = data[ offset + 2 * stride ];
    }

    public Vector3f(float[] data, int offset) {
        assert data != null;
        assert data.length >= offset + 3;

        x = data[ offset + 0 ];
        y = data[ offset + 1 ];
        z = data[ offset + 2 ];
    }

    public Vector3f(float[] data) {
        assert data != null;
        assert data.length >= 3;

        x = data[ 0 ];
        y = data[ 1 ];
        z = data[ 2 ];
    }

    public Vector3f(float x, float y, float z) {

        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float getZ() {
        return z;
    }
    
    public float getR() {
        return x;
    }
    
    public float getG() {
        return y;
    }
    
    public float getB() {
        return z;
    }
    
    public Vector3f add(Vector3f other) {

        float x = (getX() + other.getX());
        float y = (getY() + other.getY());
        float z = (getZ() + other.getZ());

        return new Vector3f(x, y, z);
    }
    
    public Vector3f subtract(Vector3f other) {

        float x = (getX() - other.getX());
        float y = (getY() - other.getY());
        float z = (getZ() - other.getZ());

        return new Vector3f(x, y, z);
    }
    
    public Vector3f multiply(float factor) {

        // for " + 0.0f" part, see https://stackoverflow.com/a/8153449
        float x = (getX() * factor) + 0.0f;
        float y = (getY() * factor) + 0.0f;
        float z = (getZ() * factor) + 0.0f;

        return new Vector3f(x, y, z);
    }
    
    public Vector3f divide(float factor) {

        // for " + 0.0f" part, see https://stackoverflow.com/a/8153449
        float x = (getX() / factor) + 0.0f;
        float y = (getY() / factor) + 0.0f;
        float z = (getZ() / factor) + 0.0f;

        return new Vector3f(x, y, z);
    }

    public Vector3f cross(Vector3f other) {

        float x = getY() * other.getZ() - getZ() * other.getY();
        float y = getZ() * other.getX() - getX() * other.getZ();
        float z = getX() * other.getY() - getY() * other.getX();

        return new Vector3f(x, y, z);
    }

    public float dot(Vector3f other) {

        return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ();
    }

    public Vector3f multiplyComponents(Vector3f other) {

        float x = getX() * other.getX();
        float y = getY() * other.getY();
        float z = getZ() * other.getZ();

        return new Vector3f(x, y, z);
    }

    public float length() {

        return (float) Math.sqrt(getX()*getX() + getY()*getY() + getZ()*getZ());
    }

    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public Vector3f normalize() {

        return divide(length());
    }
    
    /**
     * Returns byte array with vector components stored in (x, y, z) order.
     * The result array can be wrapped in {@link ByteBuffer} and read from using {@link ByteBuffer#asFloatBuffer()}, i.e.:
     * 
     * <pre>
     * FloatBuffer buff = ByteBuffer.wrap( aVector.asByteArray() ).asFloatBuffer();
     * float x = buff.get(0);
     * float y = buff.get(1);
     * float z = buff.get(2);
     * </pre>
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return byte array containing x, y, z components as floats
     */
    public byte[] asByteArray() {

        ByteBuffer buffer = ByteBuffer.wrap( new byte[BYTES] );
        buffer.asFloatBuffer().put(getX()).put(getY()).put(getZ());

        return Arrays.copyOf( buffer.array(), BYTES );
    }

    /**
     * Returns float array with vector components stored in (x, y, z) order.
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return float array containing x, y, z components as floats
     */
    public float[] asFloatArray() {

        FloatBuffer buffer = FloatBuffer.wrap( new float[SIZE] );
        buffer.put(getX()).put(getY()).put(getZ());

        return Arrays.copyOf( buffer.array(), SIZE );
    }

    public Vector4f asVector4f() {
        return new Vector4f(this);
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        dst.put(asByteArray(), 0, BYTES);
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(FloatBuffer dst) {
        dst.put(asFloatArray(), 0, SIZE);
    }
    
    @Override
    public String toString() {
        return String.format( "( %f, %f, %f )", x, y, z );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        result = prime * result + Float.floatToIntBits(z);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Vector3f))
            return false;
        Vector3f other = (Vector3f) obj;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
            return false;
        if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
            return false;
        return true;
    }
}
