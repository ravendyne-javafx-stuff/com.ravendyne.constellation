package com.ravendyne.constellation.geometry;

/**
 * Immutable 2D vector.
 *
 */
public class Vector2d {
    public static final int SIZE = 2;
    public static final int BYTES = Double.BYTES * SIZE;
    
    public final static Vector2d ZERO = new Vector2d( 0, 0 );

    public final double x;
    public final double y;
    private final String stringValue;

    public Vector2d(double x, double y) {
        stringValue = String.format( "( %f, %f )", x, y );
        this.x = x;
        this.y = y;
    }

    public Vector2d( Vector2d other ) {
        this(other.x, other.y);
    }

    public static Vector2d random() {
        return random(10.0);
    }

    public static Vector2d random(double magnitude) {
        return new Vector2d(2.0 * magnitude * (Math.random() - 0.5), 2.0 * magnitude * (Math.random() - 0.5));
    }

    public Vector2d add(Vector2d v2) {
        return new Vector2d(this.x + v2.x, this.y + v2.y);
    }

    public Vector2d subtract(Vector2d v2) {
        return new Vector2d(this.x - v2.x, this.y - v2.y);
    }

    public Vector2d multiply(double n) {
        return new Vector2d(this.x * n, this.y * n);
    }

    public Vector2d divide(double n) {
        if(n == 0) {
            return new Vector2d(this.x, this.y);
        }
        return new Vector2d((this.x / n), (this.y / n));
    }

    public double magnitude() {
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }

    public Vector2d flipY() {
        return new Vector2d(this.x, -this.y);
    }

    public Vector2d normalise() {
        return this.divide(this.magnitude());
    }

    public Vector2d reverse() {
        return new Vector2d(-this.x, -this.y);
    }
    
    public double dot(Vector2d other) {
        return this.x * other.x + this.y * other.y;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits( x );
        result = prime * result + ( int ) ( temp ^ ( temp >>> 32 ) );
        temp = Double.doubleToLongBits( y );
        result = prime * result + ( int ) ( temp ^ ( temp >>> 32 ) );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Vector2d other = ( Vector2d ) obj;
        if ( Double.doubleToLongBits( x ) != Double.doubleToLongBits( other.x ) )
            return false;
        if ( Double.doubleToLongBits( y ) != Double.doubleToLongBits( other.y ) )
            return false;
        return true;
    }

}
