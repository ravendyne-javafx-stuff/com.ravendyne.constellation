package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;

/**
 * 3D vector in homogeneous coordinate space.
 * 
 * This is immutable vector.
 *
 */
public class Vector4f {
    public static final int SIZE = 4;
    public static final int BYTES = Float.BYTES * SIZE;
    
    public static final Vector4f ZERO       = new Vector4f( 0, 0, 0, 0 );
    public static final Vector4f ONES       = new Vector4f( 1, 1, 1, 1 );
    public static final Vector4f NORMAL_X   = new Vector4f( 1, 0, 0 );
    public static final Vector4f NORMAL_Y   = new Vector4f( 0, 1, 0 );
    public static final Vector4f NORMAL_Z   = new Vector4f( 0, 0, 1 );

    private final float x;
    private final float y;
    private final float z;
    private final float w;

    public Vector4f(BufferVector4f bufferVector) {
        x = bufferVector.getX();
        y = bufferVector.getY();
        z = bufferVector.getZ();
        w = bufferVector.getW();
    }

    public Vector4f(BufferVector3f bufferVector) {
        x = bufferVector.getX();
        y = bufferVector.getY();
        z = bufferVector.getZ();
        w = 1.0f;
    }

    public Vector4f(Vector3f otherVector) {
        x = otherVector.getX();
        y = otherVector.getY();
        z = otherVector.getZ();
        w = 1.0f;
    }

    public Vector4f(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Vector4f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = 1.0f;
    }

    public Vector4f(float[] data, int offset, int stride) {
        x = data[ offset + 0 * stride ];
        y = data[ offset + 1 * stride ];
        z = data[ offset + 2 * stride ];
        w = data[ offset + 3 * stride ];
    }

    public Vector4f(float[] data, int offset) {
        this(data, offset, 1);
    }

    public Vector4f(float[] data) {
        this(data, 0, 1);
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float getZ() {
        return z;
    }
    
    public float getW() {
        return w;
    }
    
    public float getR() {
        return x;
    }
    
    public float getG() {
        return y;
    }
    
    public float getB() {
        return z;
    }
    
    public float getA() {
        return w;
    }

    public Vector4f add(Vector4f other) {
        float x = (getX() + other.getX());
        float y = (getY() + other.getY());
        float z = (getZ() + other.getZ());
        float W = (getW() + other.getW());

        return new Vector4f(x, y, z, W);
    }

    public Vector4f subtract(Vector4f other) {
        float x = (getX() - other.getX());
        float y = (getY() - other.getY());
        float z = (getZ() - other.getZ());
        float w = (getW() - other.getW());

        return new Vector4f(x, y, z, w);
    }
    
    public Vector4f multiply(float factor) {
        // for " + 0.0f" part, see https://stackoverflow.com/a/8153449
        float x = (getX() * factor) + 0.0f;
        float y = (getY() * factor) + 0.0f;
        float z = (getZ() * factor) + 0.0f;
        float w = (getW() * factor) + 0.0f;

        return new Vector4f(x, y, z, w);
    }
    
    public Vector4f divide(float factor) {
        // for " + 0.0f" part, see https://stackoverflow.com/a/8153449
        float x = (getX() / factor) + 0.0f;
        float y = (getY() / factor) + 0.0f;
        float z = (getZ() / factor) + 0.0f;
        float w = (getW() / factor) + 0.0f;

        return new Vector4f(x, y, z, w);
    }

    public Vector4f cross(Vector4f other) {
        float x = getY() * other.getZ() - getZ() * other.getY();
        float y = getZ() * other.getX() - getX() * other.getZ();
        float z = getX() * other.getY() - getY() * other.getX();

        return new Vector4f(x, y, z);
    }

    public float dot(Vector4f other) {
        return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ();
    }
    
    public float length() {
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public Vector4f unit() {
        Vector4f unitized = divide(length());
        return new Vector4f( unitized.getX(), unitized.getY(), unitized.getZ(), 1.0f);
    }
    
    /**
     * Converts the vector to homogeneous by normalizing over w component.
     * 
     * In other words, it divides x, y, z and w by w, yielding a vector whose w = 1.0 as a result.
     * 
     * @return
     */
    public Vector4f perspectiveDivide() {
        float x = getX();
        float y = getY();
        float z = getZ();
        float w = getW();

        return new Vector4f(x/w, y/w, z/w, 1.0f);
    }
    
    /**
     * Returns byte array with vector components stored in (x, y, z) order.
     * The result array can be wrapped in {@link ByteBuffer} and read from using {@link ByteBuffer#asFloatBuffer()}, i.e.:
     * 
     * <pre>
     * FloatBuffer buff = ByteBuffer.wrap( aVector.asByteArray() ).asFloatBuffer();
     * float x = buff.get(0);
     * float y = buff.get(1);
     * float z = buff.get(2);
     * </pre>
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return byte array containing x, y, z, w components as floats
     */
    public byte[] asByteArray() {
        ByteBuffer buffer = ByteBuffer.wrap( new byte[BYTES] );
        buffer.asFloatBuffer().put(x).put(y).put(z);
        return Arrays.copyOf( buffer.array(), BYTES );
    }

    /**
     * Returns float array with vector components stored in (x, y, z, w) order.
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return float array containing x, y, z, w components as floats
     */
    public float[] asFloatArray() {
        FloatBuffer buffer = FloatBuffer.wrap( new float[SIZE] );
        buffer.put(x).put(y).put(z).put(w);
        return Arrays.copyOf( buffer.array(), SIZE );
    }
    
    public Vector3f asVector3f() {
        return new Vector3f(this);
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        dst.put(asByteArray(), 0, BYTES);
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(FloatBuffer dst) {
        dst.put(asFloatArray(), 0, SIZE);
    }
    
    @Override
    public String toString() {
        return String.format( "( %f, %f, %f, %f )", x, y, z, w );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(w);
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        result = prime * result + Float.floatToIntBits(z);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Vector4f))
            return false;
        Vector4f other = (Vector4f) obj;
        if (Float.floatToIntBits(w) != Float.floatToIntBits(other.w))
            return false;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
            return false;
        if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
            return false;
        return true;
    }
}
