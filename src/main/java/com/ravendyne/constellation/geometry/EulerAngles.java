package com.ravendyne.constellation.geometry;

/**
 * http://www.gregslabaugh.net/publications/euler.pdf
 */
public class EulerAngles {
    
    public static enum Sequence {
        // Proper (classic) Euler angles (z-x-z, x-y-x, y-z-y, z-y-z, x-z-x, y-x-y)
        ZXZ,
        XYX,
        YZY,
        ZYZ,
        XZX,
        YXY,
        // Tait–Bryan angles (x-y-z, y-z-x, z-x-y, x-z-y, z-y-x, y-x-z).
        XYZ,
        YZX,
        ZXY,
        XZY,
        ZYX,
        YXZ;
    }
    
    public static final EulerAngles ZERO = new EulerAngles( 0, 0, 0 );
    
    private final Sequence sequence;
    
    /** Radians. */
    private final double rotationX;
    private final double rotationY;
    private final double rotationZ;

    public EulerAngles(double angleX, double angleY, double angleZ) {
        sequence = Sequence.XYZ;
        rotationX = angleX;
        rotationY = angleY;
        rotationZ = angleZ;
    }

    public EulerAngles(Sequence sequence, double angleX, double angleY, double angleZ) {
        this.sequence = sequence;
        rotationX = angleX;
        rotationY = angleY;
        rotationZ = angleZ;
    }

    public Matrix4f toMatrix() {
        switch(sequence) {
        case XYX:
            break;
        case XYZ:
            break;
        case XZX:
            break;
        case XZY:
            break;
        case YXY:
            break;
        case YXZ:
            break;
        case YZX:
            break;
        case YZY:
            break;
        case ZXY:
            break;
        case ZXZ:
            break;
        case ZYX:
            return
            Matrix4f.rotationX(rotationX)
            .multiply(
                    Matrix4f.rotationY(rotationY)
                    )
            .multiply(
                    Matrix4f.rotationZ(rotationZ)
                    );

        case ZYZ:
            break;
        default:
            break;
        
        }

        return Matrix4f.identity();
    }

    public Sequence getSequence() {
        return sequence;
    }

    public double getRotationX() {
        return rotationX;
    }

    public double getRotationY() {
        return rotationY;
    }

    public double getRotationZ() {
        return rotationZ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(rotationX);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rotationY);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rotationZ);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EulerAngles other = (EulerAngles) obj;
        if (Double.doubleToLongBits(rotationX) != Double.doubleToLongBits(other.rotationX))
            return false;
        if (Double.doubleToLongBits(rotationY) != Double.doubleToLongBits(other.rotationY))
            return false;
        if (Double.doubleToLongBits(rotationZ) != Double.doubleToLongBits(other.rotationZ))
            return false;
        if (sequence != other.sequence)
            return false;
        return true;
    }
}
