package com.ravendyne.constellation.geometry;

public class BoundingBox {
    
    public Vector2d bottomleft;
    public Vector2d topright;

    public BoundingBox(Vector2d bottomleft, Vector2d topright) {
        this.bottomleft = bottomleft;
        this.topright = topright;
    }

    @Override
    public String toString() {
        return "["+bottomleft+","+topright+"]";
    }

}
