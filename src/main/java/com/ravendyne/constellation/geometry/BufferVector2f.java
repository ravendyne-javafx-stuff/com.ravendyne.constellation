package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/**
 * 2D vector view of a (part of) byte array.
 * 
 * This is mutable vector.
 *
 */
public class BufferVector2f {
    protected static final int X_IDX = 0;
    protected static final int Y_IDX = 1;

    /** Number of elements in this vector. */
    public static final int SIZE = 2;
    /** Number of bytes this vector needs for all its elements. */
    public static final int BYTES = SIZE * Double.BYTES;

    /** Position in original buffer that this vector has been created at.
     * If original buffer was {@link ByteBuffer} it's number of bytes,
     * if it was {@link FloatBuffer} it's number of doubles. */
    protected final int position;
    
    protected FloatBuffer data;

    private BufferVector2f(FloatBuffer data, int position) {
        this.data = data;
        this.position = position;
    }
    
    /**
     * Creates an instance of 2D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #SIZE} subsequent elements.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector2f newInstance(FloatBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= SIZE;

        final BufferVector2f vector = new BufferVector2f(buffer.slice(), buffer.position());

        return vector;
    }
    
    /**
     * Creates an instance of 2D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #BYTES} subsequent bytes.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector2f newInstance(ByteBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= BYTES;

        final BufferVector2f vector = new BufferVector2f(buffer.asFloatBuffer(), buffer.position());

        return vector;
    }
    
    public float getX() {
        return data.get(X_IDX);
    }
    
    public float getY() {
        return data.get(Y_IDX);
    }
    
    public BufferVector2f setX(float x) {
        data.put(X_IDX, x);
        
        return this;
    }
    
    public BufferVector2f setY(float y) {
        data.put(Y_IDX, y);
        
        return this;
    }
    
    public BufferVector2f set(float x, float y) {
        setX(x);
        setY(y);
        
        return this;
    }
    
    public BufferVector2f add(BufferVector2f other) {
        setX(getX() + other.getX());
        setY(getY() + other.getY());

        return this;
    }
    
    public BufferVector2f subtract(BufferVector2f other) {
        setX(getX() - other.getX());
        setY(getY() - other.getY());

        return this;
    }
    
    public BufferVector2f multiply(float factor) {
        setX(getX() * factor);
        setY(getY() * factor);

        return this;
    }
    
    public BufferVector2f divide(float factor) {
        setX(getX() / factor);
        setY(getY() / factor);

        return this;
    }
    
    public float length() {
        final float x = getX();
        final float y = getY();

        return (float) Math.sqrt(x*x + y*y);
    }
    
    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public BufferVector2f normalize() {
        return divide(length());
    }
    
    public float dot(BufferVector2f other) {
        return getX() * other.getX() + getY() * other.getY();
    }
    
    /**
     * Turns this vector 90 degrees clockwise.
     * 
     * @return
     */
    public BufferVector2f cw90() {
        final float x = getX();
        final float y = getY();
        setX( - y );
        setY(   x );

        return this;
    }
    
    /**
     * Turns this vector 90 degrees counter-clockwise.
     * 
     * @return
     */
    public BufferVector2f ccw90() {
        final float x = getX();
        final float y = getY();
        setX(   y );
        setY( - x );

        return this;
    }

    public BufferVector2f copyTo(BufferVector2f other) {
        other.setX(getX());
        other.setY(getY());

        return this;
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        copyToBuffer(dst.asFloatBuffer());
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(FloatBuffer dst) {
        dst.put(data.array(), 0, SIZE);
    }
}
