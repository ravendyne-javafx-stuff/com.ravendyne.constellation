package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/**
 * 3D vector view of a (part of) byte array.
 * 
 * This is mutable vector.
 *
 */
public class BufferVector3i {
    protected static final int X_IDX = 0;
    protected static final int Y_IDX = 1;
    protected static final int Z_IDX = 2;

    /** Number of elements in this vector. */
    public static final int SIZE = 3;
    /** Number of bytes this vector needs for all its elements. */
    public static final int BYTES = SIZE * Integer.BYTES;

    /** Position in original buffer that this vector has been created at.
     * If original buffer was {@link ByteBuffer} it's number of bytes,
     * if it was {@link IntBuffer} it's number of doubles. */
    protected final int position;
    
    protected IntBuffer data;

    private BufferVector3i(IntBuffer data, int position) {
        this.data = data;
        this.position = position;
    }
    
    /**
     * Creates an instance of 3D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #SIZE} subsequent elements.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector3i newInstance(IntBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= SIZE;

        final BufferVector3i vector = new BufferVector3i(buffer.slice(), buffer.position());

        return vector;
    }
    
    /**
     * Creates an instance of 3D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #BYTES} subsequent bytes.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector3i newInstance(ByteBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= BYTES;

        final BufferVector3i vector = new BufferVector3i(buffer.asIntBuffer(), buffer.position());

        return vector;
    }
    
    public int getX() {
        return data.get(X_IDX);
    }
    
    public int getY() {
        return data.get(Y_IDX);
    }
    
    public int getZ() {
        return data.get(Z_IDX);
    }
    
    public BufferVector3i setX(int x) {
        data.put(X_IDX, x);
        
        return this;
    }
    
    public BufferVector3i setY(int y) {
        data.put(Y_IDX, y);
        
        return this;
    }
    
    public BufferVector3i setZ(int z) {
        data.put(Z_IDX, z);
        
        return this;
    }
    
    public BufferVector3i set(int x, int y, int z) {
        setX(x);
        setY(y);
        setZ(z);
        
        return this;
    }
    
    public BufferVector3i add(BufferVector3i other) {
        setX(getX() + other.getX());
        setY(getY() + other.getY());
        setZ(getZ() + other.getZ());

        return this;
    }
    
    public BufferVector3i subtract(BufferVector3i other) {
        setX(getX() - other.getX());
        setY(getY() - other.getY());
        setZ(getZ() - other.getZ());

        return this;
    }
    
    public BufferVector3i multiply(int factor) {
        setX(getX() * factor);
        setY(getY() * factor);
        setZ(getZ() * factor);

        return this;
    }
    
    public BufferVector3i divide(int factor) {
        setX(getX() / factor);
        setY(getY() / factor);
        setZ(getZ() / factor);

        return this;
    }
    
    public int length() {
        final int x = getX();
        final int y = getY();
        final int z = getZ();

        return (int) Math.sqrt(x*x + y*y + z*z);
    }
    
    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public BufferVector3i normalize() {
        return divide(length());
    }
    
    public double dot(BufferVector3i other) {
        return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ();
    }

    public BufferVector3i copyTo(BufferVector3i other) {
        other.setX(getX());
        other.setY(getY());
        other.setZ(getZ());

        return this;
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        copyToBuffer(dst.asIntBuffer());
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(IntBuffer dst) {
        dst.put(data.array(), 0, SIZE);
    }
    
    @Override
    public String toString() {
        return "[" + getX() + ", " + getY() + ", " + getZ() + "]";
    }
}
