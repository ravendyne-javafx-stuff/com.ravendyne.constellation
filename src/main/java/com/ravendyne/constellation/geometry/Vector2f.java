package com.ravendyne.constellation.geometry;

/**
 * Immutable 2D vector.
 *
 */
public class Vector2f {
    public static final int SIZE = 2;
    public static final int BYTES = Float.BYTES * SIZE;
    
    public final static Vector2f ZERO = new Vector2f();

    private final float x;
    private final float y;
    private final String stringValue;

    private Vector2f() {
        x = 0.0f;
        y = 0.0f;
        stringValue = String.format( "( %f, %f )", x, y );
    }

    public Vector2f(float x, float y) {
        stringValue = String.format( "( %f, %f )", x, y );
        this.x = x;
        this.y = y;
    }

    public Vector2f( Vector2f other ) {
        this(other.x, other.y);
    }

    public static Vector2f random() {
        return random(10.0f);
    }

    public static Vector2f random(float magnitude) {
        float xr = 2.0f * magnitude * (float)(Math.random() - 0.5);
        float yr = 2.0f * magnitude * (float)(Math.random() - 0.5);
        return new Vector2f(xr, yr);
    }

    public Vector2f add(Vector2f v2) {
        return new Vector2f(this.x + v2.x, this.y + v2.y);
    }

    public Vector2f subtract(Vector2f v2) {
        return new Vector2f(this.x - v2.x, this.y - v2.y);
    }

    public Vector2f multiply(float n) {
        return new Vector2f(this.x * n, this.y * n);
    }

    public Vector2f divide(float n) {
        if(n == 0) {
            return new Vector2f(this.x, this.y);
        }
        return new Vector2f((this.x / n), (this.y / n));
    }

    public float magnitude() {
        return (float) Math.sqrt(this.x*this.x + this.y*this.y);
    }

    public Vector2f flipY() {
        return new Vector2f(this.x, -this.y);
    }

    public Vector2f normalise() {
        return this.divide(this.magnitude());
    }

    public Vector2f reverse() {
        return new Vector2f(-this.x, -this.y);
    }
    
    public float dot(Vector2f other) {
        return this.x * other.x + this.y * other.y;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }

    public float getU() {
        return x;
    }
    
    public float getV() {
        return y;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits( x );
        result = prime * result + Float.floatToIntBits( y );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Vector2f other = ( Vector2f ) obj;
        if ( Float.floatToIntBits( x ) != Float.floatToIntBits( other.x ) )
            return false;
        if ( Float.floatToIntBits( y ) != Float.floatToIntBits( other.y ) )
            return false;
        return true;
    }

}
