package com.ravendyne.constellation.geometry;

/**
 * Immutable 2D vector.
 *
 */
public class Vector2i {
    public static final int SIZE = 2;
    public static final int BYTES = Integer.BYTES * SIZE;
    
    public final static Vector2i ZERO = new Vector2i( 0, 0 );

    public final int x;
    public final int y;
    private final String stringValue;

    public Vector2i(int x, int y) {
        stringValue = String.format( "( %d, %d )", x, y );
        this.x = x;
        this.y = y;
    }

    public Vector2i(double x, double y) {
        stringValue = String.format( "( %d, %d )", x, y );
        this.x = (int) x;
        this.y = (int) y;
    }

    public Vector2i( Vector2i other ) {
        this(other.x, other.y);
    }

    public static Vector2i random() {
        return random(10);
    }

    public static Vector2i random(int magnitude) {
        return new Vector2i(2.0 * magnitude * (Math.random() - 0.5), 2.0 * magnitude * (Math.random() - 0.5));
    }

    public Vector2i add(Vector2i v2) {
        return new Vector2i(this.x + v2.x, this.y + v2.y);
    }

    public Vector2i subtract(Vector2i v2) {
        return new Vector2i(this.x - v2.x, this.y - v2.y);
    }

    public Vector2i multiply(double n) {
        return new Vector2i(this.x * n, this.y * n);
    }

    public Vector2i divide(double n) {
        if(n == 0) {
            return new Vector2i(this.x, this.y);
        }
        return new Vector2i((this.x / n), (this.y / n));
    }

    public int magnitude() {
        return (int) Math.sqrt(this.x*this.x + this.y*this.y);
    }

    public Vector2i flipY() {
        return new Vector2i(this.x, -this.y);
    }

    public Vector2i normalise() {
        return this.divide(this.magnitude());
    }

    public Vector2i reverse() {
        return new Vector2i(-this.x, -this.y);
    }
    
    public int dot(Vector2i other) {
        return this.x * other.x + this.y * other.y;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Vector2i))
            return false;
        Vector2i other = (Vector2i) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }
}
