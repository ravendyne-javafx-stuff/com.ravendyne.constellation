package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

/**
 * 3-component int vector.
 * 
 * This is immutable vector.
 *
 */
public class Vector3i {
    /** Number of elements in this vector. */
    public static final int SIZE = 3;
    /** Number of bytes this vector needs for all its elements. */
    public static final int BYTES = Integer.BYTES * SIZE;
    
    public static final Vector3i ZERO = new Vector3i();
    public static final Vector3i NORMAL_X = new Vector3i( 1, 0, 0 );
    public static final Vector3i NORMAL_Y = new Vector3i( 0, 1, 0 );
    public static final Vector3i NORMAL_Z = new Vector3i( 0, 0, 1 );
    
    private final int x;
    private final int y;
    private final int z;

    private Vector3i() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3i(BufferVector3f bufferVector) {
        x = ( int ) bufferVector.getX();
        y = ( int ) bufferVector.getY();
        z = ( int ) bufferVector.getZ();
    }

    public Vector3i(BufferVector3i bufferVector) {
        x = bufferVector.getX();
        y = bufferVector.getY();
        z = bufferVector.getZ();
    }

    public Vector3i(Vector3i otherVector) {
        x = otherVector.getX();
        y = otherVector.getY();
        z = otherVector.getZ();
    }

//    public Vector3i(Vector4f otherVector) {
//        x = otherVector.getX();
//        y = otherVector.getY();
//        z = otherVector.getZ();
//    }

    public Vector3i(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getZ() {
        return z;
    }
    
    public int getR() {
        return x;
    }
    
    public int getG() {
        return y;
    }
    
    public int getB() {
        return z;
    }
    
    public Vector3i add(Vector3i other) {
        int x = (getX() + other.getX());
        int y = (getY() + other.getY());
        int z = (getZ() + other.getZ());

        return new Vector3i(x, y, z);
    }
    
    public Vector3i subtract(Vector3i other) {
        int x = (getX() - other.getX());
        int y = (getY() - other.getY());
        int z = (getZ() - other.getZ());

        return new Vector3i(x, y, z);
    }
    
    public Vector3i multiply(int factor) {
        int x = (getX() * factor);
        int y = (getY() * factor);
        int z = (getZ() * factor);

        return new Vector3i(x, y, z);
    }
    
    public Vector3i divide(int factor) {
        int x = (getX() / factor);
        int y = (getY() / factor);
        int z = (getZ() / factor);

        return new Vector3i(x, y, z);
    }

    public Vector3i cross(Vector3i other) {
        int x = getY() * other.getZ() - getZ() * other.getY();
        int y = getZ() * other.getX() - getX() * other.getZ();
        int z = getX() * other.getY() - getY() * other.getX();

        return new Vector3i(x, y, z);
    }

    public int dot(Vector3i other) {
        return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ();
    }
    
    public int length() {
        return (int) Math.sqrt(getX()*getX() + getY()*getY() + getZ()*getZ());
    }

    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public Vector3i normalize() {
        return divide(length());
    }

    /**
     * Returns byte array with vector components stored in (x, y, z) order.
     * The result array can be wrapped in {@link ByteBuffer} and read from using {@link ByteBuffer#asFloatBuffer()}, i.e.:
     * 
     * <pre>
     * FloatBuffer buff = ByteBuffer.wrap( aVector.asByteArray() ).asFloatBuffer();
     * int x = buff.get(0);
     * int y = buff.get(1);
     * int z = buff.get(2);
     * </pre>
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return byte array containing x, y, z components as ints
     */
    public byte[] asByteArray() {
        ByteBuffer buffer = ByteBuffer.wrap( new byte[BYTES] );
        buffer.asFloatBuffer().put(getX()).put(getY()).put(getZ());
        return Arrays.copyOf( buffer.array(), BYTES );
    }

    /**
     * Returns int array with vector components stored in (x, y, z) order.
     * 
     * Mostly used for dumping to stream, files etc.
     * 
     * @return int array containing x, y, z components as ints
     */
    public int[] asIntArray() {
        IntBuffer buffer = IntBuffer.wrap( new int[SIZE] );
        buffer.put(getX()).put(getY()).put(getZ());
        return Arrays.copyOf( buffer.array(), SIZE );
    }

//    public Vector4f asVector4f() {
//        return new Vector4f(this);
//    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        dst.put(asByteArray(), 0, BYTES);
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(IntBuffer dst) {
        dst.put(asIntArray(), 0, SIZE);
    }
    
    @Override
    public String toString() {
        return String.format( "( %d, %d, %d )", x, y, z );
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Vector3i other = ( Vector3i ) obj;
        if ( x != other.x )
            return false;
        if ( y != other.y )
            return false;
        if ( z != other.z )
            return false;
        return true;
    }
}
