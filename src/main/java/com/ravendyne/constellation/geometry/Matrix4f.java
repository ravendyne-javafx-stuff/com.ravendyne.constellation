package com.ravendyne.constellation.geometry;

import java.util.Arrays;

/**
 * Homogeneous coordinates affine transform 4x4 matrix.
 * This is 4x4 immutable matrix, backed by an array of floats in row-major order.
 * The following is an explanation of what "array of floats in row-major order" means
 * as well as some other notes and clarifications about matrices in this library.
 * <br><br>
 * We will declare/define our matrices in the code like this:
 * 
 * <pre>
        Matrix4f matrixObject = new Matrix4f(
            1, 0, 0, x,
            0, 1, 0, y,
            0, 0, 1, z,
            0, 0, 0, 1
        );
 * </pre>
 * 
 * or like this:
 * 
 * <pre>
        float[] matrixArray = new float[] {
            1, 0, 0, x,
            0, 1, 0, y,
            0, 0, 1, z,
            0, 0, 0, 1
        };
 * </pre>
 * 
 * In the case of {@code matrixArray} declaration, the first row of our matrix is
 * stored in the first 4 elements of the {@code float} array, second row is stored in the next 4, etc.
 * We now need to decide what layout will we use, how will vector xyz-components be placed, like this:
 * 
 * <pre>
        float[] columnMajorMatrix = new float[] {
            x, x, x, x,
            y, y, y, y,
            z, z, z, z,
            0, 0, 0, 1
        };
 * </pre>
 * 
 * or like this:
 * 
 * <pre>
        float[] rowMajorMatrix = new float[] {
            x, y, z, 0,
            x, y, z, 0,
            x, y, z, 0,
            x, y, z, 1
        };
 * </pre>
 * 
 * Since most of math texts prefer to represent vectors with their elements placed in a vertical column, i.e.:
 * 
 * <pre>
            | x |
        V = | y |
            | z |
            | 1 |
 * </pre>
 * 
 * and represent matrices as rows of vertical vectors (columns), i.e.:
 * 
 * <pre>
            | x1 x2 x3 0 |
        M = | y1 y2 y3 0 |
            | z1 z2 z3 0 |
            | 0  0  0  1 |
 * </pre>
 * 
 * we will opt for representation in our code as given by {@code columnMajorMatrix} declaration example above.
 * This determines few important characteristics of our vector/matrix code:
 * 
 * <ul>
 * 
 * <li>
 * First 4 elements in {@code matrixArray} declaration above will represent the first row of a matrix stored in it, because we decided to read our matrix declarations
 * as having a <a href="https://en.wikipedia.org/wiki/Row-_and_column-major_order#/media/File:Row_and_column_major_order.svg">column major order</a> (i.e. as {@code columnMajorMatrix} declaration above ).
 * <br> This makes our {@code float[]} arrays a row major order storages of matrices.
 * </li>
 * 
 * <li>
 * When defining translation vector in an affine transformation matrix, vector components in column-major matrix are placed like this:
 * <pre>
            | 1 0 0 xt |
        M = | 0 1 0 yt |
            | 0 0 1 zt |
            | 0 0 0 1  |
 * </pre>
 * This means that our {@code float[]} array matrix representation will look like this:
 * <pre>
       [ 1, 0, 0, xt, 0, 1, 0, yt, 0, 0, 1, zt, 0, 0, 0, 1 ]
 * </pre>
 * However, OpenGL <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glUniform.xhtml">glUniformMatrix*()</a> API expects translation vector components to be at 13-th, 14-th and 15-th
 * positions of a 16-element array representing the matrix, where indexing is assumed to be 1-based. In other words, we should pass this array to <b>glUniformMatrix*()</b>:
 * <pre>
       [ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, xt, yt, zt, 1 ]
 * </pre>
 * We can do that in two ways, either we transpose it befor passing it to OpenGL:
 * <pre>
       Matrix4f matrix = new Matrix4f(...);
       float[] matrixArray = matrix.transpose().toArray();
       glUniformMatrix4fv( location, 1, false, matrixArray  );
 * </pre>
 * or we let OpenGL do the transpose:
 * <pre>
       Matrix4f matrix = new Matrix4f(...);
       float[] matrixArray = matrix.toArray();
       glUniformMatrix4fv( location, 1, true, matrixArray  );
 * </pre>
 * </li>
 * </ul>
 * 
 * Also, since most math papers about matrices in 3D graphics world out there multiply with vector from the right side, i.e.:
 * 
 * <pre>
       VT = Mp x Mw * Mm * V
 * </pre>
 * 
 * as opposed to multiplying from the left, i.e.:
 * 
 * <pre>
       VT = V x Mm x Mw * Mp
 * </pre>
 * 
 * we will adhere to the former standard, i.e.
 * 
 * <pre>
       Vector4f Vt, V;
       Matrix4f Mm, Mw, Mp;
       VT = Mp.multiply( Mw ).multiply( Mm ). multiply( V );
 * </pre>
 * 
 * Note that above multiplications, assuming Mp - projection transform, Mv - world transform, Mm - model transform, should read like this:
 * <ol>
 * <li>First apply model transform to vertex V</li>
 * <li>Then apply world transform</li>
 * <li>And finally apply projection transform</li>
 * </ol>
 * 
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glUniform.xhtml
 * 
 * https://en.wikipedia.org/wiki/Transformation_matrix
 * 
 * https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions
 * https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
 * 
 * http://www.bluebit.gr/matrix-calculator/matrix_multiplication.aspx
 */
public class Matrix4f {
    public static final int DIMENSION = 4;
    public static final int SIZE = DIMENSION * DIMENSION;
    public static final int BYTES = SIZE * Float.BYTES;
    
    private static float[] IDENTITY = new float[] {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
       };
    
    public static float[] identityArray() {

        return Arrays.copyOf(IDENTITY, SIZE);
    }

    private float[] elements;


    //////////////////////////////////////////////////////////////////////////
    //
    // Constructors
    //
    //////////////////////////////////////////////////////////////////////////
    public Matrix4f() {

        elements = identityArray();
    }
    
    public Matrix4f(Matrix4f other) {
        
        elements = new float[SIZE];

        System.arraycopy( other.elements, 0, elements, 0, SIZE );
    }
    
    /**
     * Create matrix from a list of elements.
     * 
     * <p>
     * At most {@link #SIZE} elements will be used to populate this matrix. If there's less than {@link #SIZE}
     * values, the remaining elements in the matrix will be 0.
     * </p>
     * <p>
     * First 4 parameters are row 1, second 4 parameters are row 2 etc.
     * </p>
     */
    public Matrix4f( float ...values ) {
        
        int elementCount = values.length > SIZE ? SIZE : values.length;
        
        elements = new float[SIZE];
        Arrays.fill( elements, 0 );
        System.arraycopy( values, 0, elements, 0, elementCount );
    }

    public Matrix4f fromArray( float[] array ) {
        assert array != null;

        return fromArray(array, 0);
    }

    public Matrix4f fromArray( float[] array, int offset ) {
        assert array != null;
        assert array.length >= offset + SIZE;

        System.arraycopy( array, offset, elements, 0, SIZE );

        return this;
    }

    public float[] toArray() {

        return toArray( null, 0 );
    }

    public float[] toArray( float[] array ) {

        return toArray( array, 0 );
    }

    public float[] toArray( float[] array, int offset ) {

        if ( array == null ) {
            array = new float[SIZE];
            offset = 0;
        }
        assert array.length >= offset + SIZE;

        System.arraycopy( elements, 0, array, offset, SIZE );

        return array;
    }

    //////////////////////////////////////////////////////////////////////////
    //
    // Matrix operations
    //
    //////////////////////////////////////////////////////////////////////////
    public Matrix4f transpose() {
        
        Matrix4f result = new Matrix4f(this);

        float[] dstElements = result.elements;
        final int offsetFactor = DIMENSION - 1;

        for(int colidx = 0; colidx < DIMENSION; colidx++) {
            for(int rowidx = 0; rowidx < DIMENSION; rowidx++) {
                int srcOffset = toArrayOffset(colidx, rowidx);
                int transposedOffset = srcOffset + offsetFactor * (colidx - rowidx);
                dstElements[transposedOffset] = elements[srcOffset];
            }
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////
    //
    // Arithmetic operations
    //
    //////////////////////////////////////////////////////////////////////////
    public Matrix4f multiply(float scalar) {
        
        Matrix4f result = new Matrix4f();
        final float[] resultElements = result.elements;
        
        for(int idx = 0; idx < SIZE; idx++) {
            resultElements[idx] = elements[idx] * scalar;
        }

        return result;
    }

    public Vector4f multiply(Vector3f vector) {
        return multiply(vector.asVector4f());
    }

    public Vector4f multiply(Vector4f vector) {
        float[] vectorArray = vector.asFloatArray();
        
        float x = array4Dot(elements, 0, vectorArray, 0, 1);
        float y = array4Dot(elements, 1, vectorArray, 0, 1);
        float z = array4Dot(elements, 2, vectorArray, 0, 1);
        float w = array4Dot(elements, 3, vectorArray, 0, 1);

        return new Vector4f(x, y, z, w);
    }

    public Matrix4f multiply(Matrix4f matrix) {
        float[] matrixArray = matrix.toArray();
        
        float result[] = new float[SIZE];
        
        for(int column = 0; column < DIMENSION; column++) {

            result[ toArrayOffset( column, 0 ) ] = array4Dot(elements, 0, matrixArray, column, DIMENSION);
            result[ toArrayOffset( column, 1 ) ] = array4Dot(elements, 1, matrixArray, column, DIMENSION);
            result[ toArrayOffset( column, 2 ) ] = array4Dot(elements, 2, matrixArray, column, DIMENSION);
            result[ toArrayOffset( column, 3 ) ] = array4Dot(elements, 3, matrixArray, column, DIMENSION);
        }
        
        Matrix4f resultMatrix = new Matrix4f().fromArray(result);

        return resultMatrix;
    }
    
    private static final int toArrayOffset( int column, int row ) {
        return column + row * DIMENSION;
    }
    
    private static final int toArrayOffset( int column, int row, int STRIDE ) {
        return column + row * STRIDE;
    }

    /**
     * dot-product of left matrix row with right matrix column.
     * 
     * <p>
     * Also works with 4 element vectors, in which case {@code rightColumn} has to be set to {@code 0}
     * and {@code STRIDE} has to be set to {@code 1}.
     * </p>
     * 
     * @param leftArray
     * @param leftOffset
     * @param rightArray
     * @param rightOffset
     * @param STRIDE
     * @return
     */
    private static float array4Dot(float[] leftArray, int leftRow, float[] rightArray, int rightColumn, int STRIDE ) {
        
        float result = 0;

        result = 
                leftArray[ toArrayOffset( 0, leftRow ) ] * rightArray[ toArrayOffset( rightColumn, 0, STRIDE ) ] +
                leftArray[ toArrayOffset( 1, leftRow ) ] * rightArray[ toArrayOffset( rightColumn, 1, STRIDE ) ] +
                leftArray[ toArrayOffset( 2, leftRow ) ] * rightArray[ toArrayOffset( rightColumn, 2, STRIDE ) ] +
                leftArray[ toArrayOffset( 3, leftRow ) ] * rightArray[ toArrayOffset( rightColumn, 3, STRIDE ) ]
                ;

        return result;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(elements);
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Matrix4f other = (Matrix4f) obj;
        if (!Arrays.equals(elements, other.elements))
            return false;

        return true;
    }
    
    @Override
    public String toString() {
        Vector4f row1 = new Vector4f(elements, 0);
        Vector4f row2 = new Vector4f(elements, 4);
        Vector4f row3 = new Vector4f(elements, 8);
        Vector4f row4 = new Vector4f(elements, 12);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(row1);
        sb.append("\n");
        sb.append(row2);
        sb.append("\n");
        sb.append(row3);
        sb.append("\n");
        sb.append(row4);
        sb.append("]\n");
        return sb.toString();
    }

    //////////////////////////////////////////////////////////////////////////
    //
    // Utility (static) methods
    //
    //////////////////////////////////////////////////////////////////////////
    /**
     * Transforms cuboid viewing volume (left,right)-(bottom,top)-(near,far) into normalized device coordinates (-1,+1)-(-1,+1)-(-1,+1).
     * 
     * <p>
     * Given viewing volume is first centered to (0,0,0) and then scaled to [-1,+1] range on all axes.
     * </p>
     * 
     * @param left lower limit of view volume on X-axis
     * @param right upper limit of view volume on X-axis
     * @param bottom lower limit of view volume on Y-axis
     * @param top upper limit of view volume on Y-axis
     * @param near lower limit of view volume on Z-axis
     * @param far upper limit of view volume on Z-axis
     * @return matrix which scales the volume to [-1.+1] range on all axes
     */
    public static Matrix4f viewVolumeTransform( float left, float right, float bottom, float top, float near, float far ) {

        Matrix4f translateToCanonicalVolume = translation(
                - (left + right) / 2.0f,
                - (top + bottom) / 2.0f,
                - (far + near) / 2.0f
                );

        Matrix4f scaleToCanonicalVolume = scale(
                Math.abs( 2.0f / (right - left) ),
                Math.abs( 2.0f / (top - bottom) ),
                Math.abs( 2.0f / (far - near) )
                );

        // translate, then scale
        Matrix4f matrix = scaleToCanonicalVolume.multiply( translateToCanonicalVolume );

        return matrix;
    }

    public static Matrix4f viewVolumeTransform( float width, float height, float near, float far ) {

        Matrix4f translateToCanonicalVolume = translation(
                0,
                0,
                (far + near) / 2.0f
                );

        Matrix4f scaleToCanonicalVolume = scale(
                Math.abs( 2.0f / width ),
                Math.abs( 2.0f / height ),
                Math.abs( 2.0f / (far - near) )
                );

        // translate, then scale
        Matrix4f matrix = scaleToCanonicalVolume.multiply( translateToCanonicalVolume );

        return matrix;
    }

    /**
     * Transforms cuboid viewing volume centered on Z-axis.
     * 
     * @param width cuboid view volume width, on X-axis
     * @param height cuboid view volume height, on Y-axis
     * @param near cuboid view volume near face position on Z-axis
     * @param far cuboid view volume far face position on Z-axis
     * @return
     */
//    public static Matrix4f viewVolumeTransform( float width, float height, float near, float far ) {
//        return viewVolumeTransform( -width/2.0f, width/2.0f, -height/2.0f, height/2.0f, near, far );
//    }

    /**
     * Orthogonal/orthographic projection on the XY-plane.
     * 
     * <p>
     * This projection simply sets Z-coordinate to 0.
     * </p>
     * <p>
     * To get projection to XY-plane in normalized device coordinates ([-1,+1] range),
     * first apply matrix returned by {@link #viewVolumeTransform(float, float, float, float, float, float)} and then the one returned
     * by this method.
     * </p>
     */
    public static Matrix4f orthographicProjection() {

        // -1 -> Z axis IS inverted
        // +1 -> Z axis NOT-inverted
        final float invertZAxis = -1.0f;

        Matrix4f matrix = new Matrix4f(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, invertZAxis, 0,
                0, 0, 0, 1
            );
        
        return matrix;
    }

    /**
     * Perspective projection on a plane parallel to XY-plane.
     * 
     * <p>
     * This projection assumes it will be used on viewing volume in normalized device coordinates, centered at (0,0,0).
     * This projection will stretch z coordinates depending on the view angle (FOV).
     * </p>
     * <p>
     * To transform vertex space to this normalized viewing volume, use the matrix returned by {@link #viewVolumeTransform(float, float, float, float, float, float)}.
     * </p>
     * <p>
     * 
     * Perspective projection matrix returned by this method assumes:
     * 
     * <ul>
     * <li>Projection plane is parallel to XY-plane and intersects Z-axis at 0.</li>
     * <li>Near plane is parallel to XY-plane and intersects Z-axis at -1.</li>
     * <li>Far plane is parallel to XY-plane and intersects Z-axis at +1.</li>
     * <li>FOV angle is the view angle of the near plane square as viewed from camera eye position on Z-axis.</li>
     * <li>Since all planes are square ([-1,+1] coordinate range) field of view angle is the same in both vertical (YZ) and horizontal (XZ) planes.</li>
     * </ul>
     * 
     * Essentially, this projection transforms view volume of tapered rectangular shape (square frustum) to cuboid volume.
     * </p>
     * <p>
     * Typical usage would be:
     * 
     * <pre>
     *  Matrix4f viewTransform = Matrix4f.viewVolumeTransform( 800, 600, 50, 1500 );
     *  Matrix4f perspectiveProjection = Matrix4f.perspectiveProjection( 60 );
     *  Matrix4f transform = perspectiveProjection.multiply( viewTransform );
     *  
     *  Vector4f vector = new Vector4f( ... );
     *  Vector4f projected = transform.multiply( vector ).normalize();
     * </pre>
     * </p>
     * 
     * Notice two <b>important</b> things:
     * 
     * <ul>
     * <li>perspective projection should be the left-most matrix in multiplication (unless you are applying {@link #viewport(float, float)} matrix, in which case that one should be the right-most)</li>
     * <li>result of multiplying the matrix with a vector <b>has to be</b> normalized by calling {@link Vector4f#perspectiveDivide()}</li>
     * </ul>
     * 
     * @param fov field of view angle in degrees
     * @return
     */
    // TODO clean up javadoc

    public static Matrix4f perspectiveProjection2( float fov, float aspect, float near, float far ) {

        fov = ( float ) Math.toRadians( fov );
        float theta = ( float ) Math.tan( fov / 2 );
        float H = 2 * near * theta;
        float Sx1 = 2 / H;
        float Sy = 2 / H;
        float Sz1 = 2 / H;

        // -1 -> Z axis IS inverted
        // +1 -> Z axis NOT-inverted
        final float invertZAxis = -1.0f;

        Vector3f Tv = new Vector3f( 0, 0, (near + far) / 2 );
        Matrix4f translateMatrix = Matrix4f.translation( Tv );
        Matrix4f scale1Matrix = Matrix4f.scale( Sx1, Sy, Sz1 );
        Matrix4f perspectiveMatrix = new Matrix4f(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, - theta, (far + near) / (2*near)
                );

        float Sx2 = 1 / aspect;
        float Sz2 = H * ( 2 / (far-near) );

        // OpenGL uses left-hand coordinate system where Z-axis has the same direction as camera axis
        // X-axis has direction from left to right on camera projection plane and
        // Y-axis has direction from bottom to top on camera projection plane
        // In this last step we invert Z-axis by multiplying Z coordinates by -1

        Matrix4f scale2Matrix = Matrix4f.scale( Sx2, 1, invertZAxis * Sz2 );

        Matrix4f projection = scale2Matrix.multiply( perspectiveMatrix ).multiply( scale1Matrix ).multiply( translateMatrix );
        
        return projection;
    }

    /**
     * Transformation from [-1,1] range in XY-plane to (0,0)-(width,height) rectangle
     */
    public static Matrix4f viewport( float width, float height ) {
        Matrix4f scaleToViewport = new Matrix4f(
                width / 2.0f, 0,             0, 0,
                0,            height / 2.0f, 0, 0,
                0,            0,             0, 0,
                0,            0,             0, 1
            );
        Matrix4f translateToViewport = new Matrix4f(
                1, 0, 0, 1,
                0, 1, 0, 1,
                0, 0, 0, 0,
                0, 0, 0, 1
            );

        Matrix4f matrix = scaleToViewport.multiply(translateToViewport);

        return matrix;
    }

    public static Matrix4f identity() {

        Matrix4f matrix = new Matrix4f();

        matrix.fromArray(identityArray());

        return matrix;
    }

    /**
     * https://en.wikipedia.org/wiki/Translation_(geometry)#Matrix_representation
     * 
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static Matrix4f translation( float x, float y, float z ) {

        Matrix4f matrix = new Matrix4f(
            1, 0, 0, x,
            0, 1, 0, y,
            0, 0, 1, z,
            0, 0, 0, 1
        );

        return matrix;
    }

    public static Matrix4f translation( Vector3f vector ) {
        return translation(vector.getX(), vector.getY(), vector.getZ());
    }

    /**
     * https://en.wikipedia.org/wiki/Scaling_(geometry)#Using_homogeneous_coordinates
     * 
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static Matrix4f scale( float x, float y, float z ) {

        Matrix4f matrix = new Matrix4f(
            x, 0, 0, 0,
            0, y, 0, 0,
            0, 0, z, 0,
            0, 0, 0, 1
        );

        return matrix;
    }

    public static Matrix4f scale( Vector3f vector ) {
        return scale( vector.getX(), vector.getY(), vector.getZ() );
    }

    public static Matrix4f scale( float factor ) {
        return scale( factor, factor, factor );
    }

    /**
     * https://en.wikipedia.org/wiki/Shear_mapping
     * 
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static Matrix4f shear( float x, float y, float z ) {

        Matrix4f matrix = new Matrix4f(
            1, y, z, 0,
            x, 1, z, 0,
            x, y, 1, 0,
            0, 0, 0, 1
        );

        return matrix;
    }

    public static Matrix4f shear( Vector3f vector ) {
        return shear( vector.getX(), vector.getY(), vector.getZ() );
    }

    public static Matrix4f rotationX( double angle ) {
        return rotationX((float)angle);
    }

    /**
     * Positive rotation around X-axis.
     * 
     * The rotation is CW when camera direction vector is same as X-axis.
     * 
     * @see <a href="https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations">https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations</a>
     * @param angle
     * @return
     */
    public static Matrix4f rotationX( float angle ) {

        float cosA = (float) Math.cos(angle);
        float sinA = (float) Math.sin(angle);

        Matrix4f matrix = new Matrix4f(
            1, 0,    0,     0,
            0, cosA, -sinA, 0,
            0, sinA, cosA,  0,
            0, 0,    0,     1
        );

        return matrix;
    }

    /**
     * Rotation around Z-axis in right-hand coordinate system by a positive angle +alpha
     * is equal to rotation by a negative angle -alpha in left-hand coordinate system.
     * 
     * <pre>
     *  LHrotationMatrix = rotationZinRH.multiply( rotationZfromRHtoLH() );
     * </pre>
     * 
     * @return
     */
    public static Matrix4f rotationZfromRHtoLH() {

        return new Matrix4f(
                1, -1, 0, 0,
               -1,  1, 0, 0,
                0,  0, 1, 0,
                0,  0, 0, 1
            );
    }

    public static Matrix4f rotationY( double angle ) {
        return rotationY((float)angle);
    }

    /**
     * 
     * @see <a href="https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations">https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations</a>
     * @param angle
     * @return
     */
    public static Matrix4f rotationY( float angle ) {

        float cosA = (float) Math.cos(angle);
        float sinA = (float) Math.sin(angle);

        Matrix4f matrix = new Matrix4f(
            cosA,  0, sinA,  0,
            0,     1, 0,     0,
            -sinA, 0, cosA,  0,
            0,     0, 0,     1
        );

        return matrix;
    }

    public static Matrix4f rotationZ( double angle ) {
        return rotationZ((float)angle);
    }

    /**
     * 
     * @see <a href="https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations">https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations</a>
     * @param angle
     * @return
     */
    public static Matrix4f rotationZ( float angle ) {

        float cosA = (float) Math.cos(angle);
        float sinA = (float) Math.sin(angle);

        Matrix4f matrix = new Matrix4f(
            cosA, -sinA, 0, 0,
            sinA, cosA,  0, 0,
            0,    0,     1, 0,
            0,    0,     0, 1
        );

        return matrix;
    }


}
