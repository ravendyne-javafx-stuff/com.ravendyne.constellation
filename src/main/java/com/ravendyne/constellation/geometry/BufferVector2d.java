package com.ravendyne.constellation.geometry;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

/**
 * 2D vector view of a (part of) byte array.
 * 
 * This is mutable vector.
 *
 */
public class BufferVector2d {
    protected static final int X_IDX = 0;
    protected static final int Y_IDX = 1;

    /** Number of elements in this vector. */
    public static final int SIZE = 2;
    /** Number of bytes this vector needs for all its elements. */
    public static final int BYTES = SIZE * Double.BYTES;

    /** Position in original buffer that this vector has been created at.
     * If original buffer was {@link ByteBuffer} it's number of bytes,
     * if it was {@link DoubleBuffer} it's number of doubles. */
    protected final int position;
    
    protected DoubleBuffer data;

    private BufferVector2d(DoubleBuffer data, int position) {
        this.data = data;
        this.position = position;
    }
    
    /**
     * Creates an instance of 2D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #SIZE} subsequent elements.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector2d newInstance(DoubleBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= SIZE;

        final BufferVector2d vector = new BufferVector2d(buffer.slice(), buffer.position());

        return vector;
    }
    
    /**
     * Creates an instance of 2D vector view on this buffer.
     * 
     * The view is created starting at buffer's current position and takes up a number of {@link #BYTES} subsequent bytes.
     * 
     * Vector's elements are backed by this buffer, any change in the buffer at the position at which the vector is
     * created will be visible in vector's elements and vice versa.
     * 
     * @param buffer
     * @return
     */
    public static BufferVector2d newInstance(ByteBuffer buffer) {
        assert buffer != null;
        assert buffer.remaining() >= BYTES;

        final BufferVector2d vector = new BufferVector2d(buffer.asDoubleBuffer(), buffer.position());

        return vector;
    }
    
    public double getX() {
        return data.get(X_IDX);
    }
    
    public double getY() {
        return data.get(Y_IDX);
    }
    
    public BufferVector2d setX(double x) {
        data.put(X_IDX, x);
        
        return this;
    }
    
    public BufferVector2d setY(double y) {
        data.put(Y_IDX, y);
        
        return this;
    }
    
    public BufferVector2d set(double x, double y) {
        setX(x);
        setY(y);
        
        return this;
    }
    
    public BufferVector2d add(BufferVector2d other) {
        setX(getX() + other.getX());
        setY(getY() + other.getY());

        return this;
    }
    
    public BufferVector2d subtract(BufferVector2d other) {
        setX(getX() - other.getX());
        setY(getY() - other.getY());

        return this;
    }
    
    public BufferVector2d multiply(double factor) {
        setX(getX() * factor);
        setY(getY() * factor);

        return this;
    }
    
    public BufferVector2d divide(double factor) {
        setX(getX() / factor);
        setY(getY() / factor);

        return this;
    }
    
    public double length() {
        final double x = getX();
        final double y = getY();

        return Math.sqrt(x*x + y*y);
    }
    
    /**
     * Turns this vector into unit vector.
     * 
     * @return
     */
    public BufferVector2d normalize() {
        return divide(length());
    }
    
    public double dot(BufferVector2d other) {
        return getX() * other.getX() + getY() * other.getY();
    }
    
    /**
     * Turns this vector 90 degrees clockwise.
     * 
     * @return
     */
    public BufferVector2d cw90() {
        final double x = getX();
        final double y = getY();
        setX( - y );
        setY(   x );

        return this;
    }
    
    /**
     * Turns this vector 90 degrees counter-clockwise.
     * 
     * @return
     */
    public BufferVector2d ccw90() {
        final double x = getX();
        final double y = getY();
        setX(   y );
        setY( - x );

        return this;
    }

    public BufferVector2d copyTo(BufferVector2d other) {
        other.setX(getX());
        other.setY(getY());

        return this;
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #BYTES} bytes.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(ByteBuffer dst) {
        copyToBuffer(dst.asDoubleBuffer());
    }

    /**
     * Copies this vector to the {@code dst} buffer at its current position,
     * advancing the position by number of {@link #SIZE} elements.
     * 
     * @param dst buffer to copy this vector elements to
     */
    public void copyToBuffer(DoubleBuffer dst) {
        dst.put(data.array(), 0, SIZE);
    }
}
