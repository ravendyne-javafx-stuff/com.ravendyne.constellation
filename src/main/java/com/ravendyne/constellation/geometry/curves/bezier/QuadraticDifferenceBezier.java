package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class QuadraticDifferenceBezier extends QuadraticBezier {
    Point2D a;
    Point2D b;
    Point2D c;

    Point2D k0;
    Point2D k1;
    
    double step = 0.05;
    double u = 0.0;
    Point2D Bu;

    public QuadraticDifferenceBezier() {
        super();
        updateCoefficients();
    }

    private void updateCoefficients() {
        Point2D P0 = controls.get(0);
        Point2D P1 = controls.get(1);
        Point2D P2 = controls.get(2);
        a = P2.subtract(P1.multiply(2)).add(P0);
        b = P1.subtract(P0).multiply(2);
        c = P0;

        Point2D kA = a.multiply(step);
        Point2D kB = b.multiply(step);
        k0 = kA.multiply(2);
        k1 = kA.multiply(step).add(kB);
    }

    @Override
    protected void onControlPositionChanged(int index) {
        updateCoefficients();
    }

    // P = t^2 * (P2 - 2 * P1 + P0) + t * 2 * (P1 - P0) + P0
    @Override
    public Point2D B(double t) {
        if(t <= 0.0) {
            u = 0.0;
            Bu = super.B(0.0);
            return controls.get( 0 );
        }
        if(t >= 1.0) {
            u = 0.0;
            Bu = super.B(0.0);
            return controls.get( 2 );
        }
        
        Point2D Bu_d = Bu.add(k0.multiply(u)).add(k1);
        u += step;
        Bu = Bu_d;

        return Bu_d;
    }
}
