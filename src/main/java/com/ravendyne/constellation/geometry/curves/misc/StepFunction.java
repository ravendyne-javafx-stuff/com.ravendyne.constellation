package com.ravendyne.constellation.geometry.curves.misc;

import com.ravendyne.constellation.geometry.curves.AbstractParametricCurve;
import com.ravendyne.constellation.geometry.curves.IParametericValueProducer;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

public class StepFunction extends AbstractParametricCurve {
    
    public StepFunction() {
        super(1);
    }
    
    @Override
    protected void generateInitalControls() {
        controls.set(0, new Point2D(0.5, 1.0));
    }

    public Point2D value(double t) {
        if(t < controls.get(0).getX()) {
            return new Point2D(t, 0.0);
        }

        return new Point2D(t, controls.get(0).getY());
    }

    @Override
    public IParametericValueProducer getValueProducer() {
        return this::value;
    }

    @Override
    public BoundingBox getBoundingBox() {
        return null;
    }

    @Override
    protected void onControlPositionChanged(int index) {
    }

}
