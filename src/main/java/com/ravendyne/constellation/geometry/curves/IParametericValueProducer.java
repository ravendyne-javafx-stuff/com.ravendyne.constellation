package com.ravendyne.constellation.geometry.curves;

import javafx.geometry.Point2D;

public interface IParametericValueProducer {
    Point2D value(double u);
}
