package com.ravendyne.constellation.geometry.curves;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point2D;

public abstract class AbstractParametricCurve implements IParametericCurve {
    protected List<Point2D> controls;

    public AbstractParametricCurve(int controlCount) {
        if(controlCount < 0) {
            throw new IllegalArgumentException("Number of curve control points can't be < 0");
        }

        controls = new ArrayList<>(controlCount);
        while(controlCount > 0) {
            controls.add(Point2D.ZERO);
            controlCount--;
        }
        generateInitalControls();
    }
    
    @Override
    public int getControlCount() {
        return controls.size();
    }
    
    @Override
    public void setControlPosition(int index, Point2D point) {
        controls.set( index, point );
        onControlPositionChanged(index);
    }
    
    @Override
    public Point2D getControlPosition(int index) {
        return controls.get( index );
    }
    
    /**
     * Called whenever new positionis set for {@code index}-th control point.
     * @param index
     */
    protected abstract void onControlPositionChanged(int index);

    /**
     * Creates control points and spaces them out evenly
     * on the top and right sides of a 1.0 x 1.0 square.
     * 
     * @param order number of control points
     */
    protected void generateInitalControls() {
        // keep the control total count
        int N = controls.size();

        // we'll have new controls put in place
        controls.clear();

        // set all the control points inside 1x1 rectangle
        // so it is easier to scale() later on
        double width = 1.0;
        double height = 1.0;

        // integer math, rounds down
        int branch = ( N - 2 ) / 2;
        double ctrlStepX = width / (branch + 1.0);
        double ctrlStepY = height / (branch + 1.0);
//        System.out.print( N + " => " );

        // P0
        controls.add( Point2D.ZERO );
//        System.out.print( " 1 " );

        // top square side
        for(double x = ctrlStepX; x < width; x += ctrlStepX) {
            controls.add( new Point2D(x, 0) );
        }
//        System.out.print( branch );
//        System.out.print( " " );

        // midpoint, if any
        if( (N - 2 - branch * 2) == 1 ) {
            controls.add( new Point2D(width, 0) );
        }
//        System.out.print( (N - 2 - branch * 2) );

        // right square side
        for(double y = ctrlStepY; y < height; y += ctrlStepY) {
            controls.add( new Point2D(width, y) );
        }
//        System.out.print( " " );
//        System.out.print( branch );

        // PN
        controls.add( new Point2D(width, height) );
//        System.out.print( " 1 " );
//        System.out.println();
    }
}
