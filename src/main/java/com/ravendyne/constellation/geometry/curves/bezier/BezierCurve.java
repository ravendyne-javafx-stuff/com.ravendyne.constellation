package com.ravendyne.constellation.geometry.curves.bezier;

import com.ravendyne.constellation.geometry.curves.AbstractParametricCurve;
import com.ravendyne.constellation.geometry.curves.IParametericCurve;
import com.ravendyne.constellation.geometry.curves.IParametericValueProducer;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

public abstract class BezierCurve extends AbstractParametricCurve {
    public enum ViewType {
        Curve,
        CurveXu,
        CurveYu,
        FirstDerivative,
        SecondDerivative,
        FirstDerivativeMagnitude,
        SecondDerivativeMagnitude,
    }

    private static class ViewTypeWrapper implements IParametericCurve {

        private BezierCurve curve;
        private ViewType type;

        public ViewTypeWrapper(BezierCurve curve, ViewType type) {
            this.curve = curve;
            this.type = type;
        }

        @Override
        public IParametericValueProducer getValueProducer() {
          switch(type) {
          case Curve:
              return curve::B;
          case CurveXu:
              return curve::Bx;
          case CurveYu:
              return curve::By;
          case FirstDerivative:
              return curve::Bdot;
          case FirstDerivativeMagnitude:
              return curve::BdotMagnitude;
          case SecondDerivative:
              return curve::Bdotdot;
          case SecondDerivativeMagnitude:
              return curve::BdotdotMagnitude;
          }
          return curve::B;
        }

        @Override
        public int getControlCount() {
            return curve.getControlCount();
        }

        @Override
        public void setControlPosition(int index, Point2D point) {
            curve.setControlPosition(index, point);
        }

        @Override
        public Point2D getControlPosition(int index) {
            return curve.getControlPosition(index);
        }

        @Override
        public BoundingBox getBoundingBox() {
            return curve.getBoundingBox();
        }
        
    }

    public BezierCurve(int order) {
        super(order);
        if(order < 2) {
            throw new IllegalArgumentException("Bezier curve order '" + order + "' is invalid value");
        }
    }
    
    public IParametericCurve forType(ViewType type) {
        return new ViewTypeWrapper(this, type);
    }

    @Override
    public IParametericValueProducer getValueProducer() {
        return this::B;
    }

    @Override
    public BoundingBox getBoundingBox() {
        double minX, maxX;
        double minY, maxY;

        Point2D prev = controls.get(0);
        minX = maxX = prev.getX();
        minY = maxY = prev.getY();

        for(int idx = 1; idx < controls.size(); idx++) {
            final Point2D Pi = controls.get(idx);
            if(Pi.getX() > maxX) maxX = Pi.getX();
            if(Pi.getX() < minX) maxX = Pi.getX();
            if(Pi.getY() > maxY) maxY = Pi.getY();
            if(Pi.getY() < minY) minY = Pi.getY();
            prev = Pi;
        }
        
        return new BoundingBox(minX, minY, maxX - minX, maxY - minY);
    }

    protected void onControlPositionChanged(int index) {
    }

    protected abstract Point2D B(double t);

    protected abstract Point2D Bdot(double t);
    
    protected abstract Point2D Bdotdot(double t);

    protected Point2D Bx(double t) {
        return new Point2D(t, B(t).getX());
    }

    protected Point2D By(double t) {
        return new Point2D(t, B(t).getY());
    }

    protected Point2D BdotMagnitude(double t) {
        return new Point2D(t, Bdot(t).magnitude());
    }

    protected Point2D BdotdotMagnitude(double t) {
        return new Point2D(t, Bdotdot(t).magnitude());
    }
}
