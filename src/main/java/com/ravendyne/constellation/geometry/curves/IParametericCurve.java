package com.ravendyne.constellation.geometry.curves;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

public interface IParametericCurve {
    IParametericValueProducer getValueProducer();
    int getControlCount();
    void setControlPosition(int index, Point2D point);
    Point2D getControlPosition(int index);

    /**
     * Returns positions of botom-left and top-right corners of the minimum size rectangle 
     * that contains the curve points for valid parameter value range (i.e. [0,1]),
     * and contains all the control points.
     * 
     * @return
     */
    BoundingBox getBoundingBox();
}
