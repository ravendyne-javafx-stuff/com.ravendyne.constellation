package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class CubicPolynomialBezier extends CubicBezier {
    Point2D a;
    Point2D b;
    Point2D c;
    Point2D d;

    public CubicPolynomialBezier() {
        super();
        updateCoefficients();
    }

    private void updateCoefficients() {
        Point2D P0 = controls.get(0);
        Point2D P1 = controls.get(1);
        Point2D P2 = controls.get(2);
        Point2D P3 = controls.get(3);
        a = P3.subtract(P2.multiply(3)).add(P1.multiply(3)).subtract(P0);
        b = P2.subtract(P1.multiply(2)).add(P0).multiply(3);
        c = P1.subtract(P0).multiply(3);
        d = P0;
    }

    @Override
    protected void onControlPositionChanged(int index) {
        updateCoefficients();
    }

    // P = t^3 * (P3 - 3 * P2 + 3 * P1 - P0) + 3 * t^2 * (P2 - 2 * P1 + P0) + 3 * t * (P1 - P0) + P0
    @Override
    public Point2D B(double t) {
        if(t <= 0.0) return controls.get( 0 );
        if(t >= 1.0) return controls.get( 3 );

        Point2D result = a.multiply(t*t*t).add(b.multiply(t*t)).add(c.multiply(t)).add(d);
        
        return result;
    }
}
