package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class CubicDifferenceBezier extends CubicBezier {
    Point2D a;
    Point2D b;
    Point2D c;
    Point2D d;

    Point2D k0;
    Point2D k1;
    Point2D k2;
    
    double step = 0.05;
    double u = 0.0;
    Point2D Bu;

    public CubicDifferenceBezier() {
        super();
        updateCoefficients();
    }

    private void updateCoefficients() {
        Point2D P0 = controls.get(0);
        Point2D P1 = controls.get(1);
        Point2D P2 = controls.get(2);
        Point2D P3 = controls.get(3);
        a = P3.subtract(P2.multiply(3)).add(P1.multiply(3)).subtract(P0);
        b = P2.subtract(P1.multiply(2)).add(P0).multiply(3);
        c = P1.subtract(P0).multiply(3);
        d = P0;
        
        Point2D kA = a.multiply(step);
        Point2D kB = b.multiply(step);
        Point2D kC = c.multiply(step);
        k0 = kA.multiply(step*step).add(kB.multiply(step)).add(kC);
        k1 = kA.multiply(3);
        k2 = kA.multiply(step).multiply(3).add(kB.multiply(2));
    }

    @Override
    protected void onControlPositionChanged(int index) {
        updateCoefficients();
    }

    // P = t^3 * (P3 - 3 * P2 + 3 * P1 - P0) + 3 * t^2 * (P2 - 2 * P1 + P0) + 3 * t * (P1 - P0) + P0
    @Override
    public Point2D B(double t) {
        if(t <= 0.0) {
            u = 0.0;
            Bu = super.B(0.0);
            return controls.get( 0 );
        }
        if(t >= 1.0) {
            u = 0.0;
            Bu = super.B(0.0);
            return controls.get( 3 );
        }

        Point2D Bu_d = Bu.add(k0).add(k1.multiply(u*u)).add(k2.multiply(u));
        u += step;
        Bu = Bu_d;

        return Bu_d;
    }
}
