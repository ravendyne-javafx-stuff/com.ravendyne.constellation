package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class CubicBezier extends BezierCurve {

    public CubicBezier() {
        super(4);
    }

    public Point2D B(double t) {
        if(t <= 0.0) return controls.get( 0 );
        if(t >= 1.0) return controls.get( 3 );

        double A = (1.0 - t) * (1.0 - t) * (1.0 - t);
        double B = 3.0 * (1.0 - t) * (1.0 - t) * t;
        double C = 3.0 * (1.0 - t) * t * t;
        double D = t * t * t;
        
        Point2D result = 
                controls.get( 0 ).multiply(A).add(
                controls.get( 1 ).multiply(B).add(
                controls.get( 2 ).multiply(C).add(
                controls.get( 3 ).multiply(D))));
        
        return result;
    }

    public Point2D Bdot(double t) {
        if(t <= 0.0) return controls.get( 1 ).subtract(controls.get( 0 )).multiply(3.0);
        if(t >= 1.0) return controls.get( 3 ).subtract(controls.get( 2 )).multiply(3.0);

        double A = 3.0 * (1.0 - t) * (1.0 - t);
        double B = 6.0 * (1.0 - t) * t;
        double C = 3.0 * t * t;
        
        Point2D result = 
                controls.get( 1 ).subtract(controls.get( 0 )).multiply(A).add(
                controls.get( 2 ).subtract(controls.get( 1 )).multiply(B).add(
                controls.get( 3 ).subtract(controls.get( 2 )).multiply(C)));
        
        return result;
    }
    
    public Point2D Bdotdot(double t) {
        if(t <= 0.0)
            return controls.get( 2 ).subtract(controls.get( 1 ).multiply(2.0)).add(controls.get( 0 )).multiply(6.0);
        if(t >= 1.0)
            return controls.get( 3 ).subtract(controls.get( 3 ).multiply(2.0)).add(controls.get( 1 )).multiply(6.0);

        double A = 6.0 * (1.0 - t);
        double B = 6.0 * t;

        return
                // P2 - 2 * P1 + P0
                controls.get( 2 ).subtract(controls.get( 1 ).multiply(2.0)).add(controls.get( 0 )).multiply(A).add(
                // P3 - 2 * P2 + P1
                controls.get( 3 ).subtract(controls.get( 3 ).multiply(2.0)).add(controls.get( 1 )).multiply(B));
    }
}
