package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class LinearBezier extends BezierCurve {

    public LinearBezier() {
        super(2);
    }

    public Point2D B(double t) {
        if(t <= 0.0) return controls.get( 0 );
        if(t >= 1.0) return controls.get( 1 );

        double A = (1.0 - t) ;
        double B = t;
        
        Point2D result = 
                controls.get( 0 ).multiply(A).add(
                controls.get( 1 ).multiply(B));
        
        return result;
    }

    public Point2D Bdot(double t) {
        Point2D result = 
            // 2 * ( P1 - P0 )
            controls.get( 1 ).subtract(controls.get( 0 )).multiply(2.0);

        return result;
    }
    
    public Point2D Bdotdot(double t) {
        return Point2D.ZERO;
    }
}
