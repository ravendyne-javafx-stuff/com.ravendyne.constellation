package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class QuadraticBezier extends BezierCurve {

    public QuadraticBezier() {
        super(3);
    }

    public Point2D B(double t) {
        if(t <= 0.0) return controls.get( 0 );
        if(t >= 1.0) return controls.get( 2 );

        double A = (1.0 - t) * (1.0 - t);
        double B = 2.0 * (1.0 - t) * t;
        double C = t * t;
        
        Point2D result = 
                controls.get( 0 ).multiply(A).add(
                controls.get( 1 ).multiply(B).add(
                controls.get( 2 ).multiply(C)));
        
        return result;
    }

    public Point2D Bdot(double t) {
        if(t <= 0.0) return controls.get( 1 ).subtract(controls.get( 0 )).multiply(2.0);
        if(t >= 1.0) return controls.get( 2 ).subtract(controls.get( 1 )).multiply(2.0);

        double A = 2.0 * (1.0 - t);
        double B = 2.0 * t;
        
        Point2D result = 
            controls.get( 1 ).subtract(controls.get( 0 )).multiply(A).add(
            controls.get( 2 ).subtract(controls.get( 1 )).multiply(B));

        return result;
    }
    
    public Point2D Bdotdot(double t) {
        return
                // 2 * (P2 - 2 * P1 + P0)
                controls.get( 2 ).subtract(controls.get( 1 ).multiply(2.0)).add(controls.get( 0 )).multiply(2.0);
    }
}
