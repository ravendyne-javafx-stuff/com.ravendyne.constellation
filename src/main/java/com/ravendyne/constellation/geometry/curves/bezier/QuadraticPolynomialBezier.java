package com.ravendyne.constellation.geometry.curves.bezier;

import javafx.geometry.Point2D;

public class QuadraticPolynomialBezier extends QuadraticBezier {
    Point2D a;
    Point2D b;
    Point2D c;

    public QuadraticPolynomialBezier() {
        super();
        updateCoefficients();
    }

    private void updateCoefficients() {
        Point2D P0 = controls.get(0);
        Point2D P1 = controls.get(1);
        Point2D P2 = controls.get(2);
        a = P2.subtract(P1.multiply(2)).add(P0);
        b = P1.subtract(P0).multiply(2);
        c = P0;
    }

    @Override
    protected void onControlPositionChanged(int index) {
        updateCoefficients();
    }

    // P = t^2 * (P2 - 2 * P1 + P0) + t * 2 * (P1 - P0) + P0
    @Override
    public Point2D B(double t) {
        if(t <= 0.0) return controls.get( 0 );
        if(t >= 1.0) return controls.get( 2 );
        
        Point2D result = a.multiply(t*t).add(b.multiply(t)).add(c);
        
        return result;
    }
}
