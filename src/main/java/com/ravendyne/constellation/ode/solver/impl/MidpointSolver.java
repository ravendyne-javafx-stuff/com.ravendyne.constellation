package com.ravendyne.constellation.ode.solver.impl;

import com.ravendyne.constellation.ode.solver.SolverBase;
import com.ravendyne.constellation.ode.solver.api.IPhaseSpace;
import com.ravendyne.constellation.ode.solver.api.IPhaseSpaceProvider;
import com.ravendyne.constellation.ode.solver.api.ISolver;

/**
 * A.k.a. 2nd order Runge-Kutta method.
 *
 */
public class MidpointSolver extends SolverBase implements ISolver {

    public MidpointSolver() {
    }
    
    /*
     * midpoint method:
     *  - compute euler step:
     *      dq = h * F(q,t)
     *  - evaluate F at the midpoint of the step:
     *      Fmid = F(q + dq/2, t + h/2)
     *  - find next value using the midpoint F value:
     *      q(t + h) = q(t) + h * Fmid
     */
    @Override
    public void applyStep(IPhaseSpaceProvider state, double delta) {
        // fetch [ x, v ]
        IPhaseSpace x = state.getPhaseSpace();
        // x + dt/2 * F(x)
        // get [ v, f/m ] a.k.a. [ x', x'']
        // F(x)
        IPhaseSpace xd2 = state.getDerivative();
        // dt/2 * F(x)
        xd2.scale(delta / 2);
        // x + dt/2 * F(x)
        xd2.sum( x );
        
        // x + dt * F(x + dt/2 * F(x))  or
        // x + dt * F(xd2)
        // F(xd2)
        state.setPhaseSpace(xd2);
        IPhaseSpace xn = state.getDerivative();
        //  dt * F(xd2)
        xn.scale(delta);
        // x + dt * F(xd2)
        xn.sum( x );

        // xn = x + dt * F(x + dt/2 * F(x))
        state.setPhaseSpace( xn );

        state.advanceTime(delta);
    }

}
