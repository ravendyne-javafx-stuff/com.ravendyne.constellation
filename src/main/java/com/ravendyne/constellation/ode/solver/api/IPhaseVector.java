package com.ravendyne.constellation.ode.solver.api;

public interface IPhaseVector {
    // since we are making copies of phase vectors left and right
    // we can't figure out from just the values in phase vectors from which particular vertex
    // have they originated. getHandle() is a (not so efficient) way to keep that connection.
    // we need it so we can update the vertex once calculations are done.
    // TODO make this go away or just use IParticle and call it getParticle() ??
    IPhaseVectorHandle getHandle();
    // this is used by implementation to make copies of phase space
    // when ISolver requests current phase space or a derivative of the space
    IPhaseVector copy();
    void add( IPhaseVector next );
    void scale( double delta );
}
