package com.ravendyne.constellation.ode.solver.api;

public interface IPhaseSpace {
    void sum(IPhaseSpace other);
    void scale(double delta);
    IPhaseVector get(IPhaseVectorHandle handle);
    int size();
}
