package com.ravendyne.constellation.ode.solver.api;

//phys_model.pdf, p. 129
public interface IPhaseSpaceProvider {
    void advanceTime( double delta );
    IPhaseSpace getPhaseSpace();
    IPhaseSpace getDerivative();
    void setPhaseSpace( IPhaseSpace space );
}
