package com.ravendyne.constellation.ode.solver.api;

public interface ISolver {
    void applyStep(IPhaseSpaceProvider state, double delta);
}
