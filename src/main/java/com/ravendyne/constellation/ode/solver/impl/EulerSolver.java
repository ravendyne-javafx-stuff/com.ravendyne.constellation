package com.ravendyne.constellation.ode.solver.impl;

import com.ravendyne.constellation.ode.solver.SolverBase;
import com.ravendyne.constellation.ode.solver.api.IPhaseSpace;
import com.ravendyne.constellation.ode.solver.api.IPhaseSpaceProvider;
import com.ravendyne.constellation.ode.solver.api.ISolver;

public class EulerSolver extends SolverBase implements ISolver {

    public EulerSolver() {
    }
    
    /*
     * euler method:
     * q(t + h) = q(t) + h * F(q,t)
     */
    @Override
    public void applyStep(IPhaseSpaceProvider state, double delta) {
        // fetch [ x, v ]
        IPhaseSpace x = state.getPhaseSpace();
        // x + dt * F(x)
        // get F(x)
        // a.k.a. [ v, f/m ], a.k.a. [ x', x'']
        // F(x)
        IPhaseSpace xd = state.getDerivative();
        // dt * F(x)
        xd.scale(delta);
        // x + dt * F(x)
        xd.sum( x );

        state.setPhaseSpace(xd);

        state.advanceTime(delta);
    }

}
